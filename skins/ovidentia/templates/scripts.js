
/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};


function smed_node_type(val) {
	
	// in modification, the id_function field is disabled and id_function is set in a hidden field
	// for a new node, id_function is not disabled
	
	var isnew = true;
	
	if (jQuery('#smed_id_function input[disabled]').length) {
		isnew = false;
	}
	
	
	switch(val) {
		
		case '1':
		
			if (isnew) {
				
				// new node linked to a core node
				
				jQuery('#smed_id_function').hide();
				jQuery('#smed_labels_core').hide();
				jQuery('#smed_id_function_core').show();
				
			} else {
				
				// modification of a node linked to a core node without custom labels
				
				jQuery('#smed_id_function').hide();
				jQuery('#smed_labels_core').show();
				jQuery('#smed_id_function_core').show();
			}
			
			jQuery('#smed_labels_modify').hide();
			jQuery('#smed_url').hide();
			jQuery('#smed_classnames').hide();
			jQuery('#smed_custom_options').hide();
			
			break;
		
		
			
		case '2':
		
			// modification of a node linked to a core node with custom labels
		
			jQuery('#smed_id_function').hide();
			jQuery('#smed_id_function_core').show();
			jQuery('#smed_labels_modify').show();
			jQuery('#smed_labels_core').hide();
			jQuery('#smed_url').hide();
			jQuery('#smed_classnames').hide();
			jQuery('#smed_custom_options').hide();
			break;
			
		case '3':
		
			// new custom node
			
			jQuery('#smed_id_function').show();
			jQuery('#smed_id_function_core').hide();
			jQuery('#smed_labels_modify').show();
			jQuery('#smed_labels_core').hide();
			jQuery('#smed_url').show();
			jQuery('#smed_classnames').show();
			jQuery('#smed_custom_options').show();
			break;
	}
}






jQuery(document).ready(function() {
	
	jQuery("#smed_node_type").change(function() {
		smed_node_type(jQuery(this).val());
	});
	
	smed_node_type(jQuery("#smed_node_type").val());
	
});



jQuery(document).ready(function() {

	var pageId = jQuery('body').attr('id');
	
	var panelCookieBaseName = 'smed_panel';

	jQuery('.workspace-side-panel').each(function() {
	
		resizeSplitView(this);
	}
	);
	
	jQuery('.smed-resizable-panel')
	.resizable({
		handles: 'e',
		autoHide: true,
		start: function (event, ui) {
			window.noclick = true;
			jQuery(this)
				.removeClass('folded');
			jQuery.cookie(panelCookieBaseName + '_folded', null);
		},
		stop: function (event, ui) {
			jQuery.cookie(panelCookieBaseName + '_width', ui.size.width);
			if (ui.size.width <= 40) {
				jQuery(this)
					.width(8)
					.addClass('folded');
				jQuery.cookie(panelCookieBaseName + '_folded', 1);
			}
		}
	}
	)
	
	var panelWidth = parseInt(jQuery.cookie(panelCookieBaseName + '_width')) || 250;
	jQuery('.smed-resizable-panel')
	.width(panelWidth);
	
	jQuery('.smed-resizable-panel').each(
		function () {
			var resizable = this;
			if (jQuery.cookie(panelCookieBaseName + '_folded') == 1) {
				jQuery(resizable)
					.css('width', '8px')
					.addClass('folded');
			}
			jQuery(this).find('.ui-resizable-e').click(
	
				function () {
					if (window.noclick == true) {
						window.noclick = false;
					} else {
						if (jQuery(resizable).width() == 8) {
							var panelWidth = parseInt(jQuery.cookie(panelCookieBaseName + '_width')) || 250;
							jQuery(resizable)
								.animate({ width: panelWidth + 'px' }, 'fast')
								.removeClass('folded');
							jQuery.cookie(panelCookieBaseName + '_folded', null);
						} else {
							jQuery(resizable)
								.animate({ width: '8px' }, 'fast')
								.addClass('folded');
							jQuery.cookie(panelCookieBaseName + '_folded', 1);
						}
					}
				}
			);
		}
	);


});



function smed_windowResize()
{
	var mainPanel = jQuery('#smed_treeview');
	var windowInnerHeight = jQuery(window).height();
	mainPanel.length && mainPanel.css('minHeight', (windowInnerHeight - mainPanel.position().top) + 'px');
}

jQuery(window).load(function () {
	smed_windowResize();
	jQuery(window).resize(function () {
		smed_windowResize();
	});
});


