<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/iterator/iterator.php';
require_once dirname(__FILE__).'/node.class.php';


/**
 * Sitemap class
 *
 */
class smed_Sitemap {


    const ROOT = 'DGAll';


    /**
     * @var int
     */
    private $id_sitemap = null;


    /**
     *
     * @var smed_Node
     */
    private $root_node	= null;


    private $modified_nodelist = null;
    private $merged_nodelist = array();


    /**
     * Create a new sitemap
     *
     * @return	int
     */
    public static function create($name, $description = '') {

        global $babDB;


        $core = bab_siteMap::get();
        $dgAll = $core->getNodeById(self::ROOT);
        if ($dgAll) {


            $babDB->db_query('
                INSERT INTO smed_sitemap
                    (name, description)
                VALUES
                    ('.$babDB->quote($name).', '.$babDB->quote($description).')'
            );

            $id_sitemap = (int) $babDB->db_insert_id();


            $rootID = $dgAll->getId();

            $node = smed_Node::getFromCore($id_sitemap, $rootID);
            $node->setNodeType(smed_Node::ISCORE);
            $node->insert();

            return $id_sitemap;
        }

        return null;
    }


    /**
     * Update name and description of a sitemap
     *
     *
     */
    public static function update($id, $name, $description) {

        global $babDB;

        $babDB->db_query('
            UPDATE smed_sitemap
                SET
                name='.$babDB->quote($name).',
                description='.$babDB->quote($description).'
            WHERE
                id='.$babDB->quote($id).'
        ');

    }


    /**
     * Delete sitemap
     * @param	int		$id
     */
    public static function delete($id) {

        global $babDB;

        $babDB->db_query('
            DELETE FROM smed_nodes WHERE id_sitemap='.$babDB->quote($id).'
        ');

        $babDB->db_query('
            DELETE FROM smed_sitemap
            WHERE
                id='.$babDB->quote($id).'
        ');


    }



    public function __construct($id_sitemap) {

        $this->id_sitemap = $id_sitemap;
    }

    /**
     *
     * @return	array
     */
    public function getValues() {
        global $babDB;

        $res = $babDB->db_query('SELECT * FROM smed_sitemap WHERE id='.$babDB->quote($this->id_sitemap));

        $arr = $babDB->db_fetch_assoc($res);

        return $arr;
    }


    /**
     * Get the modified nodes of the sitemap
     * @return smed_nodesIterator
     */
    private function getNodes() {

        global $babDB;


        $res = $babDB->db_query('
            SELECT
                n.*,
                l.name,
                l.description
            FROM
                smed_nodes n
                    LEFT JOIN smed_node_labels l ON
                        l.id_sitemap=n.id_sitemap
                        AND l.id_function=n.id_function
                        AND l.lang='.$babDB->quote($GLOBALS['babLanguage']).'
            WHERE
                n.id_sitemap='.$babDB->quote($this->id_sitemap)
        );

        return new smed_nodesIterator($res);

    }




    /**
     *
     *
     */
    private function getMergedLevel(smed_Node $node) {

        $level = $node->getChildNodes();

        if ($level) {

            $this->merged_nodelist = $this->merged_nodelist + $level;

            foreach($level as $sub_node) {
                $this->getMergedLevel($sub_node);
            }
        }





    }


    /**
     * Get core sitemap merged with modifications
     *
     *
     * @return bab_TreeView
     */
    public function getMerged($treeView) {

        $root = $this->getRootNode();
        $this->getMergedLevel($root);

        //print_r(array_keys($this->merged_nodelist));

        foreach($this->merged_nodelist as $smed_node) {

            $classname = '';

            $element = $treeView->createElement($smed_node->getId(), $classname, $smed_node->getName(), $smed_node->getDescription(), $smed_node->getUrl());

            $id_parent = $smed_node->getParentId();

            switch($smed_node->getNodeType()) {

                case smed_Node::ISCORE:
                    $element->setIcon($GLOBALS['babSkinPath'] . 'images/Puces/check-green.gif');
                    break;

                case smed_Node::ISMODIFIED:
                    $element->setIcon($GLOBALS['babSkinPath'] . 'images/Puces/check-orange.gif');
                    break;

                case smed_Node::ISNEW:
                    $element->setIcon($GLOBALS['babSkinPath'] . 'images/Puces/folder_add.png');
                    break;

                default:
                    $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
            }

            $treeView->appendElement($element, $id_parent);
        }

        return $treeView;
    }




    /**
     * Create a new node under $rootNode from core sitemap
     *
     * @todo remove method_exist, change compatibility to 7.8.92
     *
     * @param	bab_siteMapOrphanRootNode	$rootNode
     * @param	string						$node_id
     */
    private static function copyNode(bab_siteMapOrphanRootNode $rootNode, $node_id) {



        $core = bab_siteMap::get();
        $node = $core->getNodeById($node_id);
        $data = clone $node->getData();
        $index = $node->getIndex();

        $id_parent = $node->parentNode();

        if (null !== $id_parent) {
            $id_parent = $id_parent->getId();
        }

        $node = $rootNode->createNode($data, $data->id_function);
        $node->setIndex($index);
        // $rootNode->addNodeIndexes($node, $data);
        $rootNode->appendChild($node, $id_parent);
    }

    /**
     * Create a new node under $rootNode from core sitemap
     * @param	bab_OrphanRootNode		$rootNode
     * @param	string					$node_id
     */
    private static function copyNodeRecursively($rootNode, $node_id) {

        self::copyNode($rootNode, $node_id);

        $core = bab_siteMap::get();
        $node = $core->getNodeById($node_id);

        $childNode = $node->firstChild();

        if (null === $childNode) {
            return null;
        }

        do {
            self::copyNodeRecursively($rootNode, $childNode->getId());

        } while ($childNode = $childNode->nextSibling());
    }


    public static function removeCache()
    {

        $cache = smed_Cache::getInstance();
        $cache->clear();
    }


    /**
     * Get modified sitemap according to core format
     * replace the DGAll branch from core sitemap and return modified sitemap
     *
     * @param	int	$id_sitemap
     *
     * @return bab_siteMapOrphanRootNode
     */
    public static function get($id_sitemap, $build = true)
    {
        if (bab_Registry::get('/sitemap_editor/disableCache', false) == true) {
            // disable cache
            return self::get_nocache($id_sitemap);
        }

        $cache = smed_Cache::getInstance();

        static $_sc = null;

        if (isset($_sc[$id_sitemap]))
        {
            return $_sc[$id_sitemap];
        }

        $uid_functions = bab_siteMap::getProfilVersionUid();

        if (null === $uid_functions)
        {
            if (!$build) {
                return null;
            }
            return self::get_nocache($id_sitemap);
        }

        $rootnode = $cache->get($id_sitemap, $uid_functions);

        if (null === $rootnode) {
            if (!$build) {
                return null;
            }

            $rootnode = self::get_nocache($id_sitemap);

            if (!isset($rootnode)) {
                return null;
            }

            $cache->set($id_sitemap, $uid_functions, $rootnode);
        }

        $_sc[$id_sitemap] = $rootnode;

        return $rootnode;
    }




    /**
     * Get modified sitemap according to core format
     * replace the DGAll branch from core sitemap and return modified sitemap
     *
     * @param	int	$id_sitemap
     *
     * @return bab_siteMapOrphanRootNode
     */
    public static function get_nocache($id_sitemap) {

        global $babDB;

        static $cache = array();
        if (isset($cache[$id_sitemap])) {
            return $cache[$id_sitemap];
        }

        $rootNode = new bab_siteMapOrphanRootNode;


        $data = new bab_siteMapItem();
        $data->id_function 		= 'root';
        $data->name 			= 'Root';
        $data->description 		= '';
        $data->url 				= '';
        $data->onclick 			= '';
        $data->folder 			= true;
        $data->iconClassnames	= '';

        $node = $rootNode->createNode($data, 'root');
        $rootNode->addNodeIndexes($node, $data);
        $rootNode->appendChild($node, null);


        // copy all delegation branchs except DGAll
        /* @var $babSitemap bab_siteMapOrphanRootNode */
        $babSitemap = bab_siteMap::get();
        $dgAll = $babSitemap->getNodeById(self::ROOT);


        if (!$dgAll) {
            // pas de noeud dgAll,
            // on est dans le cas ou une session existe
            // mais l'identifiant de l'utilisateur en session n'existe pas dans la base

            return null;
        }

        $rootCore = $dgAll->parentNode();
        $dg = $rootCore->firstChild();

        do {

            if (self::ROOT === $dg->getId()) {
                self::copyNode($rootNode, self::ROOT);
            } else {
                self::copyNodeRecursively($rootNode, $dg->getId());
            }
        } while($dg = $dg->nextSibling());


        // rewriting

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/sitemap_editor/');
        $rootNode->enableRewriting = bab_Registry::get('/sitemap_editor/rewriting', false);
        $appendId = $registry->getValue('appendId', false);

        $sitemap = new smed_Sitemap($id_sitemap);

        $rn = $sitemap->getRootNode();

        if (!($rn instanceof smed_Node))
        {
            trigger_error('Error, impossible to get rootnode for sitemap : '.$id_sitemap.' in sitemap editor');
            return null;
        }

        $sitemap->getMergedLevel($rn); // Populates $sitemap->merged_nodelist


        foreach($sitemap->merged_nodelist as $smed_node) {
            /* @var $smed_node smed_Node */

            // ignore container without accessible children
            if ('container' === $smed_node->getContentType() && !$smed_node->hasLeafNodes())
            {
                continue;
            }


            $data = $smed_node->getSitemapItem();

            if (!isset($data)) {
                continue;
            }


            if ($appendId
                && $data->url !== ''
                && (
                    smed_nodeType($smed_node) !== 'url'
                    || substr($data->url, 0, strlen($GLOBALS['babUrl'])) === $GLOBALS['babUrl']
                    || substr($data->url, 0, 1) === '?'
                    )
                ) {


                // If there is a non empty url and it is not an external link, we
                // append the smed_id parameter.
                $data->url .=  '&smed_id=' . $data->id_function;
            }
            $id_parent = $smed_node->getParentId();


            $node = $rootNode->createNode($data, $data->id_function);
            $rootNode->addNodeIndexes($node, $data);
            $rootNode->appendChild($node, $id_parent);

        }

        $cache[$id_sitemap] = $rootNode;

        return $rootNode;
    }


    /**
     * Search the smed Node in ancestors
     * @param int 						$id_sitemap
     * @param bab_siteMapOrphanRootNode $sitemapRootNode
     * @param	string					$nodeId					Sitemap node ID used as target in sitemap editor
     *
     * @return smed_Node
     */
    private static function searchForSmedAncestor($id_sitemap, $sitemapRootNode, $nodeId)
    {

        $node = smed_Node::getFromTargetId($id_sitemap, $nodeId);
        if (null === $node)
        {
            $node = $sitemapRootNode->getNodeById($nodeId);
            if($node === null){
                return null;
            }
            $parentNode = $node->parentNode();

            if (!isset($parentNode))
            {
                return null;
            }

            $nodeId = $parentNode->getId();

            return self::searchForSmedAncestor($id_sitemap, $sitemapRootNode, $nodeId);
        }

        return $node;
    }



    /**
     * Get a subtree with a list of childnodes, the node, the node ancestors
     * This method is used to display one menu level, the ancestors a required to generate the rewrite path
     *
     *
     * @param int $id_sitemap
     * @param string $nodeId
     *
     * @return bab_siteMapOrphanRootNode
     */
    public static function getLevel($id_sitemap, $nodeId)
    {


        $node = smed_Node::getFromId($id_sitemap, $nodeId);

        if (null === $node)
        {
            // si c'est un noeud genere par une reference avec enfant, l'id du noyau est suivi d'un suffix
            if (false !== $pos = strpos($nodeId, '#'))
            {
                $nodeId = substr($nodeId, 0, ($pos - 1));
            }

            // in core sitemap
            $rootNode = bab_sitemap::getLevel($nodeId);

            $iterator = new bab_NodeIterator($rootNode);
            while ($node = $iterator->nextNode()) {
                $sitemapItem = $node->getData();
                /*@var $sitemapItem bab_SitemapItem */

                if (isset($sitemapItem))
                {
                    $sitemapItem->rewriteName = smed_Node::formatRewrittenName($sitemapItem->name);
                }
            }


            $sitemapRootNode = bab_sitemap::getFromSite();
            $refNode = self::searchForSmedAncestor($id_sitemap, $sitemapRootNode, $nodeId);

            if (isset($refNode) && $refNode->getSortType() === smed_Node::SORT_ALPHA)
            {
                $rootNode->sortSubTree();
            }

            return $rootNode;
        }



        $rootNode = new bab_siteMapOrphanRootNode;

        $data = $node->getSitemapItem();
        $babnode = $rootNode->createNode($data, $node->id_function);
        $rootNode->addNodeIndexes($babnode, $data);
        $rootNode->appendChild($babnode, $node->getParentId());


        $a = $node->getModifiedAncestors();
        foreach($a as $k => $an)
        {
            /* @var $an smed_Node */
            $data = $an->getSitemapItem();

            $babnode = $rootNode->createNode($data, $an->id_function);
            $rootNode->addNodeIndexes($babnode, $data);
            if ($k === 0)
            {
                $parent = null;
            } else {
                $parent = $an->getParentId();
            }
            $rootNode->appendChild($babnode, $parent);
        }




        $level = $node->getChildNodes();

        foreach($level as $node)
        {
            /* @var $node smed_Node */

            $data = $node->getSitemapItem();

            // add item count

            $data->haschildnodes = (count($node->getChildNodes()) > 0);


            $babnode = $rootNode->createNode($data, $node->id_function);
            $rootNode->addNodeIndexes($babnode, $data);
            $rootNode->appendChild($babnode, $node->getParentId());


        }




        return $rootNode;
    }











    /**
     * A sitemap must have a root node
     * @return smed_Node
     */
    public function getRootNode() {

        return smed_Node::getFromId($this->id_sitemap, self::ROOT);
    }







    /**
     * Delete node from merged sitemap
     * set disabled if the node is a recorded core reference
     * Add a disabled if the node is a core node
     * Or delete is the node is a new node
     *
     * @param	string	$id_function
     *
     */
    public function deleteNodeFromMerged($id_function) {


        $node = smed_Node::getFromId($this->id_sitemap, $id_function);
        if (null !== $node) {

            // the node is in table

            if (smed_Node::ISNEW === (int) $node->getNodeType()) {
                // delete
                $node->deleteReference();

            } else {

                // disable
                $node->disableReference();
            }

        } else {

            $node = smed_Node::getFromCore($this->id_sitemap, $id_function);

            $node->insert();
            $node->disableReference();
        }
    }









    /**
     * Get an iterator object of the modified nodes for user side
     * @return Iterator
     */
    public static function getModifiedIterator($parentNodeId = null)
    {
        global $babDB;

        $query = '
            SELECT
                n.*,
                l.name,
                l.description
            FROM
                smed_nodes n
                    LEFT JOIN smed_node_labels l ON
                        l.id_sitemap=n.id_sitemap
                        AND l.id_function=n.id_function
                        AND l.lang='.$babDB->quote(bab_getLanguage()).'
                    LEFT JOIN smed_sort s ON
                        s.id_sitemap=n.id_sitemap
                        AND s.id_function=n.id_function
                        AND s.id_parent=n.id_parent
            WHERE
                n.id_sitemap='.$babDB->quote(1).'
                AND n.id_function<>'.$babDB->quote(self::ROOT);

        if (isset($parentNodeId)) {
            $query .= '
                AND n.id_parent = ' . $babDB->quote($parentNodeId);
        }

        $query .= '
            ORDER BY s.sort_index, l.name';


        $res = $babDB->db_query($query);
        return new smed_nodesIterator($res);
    }
}



abstract class smed_Cache
{
    /**
     * @return smed_Cache
     */
    public static function getInstance()
    {
        //return new smed_Cache_Db;
        return new smed_Cache_File;
    }

    /**
     * Clear cache
     */
    abstract public function clear();

    /**
     * Set rootnode in cache
     *
     * @param int $id_sitemap
     * @param bab_OrphanRootNode $rootnode
     * @param int $uid_functions
     */
    abstract public function set($id_sitemap, $uid_functions, bab_OrphanRootNode $rootnode);

    /**
     * Get rootnode from cache
     * @param int $id_sitemap
     * @param bab_OrphanRootNode $rootnode
     * @return bab_OrphanRootNode
     */
    abstract public function get($id_sitemap, $uid_functions);
}



/**
 * Cache sitemap into database
 *
 */
class smed_Cache_Db extends smed_Cache
{
    /**
     * Clear cache
     */
    public function clear()
    {
        global $babDB;
        $babDB->db_query("TRUNCATE smed_cache");
    }



    /**
     * Set rootnode in cache
     *
     * @param int $id_sitemap
     * @param bab_OrphanRootNode $rootnode
     * @param int $uid_functions
     */
    public function set($id_sitemap, $uid_functions, bab_OrphanRootNode $rootnode)
    {
        global $babDB;

        $babDB->db_query('
            INSERT INTO
                smed_cache

            (id_sitemap, data, uid_functions, creation_date)
                VALUES (
                '.$babDB->quote($id_sitemap).',
                '.$babDB->quote(serialize($rootnode)).',
                '.$babDB->quote($uid_functions).',
                NOW()
            )
        ');

        $babDB->db_query('DELETE FROM smed_cache WHERE DATE_ADD(creation_date, INTERVAL 1 DAY)<CURDATE()');
    }


    /**
     * Get rootnode from cache
     * @return bab_OrphanRootNode
     */
    public function get($id_sitemap, $uid_functions)
    {
        global $babDB;

        $res = $babDB->db_query("SELECT data FROM smed_cache
            WHERE uid_functions=".$babDB->quote($uid_functions)." AND id_sitemap=".$babDB->quote($id_sitemap)."
            ORDER BY creation_date DESC LIMIT 0,1
        ");


        if (0 === $babDB->db_num_rows($res))
        {
            return null;
        }

        $dbcache = $babDB->db_fetch_assoc($res);
        $rootnode = unserialize($dbcache['data']);

        if (!($rootnode instanceof bab_OrphanRootNode))
        {
            bab_debug('Failed to load sitemap from cache');
            return null;
        }

        return $rootnode;
    }
}



/**
 * Cache sitemap into files
 *
 */
class smed_Cache_File extends smed_Cache
{

    /**
     * @return bab_Path
     */
    private function getFolder()
    {
        static $path = null;

        if (null === $path)
        {
            require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

            $addon = bab_getAddonInfosInstance('sitemap_editor');

            $path = new bab_Path($addon->getUploadPath(), __CLASS__);
            $path->createDir();
        }

        return $path;
    }

    /**
     * Clear cache
     */
    public function clear()
    {
        $path = $this->getFolder();

        try {
            $path->deleteDir();
        } catch(bab_FolderAccessRightsException $e)
        {
            bab_debug($e->getMessage());
        }
    }



    /**
     * Set rootnode in cache
     *
     * @param int $id_sitemap
     * @param bab_OrphanRootNode $rootnode
     * @param int $uid_functions
     */
    public function set($id_sitemap, $uid_functions, bab_OrphanRootNode $rootnode)
    {
        $path = $this->getFolder();
        $file = clone $path;
        $file->push($id_sitemap.'_'.$uid_functions);
        file_put_contents($file->tostring(), serialize($rootnode));

        // delete files oder than one day

        foreach($path as $file)
        {
            /*@var $file bab_Path */
            if ((time() - filemtime($file->tostring())) > 86400)
            {
                try {
                    $file->delete();
                } catch (bab_FileAccessRightsException $e) {
                    bab_debug($e->getMessage());
                }
            }
        }
    }


    /**
     * Get rootnode from cache
     * @return bab_OrphanRootNode
     */
    public function get($id_sitemap, $uid_functions)
    {
        $path = $this->getFolder();
        $file = clone $path;
        $file->push($id_sitemap.'_'.$uid_functions);

        if (!$file->fileExists())
        {
            return null;
        }

        $rootnode = unserialize(file_get_contents($file->tostring()));

        if (!($rootnode instanceof bab_OrphanRootNode))
        {
            bab_debug('Failed to load sitemap from cache');
            return null;
        }

        return $rootnode;
    }
}




/**
 * Result list of nodes
 */
class smed_nodesIterator extends BAB_MySqlResultIterator {

    /**
     * @param	ressource	Mysql ressource
     *
     */
    public function __construct($res) {
        parent::BAB_MySqlResultIterator();
        $this->setMySqlResult($res);
    }


    public function getObject($arr) {
        return smed_node::getFromTable($arr);
    }
}


