<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/base.ui.php';
require_once dirname(__FILE__) . '/config.ui.php';
require_once dirname(__FILE__) . '/breadcrumbs.class.php';





/**
 * This controller manages actions that can be performed on the configuration.
 */
class smed_CtrlConfig extends smed_Controller
{

	private function registry()
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/sitemap_editor/');

		return $registry;
	}





	public function edit()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();
		$page->addClass('crm-page crm-page-editor');

		$page->setTitle(smed_translate('Sitemap editor options'));
		$editor = new smed_ConfigEditor();

		$editor->setValues(
			array(
				'config' => array(
				    'rewriting' 			=> bab_Registry::get('/sitemap_editor/rewriting', false),
				    'appendId' 				=> bab_Registry::get('/sitemap_editor/appendId', false),
				    'createCategoryNode' 	=> bab_Registry::get('/sitemap_editor/createCategoryNode', false),
				    'moveAdminTopic'		=> bab_Registry::get('/sitemap_editor/moveAdminTopic', true)					// autoriser le deplacement et renommage des themes d'articles dans l'administration
				)
			)
		);

		$source = bab_Registry::getSource('/sitemap_editor/rewriting');
		if ($source === 'importantConstant') {
		    $editor->rewritingItem->disable();
		    $editor->setTitle(smed_translate('Forced by constant'));
		}
		if ($source === 'constant') {
		    $editor->setTitle(smed_translate('Defined by constant'));
		}

		$page->addItem($editor->addClass('icon-left-16 icon-left icon-16x16'));
		return $page;
	}



	public function reset()
	{
	    $registry = $this->registry();

	    $registry->deleteDirectory();

	    return true;
	}


	/**
	 * Saves configuration
	 *
	 * @param array	$note
	 * @return Widget_Action
	 */
	public function save($config = null)
	{
		global $babBody;
		$rewriting 			= (bool) $config['rewriting'];
		$appendId 			= (bool) $config['appendId'];
		$createCategoryNode = (bool) $config['createCategoryNode'];
		$moveAdminTopic 	= (bool) $config['moveAdminTopic'];

		$root = dirname('.');
		$htaccess = $root.'/.htaccess';

		if (true === $rewriting)
		{
			// require an apache server


			if (!isset($_SERVER["SERVER_SOFTWARE"]) ||
			    (
			        false === strpos($_SERVER["SERVER_SOFTWARE"], 'Apache')
			        && false === strpos($_SERVER["SERVER_SOFTWARE"], 'Development Server')
			    ))
			{
				$babBody->addError(smed_translate('You need an apache server to enable rewriting'));
				return $this->edit();
			}

			// test if root folder is writable to write the htaccess file


			if (!file_exists($htaccess))
			{

				if (!is_writable($root))
				{
					$babBody->addError(smed_translate('Error, the .htaccess file could not be written'));
					return $this->edit();
				}


				// RewriteBase

				$arr = parse_url($GLOBALS['babUrl']);

				$template = file_get_contents(dirname(__FILE__).'/htaccesstemplate');
				$template = str_replace('%RewriteBase%', $arr['path'], $template);

				file_put_contents($htaccess, $template);

			}

		} else {

			if (file_exists($htaccess))
			{
				// unlink($htaccess);
			}
		}

		$this->registry()->setKeyValue('rewriting', $rewriting);
		$this->registry()->setKeyValue('appendId', $appendId);
		$this->registry()->setKeyValue('createCategoryNode', $createCategoryNode);
		$this->registry()->setKeyValue('moveAdminTopic', $moveAdminTopic);

		require_once dirname(__FILE__).'/sitemap.class.php';
		smed_Sitemap::removeCache();
		$this->cancel();
	}



	/**
	 * Does nothing and returns to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		$last = smed_BreadCrumbs::last();
		if (isset($last)) {
			smed_redirect($last);
			die;
		}
		smed_redirect('?tg=addons');
		die;
	}


	/**
	 * Returns to the previous page and displays the specified error message.
	 *
	 * @param string	$errorMessage
	 * @return Widget_Action
	 */
	protected function error($errorMessage)
	{
		smed_redirect(smed_BreadCrumbs::last(), $errorMessage);
	}


}
