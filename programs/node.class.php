<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/sitemap.php';


class smed_Node {

    /**
     * Node Type : is default
     * The node has not been modified
     */
    const ISDEFAULT		= 0;

    /**
     * Node Type : is core
     * The node has not been modified but is stored in node list to save disable status or a change of position
     */
    const ISCORE		= 1;

    /**
     * Node type : is modified
     * The node exists in core but label are modified
     */
    const ISMODIFIED	= 2;


    /**
     * Node type : is new
     * The node does not exists in core
     */
    const ISNEW			= 3;


    /**
     * No sort info for child nodes (quickest method)
     */
    const SORT_UNORDERED	= NULL;

    /**
     * Sort child nodes by internationalized name
     */
    const SORT_ALPHA		= 'ALPHANUM';

    /**
     * Sort child nodes manually and remaining unsorted items are put in original order on top
     */
    const SORT_FIXED_TOP	= 'FIXEDTOP';

    /**
     * Sort child nodes manually and remaining unsorted items are put in original order at the bottom
     */
    const SORT_FIXED_BOTTOM	= 'FIXEDBOT';



    public $id_function 		= null;
    private $id_parent			= null;
    private $id_sitemap		    = null;
    private $node_type			= null;

    /**
     * @var string
     */
    private $sort_type			= null;

    private $disabled			= 0;
    private $disabled_rewrite	= 0;

    private $page_title		= null;
    private $page_keywords		= null;
    private $page_description	= null;

    private $rewrite_name		= null;

    private $popup				= 0;
    private $menuignore		    = 0;
    private $breadcrumbignore	= 0;
    private $rewriteignore	    = 0;
    private $targetchildnodes	= 0;
    private $target			= null;

    /**
     * content type of the node
     * @var string
     */
    private $content_type		= null;

    /**
     * Content type allowed for subnodes
     * @var string (comma separated)
     */
    private $contenttypes		= null;

    private $name				= null;
    private $description		= null;
    private $url				= null;
    private $classnames			= null;


    private $setlanguage        = null;
    private $langid             = null;

    private $allowedContentTypes = null;
    private $translations		= null;

    private $sort_childnodes	= null;
    private $childNodesCache	= null;

    private function __construct() {
        // new is forbidden
    }



    /**
     * Create node object from an array (table row)
     * @param	array	$arr
     * @return smed_Node
     */
    public static function getFromTable(array $arr) {
        $node = new smed_Node;
        $node->setFromTable($arr);




        return $node;
    }


    public static function getFromTargetId($id_sitemap, $id_function) {
        global $babDB;

        $query = '
        SELECT
            n.*,
            l.name,
            l.description
        FROM
        smed_nodes n
            LEFT JOIN smed_node_labels l ON
                l.id_sitemap=n.id_sitemap
                AND l.id_function=n.id_function
                AND l.lang='.$babDB->quote(bab_getLanguage()).'
        WHERE
            n.target='.$babDB->quote($id_function).'
            AND n.id_sitemap='.$babDB->quote($id_sitemap);

        $res = $babDB->db_query($query);

        if (0 === $babDB->db_num_rows($res)) {
            return null;
        }

        $arr = $babDB->db_fetch_assoc($res);
        $node = self::getFromTable($arr);

        return $node;
    }


    /**
     * Get sitemap node from table with the ID
     * @param	string	$id_function
     * @return	smed_Node | null		return null if the node is not registered
     */
    public static function getFromId($id_sitemap, $id_function) {
        global $babDB;

        $query = '
            SELECT
                n.*,
                l.name,
                l.description
            FROM
                smed_nodes n
                    LEFT JOIN smed_node_labels l ON
                        l.id_sitemap=n.id_sitemap
                        AND l.id_function=n.id_function
                        AND l.lang='.$babDB->quote(bab_getLanguage()).'
            WHERE
                n.id_function='.$babDB->quote($id_function).'
                AND n.id_sitemap='.$babDB->quote($id_sitemap);

        $res = $babDB->db_query($query);

        if (1 !== $babDB->db_num_rows($res)) {
            return null;
        }

        $arr = $babDB->db_fetch_assoc($res);
        $node = self::getFromTable($arr);

        return $node;
    }

    /**
     * create a smed_Node from a core node
     * @return smed_Node
     */
    private static function getFromCoreNode($id_sitemap, bab_Node $core_node) {
        $node = new smed_Node;



        $node->id_function 	= $core_node->getId();
        $node->id_sitemap	= $id_sitemap;
        $node->node_type	= self::ISDEFAULT;

        $parent = $core_node->parentNode();
        if (null === $parent) {
            $node->id_parent	= 0;
        } else {
            $node->id_parent	= $parent->getId();
        }

        return $node;
    }





    /**
     * Create a node from core sitemap
     * @param	int		$id_sitemap
     * @param	string	$id_function
     * @return	smed_Node
     */
    public static function getFromCore($id_sitemap, $id_function) {
        $core_node = bab_siteMap::get()->getNodeById($id_function);
        if (!$core_node) {
            return null;
        }

        return self::getFromCoreNode($id_sitemap, $core_node);
    }




    /**
     * Get node from merged list
     * Get node from modified list or directly from core
     * @return	smed_Node
     */
    public static function get($id_sitemap, $id_function) {
        $modified = self::getFromId($id_sitemap, $id_function);

        if (null !== $modified) {
            return $modified;
        }

        $core = self::getFromCore($id_sitemap, $id_function);

        if (null !== $core) {
            return $core;
        }
    }




    /**
     * Set values from Mysql result
     * @param	array	$arr
     * @return	smed_Node
     */
    private function setFromTable(array $arr) {

        foreach($arr as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new Exception('wrong property '.$key);
            }

            $this->$key = $value;
        }

        return $this;
    }








    private function getCoreSitemapItemProperty($property) {

        $core_node = bab_siteMap::get()->getNodeById($this->id_function);
        if (!$core_node) {
            return null;
        }

        $sitemap_item = $core_node->getData();

        if (!($sitemap_item instanceOf bab_siteMapItem))
        {
            return null;
        }

        return $sitemap_item->$property;
    }



    /**
     * return parent, if root node, return null
     * @return string | NULL
     */
    public function getParentId() {

        if ('' === $this->id_parent) {
            return null;
        }

        return $this->id_parent;
    }



    /**
     * Get list of ancestors from root to leaf
     * this method browse only the modified node from the sitemap editor
     * and stop the ancestor search at the user tree root level
     * @return smed_Node[]
     */
    public function getModifiedAncestors()
    {
        $arr = array();
        self::addParentNodes($arr, $this);

        return $arr;
    }





    /**
     * populate the ancestor search result for one level
     * @param array $arr
     * @return bool
     */
    private static function addParentNodes(&$arr, smed_Node $node)
    {
        if ('Custom' === $node->getId() || null === $parent = $node->getParentId())
        {
            return false;
        }

        $parent_node = smed_Node::getFromId($node->id_sitemap, $parent);

        if (null === $parent_node)
        {
            return false;
        }

        array_unshift($arr, $parent_node);
        self::addParentNodes($arr, $parent_node);
        return true;
    }


    /**
     * @return string
     */
    public function getId() {
        return $this->id_function;
    }

    /**
     * @return string
     */
    public function getLangId() {

        if ('' === $this->langid) {
            return null;
        }

        return $this->langid;
    }

    /**
     * Get node type
     * @return	int				self::ISDEFAULT | self::ISCORE | self::ISMODIFIED | self::ISNEW
     */
    public function getNodeType() {
        return (int) $this->node_type;
    }


    /**
     * Set node type
     * @param	int		$type	self::ISDEFAULT | self::ISCORE | self::ISMODIFIED | self::ISNEW
     *
     */
    public function setNodeType($type) {
        $this->node_type = (int) $type;

        return $this;
    }


    /**
     * Get sort type of child nodes
     * @return	string			self::SORT_UNORDERED | self::SORT_ALPHA | self::SORT_FIXED_TOP | self::SORT_FIXED_BOTTOM
     */
    public function getSortType() {
        return $this->sort_type;
    }

    /**
     * Get sort type of child nodes
     * @param	string	$type	self::SORT_UNORDERED | self::SORT_ALPHA | self::SORT_FIXED_TOP | self::SORT_FIXED_BOTTOM
     */
    public function setSortType($type) {
        $this->sort_type = $type;

        return $this;
    }



    /**
     * Get max sort index of childrens
     * @return int
     */
    public function getMaxChildSort() {
        global $babDB;

        $res = $babDB->db_query('
            SELECT

                sort_index
            FROM
                smed_sort
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_parent='.$babDB->quote($this->id_function).'
            ORDER BY sort_index DESC'
        );

        if (0 === $babDB->db_num_rows($res))
        {
            return 0;
        }

        $arr = $babDB->db_fetch_assoc($res);

        return (int) $arr['sort_index'];
    }


    /**
     * get the first setlanguage property in the ancestors
     * @return string
     */
    public function getTreeSetLanguage()
    {
        // get language from setlanguage in ancestors
        $ancestors = $this->getModifiedAncestors();
        foreach($ancestors as $anode) {
            $setlanguage = $anode->getSetLanguage();
            if (isset($setlanguage)) {
                return $setlanguage;
            }
        }

        return $this->getSetLanguage();
    }



    /**
     * Get sort index of the child node
     *
     * @param	smed_Node	$node
     *
     * @return	int
     */
    public function getSortValues() {


        if (null === $this->sort_childnodes) {
            global $babDB;

            $res = $babDB->db_query('
                SELECT
                    id_function,
                    sort_index
                FROM
                    smed_sort
                WHERE
                    id_sitemap='.$babDB->quote($this->id_sitemap).'
                    AND id_parent='.$babDB->quote($this->id_function)
            );

            $this->sort_childnodes = array();

            while ($arr = $babDB->db_fetch_assoc($res)) {
                $this->sort_childnodes[$arr['id_function']] = (int) $arr['sort_index'];
            }


            foreach($this->getChildNodes() as $node) {

                if (!isset($this->sort_childnodes[$node->getId()])) {

                    if (self::SORT_FIXED_TOP === $this->sort_type) {
                        $this->sort_childnodes[$node->getId()] = 0;
                    }

                    if (self::SORT_FIXED_BOTTOM === $this->sort_type) {
                        $this->sort_childnodes[$node->getId()] = 1000000;
                    }
                }
            }
        }

        return $this->sort_childnodes;
    }



    /**
     * Get sort index of node
     * @return int
     */
    public function getSortIndex() {
        global $babDB;

        static $sortIndex = null;

        if($sortIndex === null) {
            $sortIndex = array();
            $res = $babDB->db_query('
                SELECT
                    id_function,
                    sort_index,
                    id_sitemap,
                    id_parent,
                    id_function
                FROM
                    smed_sort'
            );
            while ($arr = $babDB->db_fetch_assoc($res)) {
                if(!isset($sortIndex[$arr['id_sitemap']])) {
                    $sortIndex[$arr['id_sitemap']] = array();
                }
                if(!isset($sortIndex[$arr['id_sitemap']][$arr['id_parent']])) {
                    $sortIndex[$arr['id_sitemap']][$arr['id_parent']] = array();
                }
                $sortIndex[$arr['id_sitemap']][$arr['id_parent']][$arr['id_function']] = (int) $arr['sort_index'];
            }
        }

        if (isset($sortIndex[$this->id_sitemap])
            && isset($sortIndex[$this->id_sitemap][$this->id_parent])
            && isset($sortIndex[$this->id_sitemap][$this->id_parent][$this->id_function])) {
            return $sortIndex[$this->id_sitemap][$this->id_parent][$this->id_function];
        }

        return null;
    }







    /**
     * Set sort values for childnodes
     * @param	array	$values			keys are id_function and values are numeric sort indexes
     */
    public function setSortValues($values) {

        global $babDB;


        foreach($values as $id_function => $sort_index) {

            $babDB->db_query('DELETE FROM smed_sort
                WHERE
                    id_sitemap='.$babDB->quote($this->id_sitemap).'
                    AND id_function='.$babDB->quote($id_function)
            );

            $babDB->db_query('INSERT INTO smed_sort
                    (id_sitemap, id_parent, id_function, sort_index)
                VALUES
                    ('.$babDB->quote($this->id_sitemap).', '.$babDB->quote($this->id_function).', '.$babDB->quote($id_function).', '.$babDB->quote($sort_index).')
            ');
        }

    }



    public function setSortIndex($sort_index)
    {
        global $babDB;


        $babDB->db_query('DELETE FROM smed_sort
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_function='.$babDB->quote($this->id_function)
        );

        $babDB->db_query('INSERT INTO smed_sort
                (id_sitemap, id_parent, id_function, sort_index)
            VALUES
                ('.$babDB->quote($this->id_sitemap).', '.$babDB->quote($this->id_parent).', '.$babDB->quote($this->id_function).', '.$babDB->quote($sort_index).')
        ');
    }



    /**
     * get other nodes with same langid
     * @return array
     */
    public function getTranslatedNodes()
    {
        $langid = $this->getLangId();

        if (!isset($langid)) {
            return array();
        }

        global $babDB;

        $res = $babDB->db_query(
            'SELECT * FROM smed_nodes WHERE
                langid='.$babDB->quote($this->langid).'
                AND id_function<>'.$babDB->quote($this->id_function).'
                AND id_sitemap='.$babDB->quote($this->id_sitemap));

        $alt = array();
        while ($arr = $babDB->db_fetch_assoc($res)) {
            $node = smed_Node::getFromTable($arr);
            $language = $node->getLanguage();

            if (isset($language)) {
                $alt[$language] = $node;
            }
        }

        return $alt;
    }




    private function getTranslations()
    {
        if (null === $this->translations) {
            static $translations = null;
            if ($translations === null) {
                $translations = array();

                global $babDB;

                $res = $babDB->db_query('SELECT id, lang, name, description, id_function, id_sitemap FROM smed_node_labels');
                while ($arr = $babDB->db_fetch_assoc($res)) {
                    if(!isset($translations[$arr['id_sitemap']])) {
                        $translations[$arr['id_sitemap']] = array();
                    }
                    if(!isset($translations[$arr['id_sitemap']][$arr['id_function']])) {
                        $translations[$arr['id_sitemap']][$arr['id_function']] = array();
                    }
                    $translations[$arr['id_sitemap']][$arr['id_function']][$arr['lang']] = array(
                        'id' => (int) $arr['id'],
                        'name' => $arr['name'],
                        'description' => $arr['description']
                    );
                }
            }

            if(!isset($translations[$this->id_sitemap])) {
                $translations[$this->id_sitemap] = array();
            }
            if(!isset($translations[$this->id_sitemap][$this->id_function])) {
                $translations[$this->id_sitemap][$this->id_function] = array();
            }
            $this->translations = $translations[$this->id_sitemap][$this->id_function];
        }

        return $this->translations;
    }



    /**
     * get row translation for one language
     *
     * @param	string	$lang
     */
    private function getTranslation($lang) {


        $translations = $this->getTranslations();


        if (!isset($translations[$lang])) {
            return array(
                'name' => '',
                'description' => ''
            );
        }

        return $translations[$lang];
    }


    /**
     * Get language from saved translations
     * @return string
     */
    public function getLanguage()
    {
        $translations = $this->getTranslations();
        if (1 !== count($translations)) {
            return null;
        }

        return key($translations);
    }





    /**
     * @param	string		$key		name | description
     * @param	string		$lang
     *
     * @return 	string
     */
    private function getLabelString($key, $lang = null) {

        if (self::ISMODIFIED === (int) $this->node_type || self::ISNEW === (int) $this->node_type) {
            if (null === $lang) {
                $translations = $this->getTranslations();
                $arr = reset($translations);
                $translation = $arr[$key];

            } else {
                $arr = $this->getTranslation($lang);
                $translation = $arr[$key];
            }

//			if (null === $translation) {
//				return smed_translate('Missing translation');
//			}

            return $translation;
        }

        if (null === $lang || $lang === bab_getLanguage()) {
            return $this->getCoreSitemapItemProperty($key);
        }

        return '';
    }

    /**
     * Node name
     * if node name is null, no label set on modificiation, get the orgiginal name from core node if the node is a link
     * @return string
     */
    public function getName($lang = null) {

        $name = $this->getLabelString('name', $lang);

        if (($name === null) && (!empty($this->target))) {
            $babSitemap = bab_siteMap::get();
            /* @var babSitemapItem bab_SitemapItem */
            $node = $babSitemap->getNodeById($this->target);
            if (isset($node)) {
                $sitemapItem = $node->getData();
                $name = $sitemapItem->name;
            }
        }
        return $name;
    }

    /**
     * Node description
     * if node description is null, no label set on modificiation, get the orgiginal description from core node if the node is a link
     * @return string
     */
    public function getDescription($lang = null) {

        $description = $this->getLabelString('description', $lang);

        if (($description === null) && (!empty($this->target))) {
            $babSitemap = bab_siteMap::get();
            /* @var babSitemapItem bab_SitemapItem */
            $node = $babSitemap->getNodeById($this->target);
            if (isset($node)) {
                $sitemapItem = $node->getData();
                $description = $sitemapItem->description;
            }
        }
        return $description;
    }

    /**
     * @return string
     */
    public function getUrl() {
        if (self::ISNEW === (int) $this->node_type) {
            return $this->url;
        }

        return $this->getCoreSitemapItemProperty('url');
    }

    /**
     * @return string
     */
    public function getPageTitle() {
        return $this->page_title;
    }

    /**
     * @return string
     */
    public function getPageKeywords() {
        return $this->page_keywords;
    }

    /**
     * @return string
     */
    public function getPageDescription() {
        return $this->page_description;
    }


    /**
     * Transforms a regular utf8 string in a humane-readable url-compatible string.
     *
     * @param $rewriteName
     *
     * @return string
     */
    public static function formatRewrittenName($rewriteName)
    {
        $rewriteName = bab_removeDiacritics($rewriteName);
        $rewriteName = strtolower($rewriteName);
        $rewriteName = str_replace(array(' ', '\''), array('-', '-'), $rewriteName);
        $rewriteName = preg_replace('/[^-0-9A-Za-z]/', '', $rewriteName);
        $rewriteName = trim($rewriteName, '-');

        return $rewriteName;
    }


    /**
     *
     * @return string
     */
    public function getRewriteName($compute = true)
    {
        if ($compute && empty($this->rewrite_name)) {
            $rewriteName = $this->getPageTitle();
            if (empty($rewriteName)) {
                $rewriteName = $this->getName();
            }

            $this->rewrite_name = self::formatRewrittenName($rewriteName);
        }

        return $this->rewrite_name;
    }


    /**
     * @return string
     */
    public function isPopup() {
        return $this->popup;
    }

    /**
     * @return bool
     */
    public function isMenuIgnore() {
        return (bool) $this->menuignore;
    }

    /**
     * @return bool
     */
    public function isBreadCrumbIgnore() {
        return (bool) $this->breadcrumbignore;
    }

    /**
     * @return bool
     */
    public function isRewriteIgnore() {
        return (bool) $this->rewriteignore;
    }

    /**
     * @return bool
     */
    public function isRewriteDisabled()
    {
        return (bool) $this->disabled_rewrite;
    }

    /**
     * @return bool
     */
    public function isTargetChildNodes()
    {
        return (bool) $this->targetchildnodes;
    }


    /**
     * Returns the target node id.
     *
     * @return string
     */
    public function getTargetId() {
        if (self::ISNEW === (int) $this->node_type) {
            return $this->target;
        }

        return $this->getCoreSitemapItemProperty('target');
    }


    /**
     *
     * @return bab_SitemapItem
     */
    public function getTarget() {

        if (self::ISNEW === (int) $this->node_type) {
            $target = $this->target;
        } else {
            $target = $this->getCoreSitemapItemProperty('target');
        }

        if (!empty($target)) {
            $babSitemap = bab_siteMap::get();
            $node = $babSitemap->getNodeById($target);
            if ($node) {
                return $node->getData();
            }
        }
        return null;
    }


    /**
     * @return string
     */
    public function getOnClick() {
        if (self::ISNEW === (int) $this->node_type) {

            if (1 === (int) $this->popup)
            {
                return 'bab_popup(this.href, 30, 1, 1, 1);return false;';
            }

            return '';
        }

        return $this->getCoreSitemapItemProperty('onclick');
    }



    public function getIconClassnames() {
        if (self::ISNEW === (int) $this->node_type) {
            return $this->classnames;
        }

        return $this->getCoreSitemapItemProperty('iconClassnames');
    }


    /**
     * language code
     * @return string
     */
    public function getSetLanguage() {
        if ('' === $this->setlanguage) {
            return null;
        }
        return $this->setlanguage;
    }


    /**
     * Insert data in nodes table if not exists
     * @return bool
     */
    public function insert() {
        global $babDB;


        if (null !== self::getFromId($this->id_sitemap, $this->id_function)) {
            return false;
        }


        $babDB->db_query('INSERT INTO smed_nodes
            (
                id_function,
                id_parent,
                id_sitemap,
                node_type,
                url,
                sort_type,
                classnames,
                disabled,
                page_title,
                page_keywords,
                page_description,
                rewrite_name,
                target,
                targetchildnodes,
                popup,
                menuignore,
                breadcrumbignore,
                rewriteignore,
                content_type,
                contenttypes,
                disabled_rewrite,
                setlanguage,
                langid
            )
            VALUES

            (
                '.$babDB->quote($this->id_function).',
                '.$babDB->quote($this->id_parent).',
                '.$babDB->quote($this->id_sitemap).',
                '.$babDB->quote($this->node_type).',
                '.$babDB->quote((string) $this->url).',
                '.$babDB->quoteOrNull($this->sort_type).',
                '.$babDB->quote($this->classnames).',
                '.$babDB->quote($this->disabled).',
                '.$babDB->quote((string) $this->page_title).',
                '.$babDB->quote((string) $this->page_keywords).',
                '.$babDB->quote((string) $this->page_description).',
                '.$babDB->quote((string) $this->rewrite_name).',
                '.$babDB->quote((string) $this->target).',
                '.$babDB->quote($this->targetchildnodes).',
                '.$babDB->quote($this->popup).',
                '.$babDB->quote($this->menuignore).',
                '.$babDB->quote($this->breadcrumbignore).',
                '.$babDB->quote($this->rewriteignore).',
                '.$babDB->quote($this->content_type).',
                '.$babDB->quote($this->contenttypes).',
                '.$babDB->quote($this->disabled_rewrite).',
                '.$babDB->quote((string) $this->setlanguage).',
                '.$babDB->quote((string) $this->langid).'
            )
        ');

        return true;
    }




    /**
     * Update data in table
     */
    public function update() {

        global $babDB;

        if (null === $this->sort_type) {
            $sort_type = 'NULL';
        } else {
            $sort_type = $babDB->quote($this->sort_type);
        }


        $sql = 'UPDATE smed_nodes
            SET
                id_parent 		 = '.$babDB->quote($this->id_parent).',
                node_type 		 = '.$babDB->quote($this->node_type).',
                url 			 = '.$babDB->quote($this->url).',
                sort_type 		 = '.$sort_type.',
                classnames		 = '.$babDB->quote($this->classnames).',
                disabled 		 = '.$babDB->quote($this->disabled).',
                page_title 		 = '.$babDB->quote((string) $this->page_title).',
                page_keywords    = '.$babDB->quote((string) $this->page_keywords).',
                page_description = '.$babDB->quote((string) $this->page_description).',
                rewrite_name 	 = '.$babDB->quote((string) $this->rewrite_name).',
                target 			 = '.$babDB->quote((string) $this->target).',
                targetchildnodes = '.$babDB->quote($this->targetchildnodes).',
                popup 			 = '.$babDB->quote($this->popup).',
                menuignore		 = '.$babDB->quote($this->menuignore).',
                breadcrumbignore = '.$babDB->quote($this->breadcrumbignore).',
                rewriteignore    = '.$babDB->quote($this->rewriteignore).',
                content_type 	 = '.$babDB->quote($this->content_type).',
                contenttypes 	 = '.$babDB->quote($this->contenttypes).',
                disabled_rewrite = '.$babDB->quote($this->disabled_rewrite).',
                setlanguage      = '.$babDB->quote((string) $this->setlanguage).',
                langid           = '.$babDB->quote((string) $this->langid).'

            WHERE

                id_sitemap = '.$babDB->quote($this->id_sitemap).'
                AND id_function = '.$babDB->quote($this->id_function).'
        ';

        $babDB->db_query($sql);

        return true;
    }



    /**
     * Get child nodes of a reference node, ignore non accessible items
     * @return array	<smed_Node>
     */
    protected function getRefChildNodes() {

        if (!$this->isTargetChildNodes()) {
            return array();
        }

        $core = bab_siteMap::get();
        $node = $core->getNodeById($this->getTargetId());

        if (!isset($node)) {
            // The node does not exists in core
            return array();
        }

        $node = $node->firstChild();

        if (null === $node) {
            return array();
        }

        $return = array();

        do {
            /*  @var $sitemapItem bab_siteMapItem */
            $sitemapItem = $node->getData();

            $ref = new smed_Node;

            $ref->id_function 		= $node->getId().'#'.uniqid();
            $ref->id_sitemap		= $this->id_sitemap;
            $ref->node_type			= self::ISNEW;
            $ref->id_parent			= $this->id_function;
            $ref->target			= $node->getId();
            $ref->targetchildnodes	= 1;
            $ref->content_type		= 'ref';
            $ref->sort_type			= $this->getSortType();
            $ref->classnames		= $sitemapItem->getIconClassnames();

            $return[$ref->getId()] = $ref;
        } while ($node = $node->nextSibling());

        return $return;
    }


    public function getSmedChildNodes()
    {
        global $babDB;

        //$arrC = $this->getCoreChildNodes();

        static $smedNodes = null;
        if($smedNodes === null) {
            $smedNodes = array();
            $query = '
                SELECT
                    n.*,
                    l.name,
                    l.description,
                    n.id_sitemap as id_sitemap,
                    n.id_parent as id_parent,
                    n.id_function as id_function
                FROM
                    smed_nodes n
                    LEFT JOIN smed_node_labels l ON
                        l.id_sitemap=n.id_sitemap
                        AND l.id_function=n.id_function
                        AND l.lang='.$babDB->quote(bab_getLanguage()).'
                    LEFT JOIN smed_sort s ON
                        s.id_function=n.id_function
                        AND s.id_parent=n.id_parent
            ';

            //if (self::SORT_FIXED_BOTTOM === $this->getSortType() || null === $this->getSortType()) {
                //$query .= ' ORDER BY s.sort_index, l.name ';
            //} else {
                $query .= ' ORDER BY l.name ';
            //}

            $res = $babDB->db_query($query);
            while($arr = $babDB->db_fetch_assoc($res)) {
                if(!isset($smedNodes[$arr['id_sitemap']])) {
                    $smedNodes[$arr['id_sitemap']] = array();
                }
                if(!isset($smedNodes[$arr['id_sitemap']][$arr['id_parent']])) {
                    $smedNodes[$arr['id_sitemap']][$arr['id_parent']] = array();
                }
                $smedNodes[$arr['id_sitemap']][$arr['id_parent']][] = smed_Node::getFromTable($arr);
            }
        }

        $return = array();

        /* @var $babSitemap bab_siteMapOrphanRootNode */
        $babSitemap = bab_siteMap::get();

        //$iterator = new smed_nodesIterator($res);
        if(isset($smedNodes[$this->id_sitemap]) && isset($smedNodes[$this->id_sitemap][$this->id_function])){
            foreach($smedNodes[$this->id_sitemap][$this->id_function] as $node) {

                /*@var $node smed_Node */

                if (!$node->canRead()) {
                    continue;
                }

                $targetId = $node->getTargetId();

                if (!empty($targetId)) {
                    $target = $babSitemap->getStaticNodeById($targetId);

                    if (!isset($target)) {
                        continue;
                    }
                }

                $return[$node->getId()] = $node;
            }

            if (self::SORT_FIXED_BOTTOM === $this->getSortType() || null === $this->getSortType()) {
                bab_Sort::sortObjects($return, 'getSortIndex');
            }
        }

        return $return;
    }


    /**
     * Get modified child nodes, ignore not accessible items
     * @return array	<smed_Node>
     */
    public function getModifiedChildNodes() {

        $return = $this->getSmedChildNodes();
        $return += $this->getRefChildNodes();

        return $return;
    }

    /**
     * get child items from core (only accessible items)
     * @return array <smed_Node>
     */
    public function getCoreChildNodes() {
        $core = bab_siteMap::get();
        $node = $core->getStaticNodeById($this->id_function); // use getStaticNodeById instead of getNodeById to prevent infinite loop if id_function does not exists in core sitemap

        if (!isset($node)) {
            // The node does not exists in core
            return array();
        }

        $node = $node->firstChild();

        if (null === $node) {
            return array();
        }



        $return = array();

        do {
            $return[$node->getId()] = self::getFromCoreNode($this->id_sitemap, $node);
        } while ($node = $node->nextSibling());



        return $return;
    }


    /**
     * Get merged child nodes (accessible items only)
     * @return array <smed_Node>
     */
    public function getChildNodes() {

        if (null === $this->childNodesCache) {


            $arrM = $this->getModifiedChildNodes();
            $arrC = $this->getCoreChildNodes();

            foreach($arrC as $key => $coreNode) {


                if (isset($arrM[$key])) {

                    if (1 === (int) $arrM[$key]->disabled || $coreNode->id_parent !== $arrM[$key]->id_parent) {

                        // if deleted or moved, remove from core list
                        unset($arrC[$key]);

                    } else {

                        // if modified, replace
                        $arrC[$key] = $arrM[$key];
                    }

                    unset($arrM[$key]);
                }
            }



            foreach($arrM as $key => $smedNode) {

                if (1 === (int) $smedNode->disabled) {
                    unset($arrM[$key]);
                }
            }

            // merged added nodes

            $this->childNodesCache = $arrC + $arrM;


            // sort

            if ($this->isSortManually()) {
                bab_Sort::sortObjects($this->childNodesCache, 'getSortIndex');
            }

            if (self::SORT_ALPHA === $this->sort_type) {
                bab_Sort::sortObjects($this->childNodesCache, 'getName');
            }

        }



        return $this->childNodesCache;
    }


    /**
     * Test if there is accessible child nodes
     * @return bool
     */
    public function hasChildNodes() {
        $arr = $this->getChildNodes();
        return (0 < count($arr));
    }


    /**
     * Test if there are accessible child node recursively without testing the containers nodes
     * @return bool
     */
    public function hasLeafNodes()
    {
        $nodes = $this->getChildNodes();
        if (0 === count($nodes))
        {
            return false;
        }

        foreach($nodes as $node)
        {
            /*@var $node smed_Node */
            if ('container' === $node->getContentType() && false === $node->hasLeafNodes())
            {
                // the node is accessible but without children, ignore
                continue;
            }

            // an accessible child, not a container
            return true;
        }

        return false;
    }


    /**
     * Get child nodes with link to a core node
     * @return array
     */
    private function getLinkedChildNodes() {

        $return = array();

        foreach($this->getChildNodes() as $key => $node) {
            if (self::ISMODIFIED !== (int) $node->node_type) {
                $return[$key] = $node;
            }
        }

        return $return;
    }





    /**
     * Record modification in modified childnodes list of references to core childnodes, no change to new nodes
     *
     *
     * @param	array	$arr	This parameter may contain regular core child nodes, they must be ignored
     * 							If one of the core child node is not present, it must be recorded as a deleted node.
     * 							This parameter may contain additional nodes to record
     *
     *
     * @see smed_Widgets::getCoreTree
     */
    public function recordModifiedChildNodes($arr) {

        $core = $this->getCoreChildNodes();
        $ongoing = $this->getModifiedChildNodes();

        if (!empty($arr)) {
            foreach($arr as $node_id => $dummy) {
                if (isset($core[$node_id])) {
                    // ignore

                    unset($arr[$node_id]);
                    unset($core[$node_id]);
                } else {

                    if (isset($ongoing[$node_id])) {

                        if (1 === (int) $ongoing[$node_id]->disabled) {
                            // the node exists in modifications but disabled,
                            // if recorded as a childnode of another node, move it back to me

                            $ongoing[$node_id]->id_parent = $this->id_function;
                            $ongoing[$node_id]->disabled = 0;
                            $ongoing[$node_id]->update();


                        } else {

                            // this node is allready recorded as an additional child node
                            unset($ongoing[$node_id]);
                        }

                    } else {

                        $this->addChildNode($node_id);
                    }
                }
            }
        }

        foreach($core as $node_id => $dummy) {

            if (isset($ongoing[$node_id])) {
                // this node is allready recorded as a deleted child node
                unset($ongoing[$node_id]);

            } else {
                $this->deleteChildNode($node_id);
            }
        }



        // clean unused links to core node

        foreach($ongoing as $node_id => $node) {
            if (self::ISNEW !== (int) $node->node_type) {

                $childNode = self::getFromId($this->id_sitemap, $node_id);
                $childNode->deleteReference();
            }
        }
    }











    /**
     * Add a core node ID to the list of references child nodes
     * If the node allready exist it will be dereferenced from the original position
     *
     * @param	string	$node_id	child node ID to add
     *
     */
    private function addChildNode($node_id) {
        global $babDB;

        if (null !== $node = self::getFromId($this->id_sitemap, $node_id)) {
            $node->deleteReference();
        }


        $babDB->db_query('
            INSERT INTO smed_nodes
                (
                    id_function,
                    id_parent,
                    id_sitemap,
                    node_type
                )
            VALUES
                (
                    '.$babDB->quote($node_id).',
                    '.$babDB->quote($this->id_function).',
                    '.$babDB->quote($this->id_sitemap).',
                    '.$babDB->quote(self::ISCORE).'
                )
        ');
    }


    /**
     * Add a core node ID to the list of deleted child nodes
     *
     * @param	string	$node_id	child node ID to delete
     *
     */
    private function deleteChildNode($node_id) {
        global $babDB;

        $babDB->db_query('
            INSERT INTO smed_nodes
                (
                    id_function,
                    id_parent,
                    id_sitemap,
                    node_type,
                    disabled
                )
            VALUES
                (
                    '.$babDB->quote($node_id).',
                    '.$babDB->quote($this->id_function).',
                    '.$babDB->quote($this->id_sitemap).',
                    '.$babDB->quote(self::ISCORE).',
                    '.$babDB->quote(1).'
                )
        ');
    }




    /**
     * Get array of node parameters for modification form
     * @return array
     */
    public function getFormValues() {

        global $babDB;

        $return = array(
            'id_function' 	=> $this->id_function,
            'node_type'		=> $this->node_type,
            'sort_type'		=> $this->sort_type,
            'languages'		=> array(),
            'url'			=> $this->url,
            'classnames'	=> $this->classnames,
            'nodes'			=> array(),
            'target'		=> $this->target,
            'content_type'	=> $this->content_type
        );

        // translations

        foreach(bab_getAvailableLanguages() as $lang) {

            $return['languages'][$lang] = array(
                'name' 			=> $this->getName($lang),
                'description' 	=> $this->getDescription($lang)
            );
        }


        // childnodes

        foreach($this->getLinkedChildNodes() as $node) {
            $return['nodes'][$node->getId()] = 1;
        }

        return $return;
    }



    /**
     * Return true if child nodes have to be sorted manually
     * @return	bool
     */
    public function isSortManually() {

        if (self::SORT_FIXED_TOP === $this->sort_type || self::SORT_FIXED_BOTTOM === $this->sort_type) {
            return true;
        }

        return false;
    }





    /**
     * Delete row from table for any type of node and recursively for custom nodes
     *
     */
    public function deleteReference() {
        global $babDB;

        $this->removeLabels();

        $res = $babDB->db_query('
            SELECT id_function FROM smed_nodes
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_parent='.$babDB->quote($this->id_function).'
                AND node_type='.$babDB->quote(self::ISNEW).'
        ');

        while ($arr = $babDB->db_fetch_assoc($res)) {

            if ($node = $this->getFromId($this->id_sitemap, $arr['id_function'])) {
                $node->deleteReference();
            }
        }


        $babDB->db_query('
            DELETE FROM
                smed_nodes
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_function='.$babDB->quote($this->id_function)
        );
    }




    /**
     * Disable row from table
     *
     */
    public function disableReference() {
        global $babDB;

        $babDB->db_query('
            UPDATE
                smed_nodes
            SET
                disabled=\'1\'
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_function='.$babDB->quote($this->id_function)
        );
    }


    /**
     * Remove all labels associated to a node
     *
     */
    private function removeLabels() {

        global $babDB;

        $babDB->db_query('
            DELETE FROM smed_node_labels
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_function='.$babDB->quote($this->id_function).'
        ');

    }






    private function recordLabels($labels) {

        global $babDB;

        $res = $babDB->db_query('SELECT * FROM smed_node_labels
            WHERE
                id_sitemap='.$babDB->quote($this->id_sitemap).'
                AND id_function='.$babDB->quote($this->id_function).'
        ');

        while ($arr = $babDB->db_fetch_assoc($res)) {
            if (isset($labels[$arr['lang']])) {

                $new_name 		 = trim($labels[$arr['lang']]['name']);
                $new_description = trim($labels[$arr['lang']]['description']);

                unset($labels[$arr['lang']]);


                if ($new_name || $new_description) {
                    $babDB->db_query('UPDATE smed_node_labels
                            SET
                                name 		= '.$babDB->quote($new_name).',
                                description = '.$babDB->quote($new_description).'
                        WHERE
                            id='.$babDB->quote($arr['id']).'
                    ');

                    continue;
                }

            }


            $babDB->db_query('DELETE FROM smed_node_labels
                WHERE id='.$babDB->quote($arr['id']).'
            ');
        }


        foreach($labels as $lang => $arr) {
            $new_name 		 = trim($arr['name']);
            $new_description = trim($arr['description']);

            if ($new_name || $new_description) {
                $babDB->db_query('
                    INSERT INTO smed_node_labels
                        (id_sitemap, id_function, lang, name, description)
                    VALUES
                        (
                            '.$babDB->quote($this->id_sitemap).',
                            '.$babDB->quote($this->id_function).',
                            '.$babDB->quote($lang).',
                            '.$babDB->quote($new_name).',
                            '.$babDB->quote($new_description).'
                        )
                ');

            }
        }
    }





    /**
     * Test if a node has been modified from core
     * the node must be of type self::ISCORE or a default node (not created in modification list)
     *
     * @param	string	$id_parent
     * @param	array	$childnodes
     * @param	int		$sort_type
     *
     * @return bool
     */
    private function isModified($id_parent, $childnodes, $sort_type) {

        if (self::ISCORE !== (int) $this->node_type && self::ISDEFAULT !== (int) $this->node_type) {
            throw new Exception('Incorrect type of node');
        }


        if ($id_parent !== $this->id_parent) {
            return true;
        }


        if ($sort_type !== (int) $this->sort_type) {
            return true;
        }

        if (1 === (int) $this->disabled) {
            return true;
        }

        // the node may be registered because the parent is a modified node
        if (null !== self::getFromId($this->id_sitemap, $id_parent)) {
            // the parent exists
            return true;
        }

        foreach($this->getCoreChildNodes() as $coreChildNode => $dummy) {
            if (!isset($childnodes[$coreChildNode])) {
                return true;
            } else {
                unset($childnodes[$coreChildNode]);
            }
        }

        if (!empty($childnodes)) {
            return true;
        }

        return false;
    }




    /**
     * Create or update a node from the form
     * parameters are allready validated
     *
     * @param	int		$id_sitemap
     * @param	string	$id_parent			NODE ID
     * @param	string	$id_function		NODE ID
     * @param	int		$node_type			smed_node::ISCORE | smed_node::ISMODIFIED | smed_node::ISNEW
     * @param	string	$url
     * @param	array	$childnodes			list of core node ID
     * @param	array	$labels				Labels by languages
     * @param	string	$sort_type			childnodes sort method
     * @param	string	$classnames			icon class
     * @param 	string	$target				Target function id
     * @param	int		$targetchildnodes	1: the target and childnodes, 0: only the target node
     * @param	string	$content_type		content type of node : container|topic|category|url|ref
     * @param 	string	$page_title			Page title for referencing
     * @param 	string	$page_keywords		Page keywords for referencing
     * @param 	string	$page_description	Page description for referencing
     * @param	string	$rewrite_name
     * @param 	string	$popup				1 if should open in a new window.
     * @param	int		$disabled_rewrite	1 | 0
     * @param  string	$menuignore			1 if should ignored in SitemapMenu.
     * @param  string	$breadcrumbignore	1 if should ignored in SitemapPosition.
     * @param  string   $rewriteignore      1 if should ignored in rewritepath
     * @param  string   $setlanguage        language code
     * @param  string   $langid             language node id
     */
    public static function record($id_sitemap, $id_parent, $id_function, $node_type, $url, $childnodes, $labels, $sort_type, $classnames, $target = '', $targetchildnodes = 0, $content_type='', $page_title = '', $page_keywords = '', $page_description = '', $rewrite_name = '',  $popup = 0, $disabled_rewrite = 0, $menuignore = 0, $breadcrumbignore = 0, $rewriteignore = 0, $setlanguage = null, $langid = null) {


        if (empty($sort_type)) {
            $sort_type = self::SORT_UNORDERED;
        }


        // search the node
        if (null === $node = self::getFromId($id_sitemap, $id_function)) {

            // no modification exists, create the node

            $parent = smed_Node::get($id_sitemap, $id_parent);

            switch($node_type) {

                case self::ISCORE:

                    $coreNode = self::getFromCore($id_sitemap, $id_function);

                    if (!$coreNode || false === $coreNode->isModified($id_parent, $childnodes, $sort_type)) {
                        // do not create the node if it is not modified from core
                        return;
                    }

                    // warning : no break

                case self::ISMODIFIED:

                    $node = self::getFromCore($id_sitemap, $id_function);
                    $node->id_parent 		= $id_parent;
                    $node->node_type 		= $node_type;
                    $node->sort_type		= $sort_type;
                    $node->page_title		= $page_title;
                    $node->page_keywords	= $page_keywords;
                    $node->page_description	= $page_description;
                    $node->rewrite_name		= $rewrite_name;
                    $node->target			= $target;
                    $node->targetchildnodes = $targetchildnodes;
                    $node->popup			= $popup;
                    $node->menuignore		= $menuignore;
                    $node->breadcrumbignore	= $breadcrumbignore;
                    $node->rewriteignore	= $rewriteignore;
                    $node->content_type		= $content_type;
                    $node->disabled_rewrite	= $disabled_rewrite;
                    $node->setlanguage      = $setlanguage;
                    $node->langid           = $langid;
                    $node->insert();

                    break;

                case self::ISNEW:

                    $node = new smed_Node;
                    $node->id_sitemap 		= $id_sitemap;
                    $node->id_parent 		= $id_parent;
                    $node->id_function 		= $id_function;
                    $node->node_type 		= $node_type;
                    $node->sort_type		= $sort_type;
                    $node->url		 		= $url;
                    $node->classnames		= $classnames;
                    $node->page_title		= $page_title;
                    $node->page_keywords	= $page_keywords;
                    $node->page_description	= $page_description;
                    $node->rewrite_name		= $rewrite_name;
                    $node->target			= $target;
                    $node->targetchildnodes = $targetchildnodes;
                    $node->popup			= $popup;
                    $node->menuignore		= $menuignore;
                    $node->breadcrumbignore	= $breadcrumbignore;
                    $node->rewriteignore	= $rewriteignore;
                    $node->content_type		= $content_type;
                    $node->disabled_rewrite	= $disabled_rewrite;
                    $node->setlanguage      = $setlanguage;
                    $node->langid           = $langid;
                    $node->contenttypes     = $parent->contenttypes;
                    $node->insert();
                    break;
            }


            // add sort number if necessary


            if (smed_Node::SORT_FIXED_BOTTOM === $parent->getSortType())
            {
                $node->setSortIndex(1 + $parent->getMaxChildSort());
            }


            if (isset($parent)) {
                $node->setRightsOnInsert($parent);
            }


        } else {

            // if modification of parent, cleanup sort entry

            if ($id_parent !== $node->getParentId())
            {
                global $babDB;

                // delete current sort entries

                $babDB->db_query('DELETE FROM smed_sort
                    WHERE
                        id_sitemap='.$babDB->quote($id_sitemap).'
                        AND id_parent='.$babDB->quote($node->getParentId()).'
                        AND id_function='.$babDB->quote($id_function)
                );
            }



            // if the node is a core node and is not modified, delete the reference


            if (self::ISCORE === (int) $node_type && false === $node->isModified($id_parent, $childnodes, $sort_type)) {

                $node->deleteReference();
                bab_debug('DELETE');

            } else {

                $node->id_parent 		= $id_parent;
                $node->node_type 		= $node_type;
                $node->sort_type		= $sort_type;
                $node->url		 		= $url;
                $node->classnames		= $classnames;
                $node->page_title		= $page_title;
                $node->page_keywords	= $page_keywords;
                $node->page_description	= $page_description;
                $node->rewrite_name		= $rewrite_name;
                $node->target			= $target;
                $node->targetchildnodes = $targetchildnodes;
                $node->popup			= $popup;
                $node->menuignore		= $menuignore;
                $node->breadcrumbignore	= $breadcrumbignore;
                $node->rewriteignore	= $rewriteignore;
                $node->content_type		= $content_type;
                $node->disabled_rewrite	= $disabled_rewrite;
                $node->setlanguage      = $setlanguage;
                $node->langid           = $langid;
                $node->update();

                bab_debug('UPDATE');

            }
        }


        // update labels

        switch($node_type) {

            case self::ISMODIFIED:
            case self::ISNEW:

                $node->recordLabels($labels);

                break;

            default:
                $node->removeLabels();
                break;
        }


        // update childnodes
        if (isset($childnodes)) {
            $node->recordModifiedChildNodes($childnodes);
        }
    }



    /**
     * Create a container with default content types allowed
     *
     */
    public static function createContainer($id_sitemap, $parent, $nodeId, $name, $description = null)
    {
        $lang = bab_getLanguage();

        $node = new smed_Node;
        $node->id_sitemap 		= $id_sitemap;
        $node->id_parent 		= $parent;
        $node->id_function 		= $nodeId;
        $node->node_type 		= smed_Node::ISNEW;
        $node->breadcrumbignore	= 1;
        $node->rewriteignore	= 1;
        $node->content_type		= 'container';
        $node->contenttypes     = 'container,topic,url,ref';
        $node->insert();

        $node->recordLabels(array(
            $lang => array('name' => $name, 'description' => $description)
        ));
    }




    /**
     * Test if the node is a core node moved in tree
     * @return bool
     */
    public function isMovedReference() {

        if (self::ISNEW === $this->getNodeType()) {
            return false;
        }

        $coreNode = self::getFromCore($this->id_sitemap, $this->id_function);

        if (!isset($coreNode)) {
            throw new Exception('The node must be a reference to a core node');
        }

        if ($this->getParentId() === $coreNode->getParentId()) {
            return false;
        }

        return true;
    }


    /**
     * Get path of core node
     * @return string
     */
    public function getOriginalPath() {

        $sitemap = bab_siteMap::get();
        $coreNode = $sitemap->getNodeById($this->id_function);

        if (!isset($coreNode)) {
            return '';
        }

        $return = '';

        while ($coreNode = $coreNode->parentNode()) {

            if ($siteMapItem = $coreNode->getData()) {
                $return = '/'.$siteMapItem->name .$return;
            }
        }

        return $return;
    }






    /**
     *
     * @param array $list
     * @return unknown_type
     */
    public function setContentTypes(Array $list)
    {
        $this->contenttypes = implode(',', $list);
        return $this;
    }



    /**
     *
     * @return array
     */
    public function getContentTypes()
    {
        if ('' === $this->contenttypes) {
            return array();
        }

        return explode(',', $this->contenttypes);
    }


    /**
     * Allowed content types in subnodes creation
     * @return string[]
     */
    public function getContentTypesLabels()
    {
        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */
        $availableTypes = $defaultFunc->getAllContentTypes();

        $arr = $this->getContentTypes();
        $list = array();
        foreach ($arr as $ct) {
            if (isset($availableTypes[$ct])) {
                $infos = $availableTypes[$ct];
                $list[] = $infos['description'];
            }
        }

        return $list;
    }


    /**
     *
     * @param string $type
     * @return unknown_type
     */
    public function setContentType($type)
    {
        $this->content_type = $type;
        return $this;
    }



    /**
     *
     * @return string
     */
    public function getContentType()
    {
        if ('' === $this->content_type) {
            $this->content_type = 'container';
        }

        return $this->content_type;
    }







    /**
     * @return string
     */
    public function getContentTypeLabel()
    {
        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */
        $availableTypes = $defaultFunc->getAllContentTypes();

        $infos = $availableTypes[$this->getContentType()];

        return $infos['description'];
    }

    /**
     * @return string
     */
    public function getContentTypeClassname()
    {
        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */
        $availableTypes = $defaultFunc->getAllContentTypes();

        $infos = $availableTypes[$this->getContentType()];

        return $infos['className'];

    }


    /**
     * Test if the content type allow modification of the target node rights in sitemap editor
     * @return bool
     */
    public function canEditRights()
    {
        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */

        if (!$defaultFunc) {
            return false;
        }

        $availableTypes = $defaultFunc->getAllContentTypes();
        $nodeContentType = $this->getContentType();

        if (!isset($availableTypes[$nodeContentType])) {
            trigger_error(sprintf(
                'Unavailable content type %s, available content types are: %s',
                $nodeContentType,
                implode(', ', array_keys($availableTypes))
            ));
            return false;
        }

        $infos = $availableTypes[$nodeContentType];

        return $infos['functionality']->canEditRights();
    }


    /**
    * Upload path for node file attachments
    *
    * @return bab_Path
    */
    public function uploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $path = new bab_Path(bab_getAddonInfosInstance('sitemap_editor')->getUploadPath());

        $path->push('node');
        $path->push($this->getId());

        return $path;
    }





    /**
     * Get sitemap Item
     * @return bab_siteMapItem
     */
    public function getSitemapItem()
    {


        /* @var $babSitemap bab_siteMapOrphanRootNode */
        $babSitemap = bab_siteMap::get();

        if ($this->node_type === self::ISDEFAULT)
        {
            // unmodified node
            $node = $babSitemap->getNodeById($this->getId());
            if (!isset($node)) {
                return null;
            }
            return $node->getData();
        }


        $data = new bab_siteMapItem();
        $data->id_function 		= $this->getId();
        $data->target			= $this->getTarget();
        $data->setlanguage      = $this->getSetLanguage();


        if (isset($this->langid)) {
            $data->langId       = array($this->getLanguage(), $this->langid);
        }

        $targetId = $this->getTargetId();

        if (!empty($targetId)) {
            $target = $babSitemap->getNodeById($targetId);

            if (!isset($target)) {
                bab_debug($targetId.' not found in core sitemap, this exit should not happen because merged nodelist contain only accessible items');
                return $data;
            }

            $targetData 	= $target->getData();
            /*@var $targetData bab_siteMapItem */

            // if target, the rights are set on target node
            $targetData->rights = $this->getAccessRights();

            if ('category' === $this->getContentType() && $this->getUrl() != '') {
                $data->url 		= $this->getUrl();
            } else {

                $data->target	= $targetData;
                $data->url 		= $targetData->url;
                $data->funcname	= $targetData->funcname;
            }

        } else {
            $data->url 		= $this->getUrl();
            // no target, we apply right on the modified node
            $data->rights   = $this->getAccessRights();
        }

        if (null === $data->url)
        {

            return $data;
        }

        $data->name 			= $this->getName();
        $data->description 		= $this->getDescription();
        $data->onclick 			= $this->getOnClick();
        $data->folder 			= $this->hasChildNodes();
        $data->iconClassnames	= $this->getIconClassnames();

        $data->pageDescription 	= $this->getPageDescription();
        $data->pageTitle 		= $this->getPageTitle();
        $data->pageKeywords 	= $this->getPageKeywords();

        $data->disabledRewrite 	= $this->isRewriteDisabled();
        $data->rewriteName 		= $this->getRewriteName();
        $data->menuIgnore		= $this->isMenuIgnore();
        $data->breadCrumbIgnore	= $this->isBreadCrumbIgnore();
        $data->rewriteIgnore	= $this->isRewriteIgnore();


        return $data;
    }








    /**
     * @return bool
     */
    public function canCreate()
    {
        $types = $this->getContentTypes();

        if (0 === count($types)) {
            return false;
        }

        $target = $this->getTarget();

        if (!isset($target) || !$this->canEditRights()) {
            // this is for nodes types with no rights set
            // container, links, etc..
            return bab_isUserAdministrator();
        }

        return $target->canCreate();
    }

    /**
     * @return bool
     */
    public function canRead()
    {
        $target = $this->getTarget();

        if (!isset($target) || !$this->canEditRights()) {
            return true;
        }


        return $target->canRead();
    }

    /**
     * @return bool
     */
    public function canUpdate()
    {
        $target = $this->getTarget();

        if (!isset($target) || !$this->canEditRights()) {
            //TODO: add a custom right to update all the folders in sitemap editor
            return bab_isUserAdministrator();
        }

        return $target->canUpdate();
    }

    /**
     * @return bool
     */
    public function canDelete()
    {
        $target = $this->getTarget();

        if (!isset($target) || !$this->canEditRights()) {
            return $this->canUpdate();
        }

        return $target->canDelete();
    }




    public function getCreate()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->getCreate();
    }

    public function setCreate($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->setCreate($value);
    }

    public function getRead()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->getRead();
    }

    public function setRead($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->setRead($value);
    }

    public function getUpdate()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->getUpdate();
    }

    public function setUpdate($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->setUpdate($value);
    }


    public function getDelete()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->getDelete();
    }

    public function setDelete($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            return null;
        }
        return $target->setDelete($value);
    }




    private function compatibility()
    {
        throw new bab_SaveErrorException('This functionality require ovidentia >= 8.6.0');
    }



    public function getDefaultCreate()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'getDefaultCreate')) {
                return BAB_ADMINISTRATOR_GROUP;
            }
            return $item->getDefaultCreate();
        }

        if (!method_exists($target, 'getDefaultCreate')) {
            return BAB_ADMINISTRATOR_GROUP;
        }
        return $target->getDefaultCreate();
    }

    public function setDefaultCreate($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'setDefaultCreate')) {
                $this->compatibility();
            }
            return $item->setDefaultCreate($value);
        }

        if (!method_exists($target, 'setDefaultCreate')) {
            $this->compatibility();
        }
        return $target->setDefaultCreate($value);
    }

    public function getDefaultRead()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'getDefaultRead')) {
                return BAB_ALLUSERS_GROUP;
            }
            return $item->getDefaultRead();
        }

        if (!method_exists($target, 'getDefaultRead')) {
            return BAB_ALLUSERS_GROUP;
        }
        return $target->getDefaultRead();
    }

    public function setDefaultRead($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'setDefaultRead')) {
                $this->compatibility();
            }
            return $item->setDefaultRead($value);
        }

        if (!method_exists($target, 'setDefaultRead')) {
            $this->compatibility();
        }
        return $target->setDefaultRead($value);
    }

    public function getDefaultUpdate()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'getDefaultUpdate')) {
                return BAB_ADMINISTRATOR_GROUP;
            }
            return $item->getDefaultUpdate();
        }

        if (!method_exists($target, 'getDefaultUpdate')) {
            return BAB_ADMINISTRATOR_GROUP;
        }
        return $target->getDefaultUpdate();
    }

    public function setDefaultUpdate($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'setDefaultUpdate')) {
                $this->compatibility();
            }
            return $item->setDefaultUpdate($value);
        }

        if (!method_exists($target, 'setDefaultUpdate')) {
            $this->compatibility();
        }
        return $target->setDefaultUpdate($value);
    }


    public function getDefaultDelete()
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'getDefaultDelete')) {
                return BAB_ADMINISTRATOR_GROUP;
            }
            return $item->getDefaultDelete();
        }

        if (!method_exists($target, 'getDefaultDelete')) {
            return BAB_ADMINISTRATOR_GROUP;
        }
        return $target->getDefaultDelete();
    }

    public function setDefaultDelete($value)
    {
        $target = $this->getTarget();
        if (!isset($target)) {
            $item = $this->getSitemapItem();
            if (!method_exists($item, 'setDefaultDelete')) {
                $this->compatibility();
            }
            return $item->setDefaultDelete($value);
        }

        if (!method_exists($target, 'setDefaultDelete')) {
            $this->compatibility();
        }
        return $target->setDefaultDelete($value);
    }








    public function getAccessRights()
    {
        return array(
            'create' => $this->canCreate(),
            'read' => $this->canRead(),
            'update' => $this->canUpdate(),
            'delete' => $this->canDelete()
        );
    }

    public function getRights()
    {
        return array(
            'create' => $this->getCreate(),
            'read' => $this->getRead(),
            'update' => $this->getUpdate(),
            'delete' => $this->getDelete()
        );
    }

    public function getRightsDefault()
    {
        return array(
            'create' => $this->getDefaultCreate(),
            'read' => $this->getDefaultRead(),
            'update' => $this->getDefaultUpdate(),
            'delete' => $this->getDefaultDelete()
        );
    }

    public function setRights(Array $rights)
    {
        if (!$this->canEditRights()) {
            return;
        }

        if (isset($rights['create'])) {
            $this->setCreate($rights['create']);
        }

        if (isset($rights['read'])) {
            $this->setRead($rights['read']);
        }

        if (isset($rights['update'])) {
            $this->setUpdate($rights['update']);
        }

        if (isset($rights['delete'])) {
            $this->setDelete($rights['delete']);
        }
    }



    public function setRightsDefault(Array $rights)
    {
        if (isset($rights['create'])) {
            $this->setDefaultCreate($rights['create']);
        }

        if (isset($rights['read'])) {
            $this->setDefaultRead($rights['read']);
        }

        if (isset($rights['update'])) {
            $this->setDefaultUpdate($rights['update']);
        }

        if (isset($rights['delete'])) {
            $this->setDefaultDelete($rights['delete']);
        }
    }


    protected function isEmpty(Array $rights)
    {
        foreach ($rights as $name => $v) {
            if ('' !== $v) {
                return false;
            }
        }

        return true;
    }


    /**
     * Set rights on insert
     * if default rights are defined use default else use parent right
     * allways set update and delete if not set
     */
    public function setRightsOnInsert(smed_Node $parent)
    {

        $default = $parent->getRightsDefault();
        if (!$this->isEmpty($default)) {

            $this->setRights($default);
            $this->setRightsDefault($default);
            return;
        }


        if (!$parent->canEditRights()) {
            $this->setRead(BAB_ALLUSERS_GROUP);
        } else {
            $this->setRights($parent->getRights());
        }

        // ensure that the created node is modifiable/deletable by the author

        if (!$this->canUpdate()) {
            $this->setUpdate($parent->getCreate());
        }

        if (!$this->canDelete()) {
            $this->setDelete($parent->getCreate());
        }

    }




    public function saveRightsRecursive(Array $rights)
    {
        require_once dirname(__FILE__).'/sitemap.class.php';

        $this->setRights($rights);
        $iterator = smed_Sitemap::getModifiedIterator($this->getId());
        foreach ($iterator as $node) {
            $node->saveRightsRecursive($rights);
        }
    }


    public function saveContentTypesRecursive(Array $contentTypes)
    {
        require_once dirname(__FILE__).'/sitemap.class.php';

        $this->setContentTypes($contentTypes);
        $this->update();

        $iterator = smed_Sitemap::getModifiedIterator($this->getId());
        foreach ($iterator as $node) {
            $node->saveContentTypesRecursive($contentTypes);
        }
    }


    /**
     * Change node ID
     * @param string $newId
     */
    public function updateIdFunction($newId)
    {
        $oldId = $this->getId();

        $rights = $this->getRights();

        global $babDB;

        $babDB->db_query('LOCK TABLES
            smed_nodes WRITE,
            smed_node_labels WRITE,
            smed_sort WRITE
        ');

        $this->id_function = $newId;

        $babDB->db_query('UPDATE smed_nodes SET id_function='.$babDB->quote($newId).'
            WHERE id_function='.$babDB->quote($oldId).'
                AND id_sitemap='.$babDB->quote($this->id_sitemap));

        $babDB->db_query('UPDATE smed_nodes SET id_parent='.$babDB->quote($newId).'
            WHERE id_parent='.$babDB->quote($oldId).'
                AND id_sitemap='.$babDB->quote($this->id_sitemap));

        $babDB->db_query('UPDATE smed_node_labels SET id_function='.$babDB->quote($newId).'
            WHERE id_function='.$babDB->quote($oldId).'
                AND id_sitemap='.$babDB->quote($this->id_sitemap));

        $babDB->db_query('UPDATE smed_sort SET id_function='.$babDB->quote($newId).'
            WHERE id_function='.$babDB->quote($oldId).'
                AND id_sitemap='.$babDB->quote($this->id_sitemap));

        $babDB->db_query('UNLOCK TABLES');

        $this->setRights($rights);
    }
}
