<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_DrilldownMenu');


/**
 * Constructs a Widget_SitemapItemPicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SitemapItemPicker
 */
function smed_NodePicker($id = null)
{
    return new smed_NodePicker($id);
}


/**
 * A Widget_SitemapItemPicker is a widget that let the user select an ovidentia sitemap item.
 *
 *
 */
class smed_NodePicker extends Widget_DrilldownMenu implements Widget_Displayable_Interface
{
    /**
     * @var string
     */
    protected $sitemapId = null;

    /**
     * @var string
     */
    protected $baseNode = null;

    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
        $this->setMetadata('crumbDefaultText', widget_translate('Choose a sitemap item'));
    }

    public function basenode($node)
    {
        $this->baseNode = $node;
        return $this;
    }


    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-sitemapitempicker';

        return $classes;
    }


    public function display(Widget_Canvas $canvas)
    {
        $datasource = new bab_url('?tg=addon/sitemap_editor/nodepicker&idx=node');

        if (isset($this->baseNode))
        {
            $datasource->basenode = $this->baseNode;
        }

        $this->setDatasource($datasource);

        $value = $this->getValue();
        if ($value) {
            $root = smed_Sitemap::get(1);
            $node = $root->getNodeById($value);
            if ($node === null) {
                $this->setTitle($value);
            } else {
                $this->setTitle($node->getData()->name);
            }
        }  else {
            $this->setTitle(smed_translate('No node selected'));
        }

        return parent::display($canvas);
    }

}
