<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

//require_once dirname(__FILE__) . '/functions.php';

bab_functionality::get('Widgets')->includePhpClass('Widget_Action');


class smed_BreadCrumbs
{
	
	/**
	 * Sets the new position in the beadcrumb history.
	 * If an element in the current breadcrumbs corresponds to the same action method,
	 * the breadcrumb will go back to this position.
	 * Only the action method is taken into consideration.
	 *
	 * @param Widget_Action $action
	 * @param string $label
	 */
	public static function setCurrentPosition(Widget_Action $action, $label)
	{
		if (!isset($_SESSION['SitemapEditorBreadCrumbs'])) {
			$_SESSION['SitemapEditorBreadCrumbs'] = array();
		}
		$breadcrumbs = $_SESSION['SitemapEditorBreadCrumbs'];

		$newKey = /*$action->getObject() . '::' . */$action->getMethod();
		
		$newBreadCrumbs = array();
		foreach ($breadcrumbs as $key => $value) {
			if ($key == $newKey) {
				 break;
			}
			$newBreadCrumbs[$key] = $value;
		}
		$newBreadCrumbs[$newKey] = array('url' => $action->url(), 'label' => $label);
		
		$_SESSION['SitemapEditorBreadCrumbs'] = $newBreadCrumbs;
	}


	public static function clear()
	{
		$_SESSION['SitemapEditorBreadCrumbs'] = array();
	}


	public static function pop()
	{
		return array_pop($_SESSION['SitemapEditorBreadCrumbs']);
	}


	public static function lastAction()
	{
		$lastElement = end($_SESSION['SitemapEditorBreadCrumbs']);
		return $lastElement['action'];
	}


	public static function last()
	{
		if (!isset($_SESSION['SitemapEditorBreadCrumbs'])) {
			return null;
		}
		
		$lastElement = end($_SESSION['SitemapEditorBreadCrumbs']);
		if ($lastElement === false) {
			return null;
		}
		return $lastElement['url'];
	}


	public function get($index)
	{
		if (!isset($_SESSION['SitemapEditorBreadCrumbs'])) {
			return null;
		}
		if ($index < 0 || $index >= count($_SESSION['SitemapEditorBreadCrumbs'])) {
			return null;
		}
//		return $_SESSION['SitemapEditorBreadCrumbs'][]
	}
}

