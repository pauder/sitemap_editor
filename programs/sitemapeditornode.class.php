<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


class Func_SitemapEditorNode extends bab_functionality
{
    
    /**
     * @var array
     */
    private static $allContentTypes;
    
    
    public function getDescription()
    {
        return smed_translate('Functionality to add new node types in sitemap editor');
    }
    
    
    public function getClassName()
    {
        return Func_Icons::PLACES_FOLDER;
    }
    
    /**
     * Test if the content type allow modification of the target node rights in sitemap editor
     * @return boolean
     */
    public function canEditRights()
    {
        return false;
    }
    
    
    /**
     * Get content types proposed by the functionality
     * the default is the functionality description
     * 
     * @return array
     */
    public function getContentTypes()
    {
        bab_functionality::includeOriginal('Icons');
        
        $arr = explode('_', get_class($this));
        unset($arr[0]);
        unset($arr[1]);
        $defaultNodeType = mb_strtolower(implode('/', $arr));
        
        return array(
            $defaultNodeType => array(
                'description' => $this->getDescription(),
                'className' => $this->getClassName()
            )
        );
    }
    
    
    /**
     * Internal method used by sitemap_editor to get the list of content types when a new node need to be created
     * @return array
     */
    public function getAllContentTypes()
    {
        if (!isset(self::$allContentTypes)) {
        
            self::$allContentTypes = array();
            $list = bab_functionality::getFunctionalities('SitemapEditorNode');
             
            $allContentTypes = array();
            foreach ($list as $funcName) {
                $nodeFunc = bab_functionality::get('SitemapEditorNode/'.$funcName);
                /*@var $nodeFunc Func_SitemapEditorNode */
                
                $ct = $nodeFunc->getContentTypes();
                
                foreach ($ct as $content_type => $arr) {
                    
                    $allContentTypes[$content_type] = array(
                        'functionality' => $nodeFunc,
                        'description' => $arr['description'],
                        'className' => $arr['className']
                    );
                }            
            }
            
            self::$allContentTypes = $allContentTypes;
        }

        return self::$allContentTypes;
    }
    
    
    /**
     * Internal method used by sitemap_editor to get page title
     * @return string
     */
    public function getContentTypeDescription($content_type)
    {
        $types = $this->getContentTypes();
        return $types[$content_type]['description'];
    }
    
    
    
    /**
     * Return false to disable the node type in the proposed types for node creation
     * @return boolean
     */
    public function isActive()
    {
        return true;
    }
    
    
    /**
     * Include node editor base class
     */
    protected function includeNodeEditor()
    {
        require_once dirname(__FILE__).'/nodeeditor.ui.php';
    }
    
    
    /**
     * Get interface to display in node edit mode
     * @param string $content_type one of the content type from the getContentTypes() method
     * @param smed_Node $node
     * @return smed_NodeEditor
     */
    public function getEditor($content_type, smed_Node $node = null)
    {
        $this->includeNodeEditor();
        return new smed_NodeEditor(null, null, $node);
    }
    
    
    /**
     * Get interface to display in node display mode
     * @return Widget_Displayable_Interface
     */
    public function display(smed_Node $node)
    {
        $editor = $this->getEditor($node->getContentType(), $node);
        $editor->addCustomSections($node->getContentType());
        $editor->setDisplayMode();
        
        return $editor;
    }
    
    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     * 
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    public function preSave(Array &$node, $nodeExists)
    {
        
    }
    
    /**
     * After sitemap node is saved
     * @param smed_Node $node       
     * @param array $values         Form posted node
     * @param mixed preSave         REturn value of the preSave method
     */
    public function postSave(smed_Node $node, Array $values, $preSave)
    {
        
    }
    
    
    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();
        
        $element->setIcon($nodeImagePath . '/nodes/folder.png');
    }
}