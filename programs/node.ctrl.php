<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/sitemap.class.php';
require_once dirname(__FILE__) . '/base.ui.php';
require_once dirname(__FILE__) . '/node.ui.php';
require_once dirname(__FILE__) . '/breadcrumbs.class.php';





/**
 * This controller manages actions that can be performed on sitemap nodes.
 */
class smed_CtrlNode extends smed_Controller
{


    protected function getEditor(smed_Node $node)
    {
        $type = $node->getContentType();

        $nodeFunc = smed_getNodeFunctionality($type);

        $editor = $nodeFunc->getEditor($type, $node);
        $editor->addIdFunction();
        $editor->addCustomSections($type);


        return $editor;
    }
    


    /**
     * @param smed_Node $node
     * 
     *
     * @return Widget_BoxLayout
     */
    protected function nodeHeader(smed_Node $node)
    {
        $W = smed_Widgets();


        $link = null;


        $url = bab_siteMap::url($node->id_function);

        if (!empty($url)) {
            $link = $W->Link($url, $url);
        }

        $target = null;
        $targetId = $node->getTargetId();

        if (!empty($targetId)) {
            $target = $W->Label(sprintf(smed_translate('Target: %s'), $targetId));
        }
        
        
        $language = $node->getLanguage();
        $langid = $node->getLangId();
        
        $sitemapXml = null;
        if ($node->hasChildNodes()) {
            $sitemapXml = $W->Link(smed_translate('sitemap.xml'), $this->proxy()->getXmlSitemap($node->getId()));
        }

        $header = $W->FlowItems(
            $W->Icon('', $node->getContentTypeClassname()),
            $W->VBoxItems(
                $W->Title($node->getName($language) , 2),
                $W->Title($node->getContentTypeLabel(), 6),
                $W->Label('Id: ' . $node->getId()),
                $W->Label('Rewrite: ' . $node->getRewriteName()),
                $target,
                $link,
                $sitemapXml
            )->setVerticalSpacing(.2, 'em')
        )
        ->setHorizontalSpacing(1, 'em')->setVerticalAlign('middle')
        ->addClass('icon-top-48 icon-top icon-48x48');
        
        
        if (isset($language) && isset($langid)) {

            $arr = $node->getTranslatedNodes();
            if (count($arr) > 0) {
                $list = $W->VBoxLayout()->setVerticalSpacing(.8, 'em')->addClass('smed-translations');
                $list->addItem($W->Title(smed_translate('Translations of the same node') , 4));
                
                foreach ($arr as $lang => $alt) {
                    
                    /* @var $alt smed_Node */
                    
                    $list->addItem(
                        $W->Link(
                            $W->FlowItems(
                                $W->CountryFlag(smed_getCountryFromLanguage($lang)),
                                $W->Label($alt->getName($lang))
                            )->setSpacing(.3, 'em')->setVerticalAlign('middle'),
                            $this->proxy()->display($alt->getId())
                        )
                    );
                }
                
                $header->addItem($list);
            }
        }

        return $header;
    }
    
    
    
    protected function getEditFrame($header, $node, $nodeEditBox, $explorer = null)
    {
        $W = bab_Widgets();
        

        if (!isset($explorer)) {
            $explorer = true;
        } else {
            $explorer = (bool) $explorer;
        }
        
        
        
        if ($explorer) {
        
            
            return $this->nodeExplorerView(
                $node,
                $W->VBoxItems($nodeEditBox)
            );
        }
        
        
        $header->addItem(
            $W->Frame()
            ->addClass(Func_Icons::ICON_TOP_24)
            ->addItem(
                $W->Link('', $this->proxy()->edit($node->getId()))->setTitle(smed_translate('Explore sitemap'))
                ->addClass('icon', Func_Icons::ACTIONS_ARROW_RIGHT)
                ),
            0
            );
    
        return $nodeEditBox;
        
        
    }
    
    
    

    /**
     *
     * @param string	$node			The node unique id_function
     * @param 1|0       $explorer       Display tree view
     * @param string    $backurl
     * @return Widget_Action
     */
    public function edit($node = null, $explorer = null, $backurl = null)
    {
        smed_requireCredential();
        $W = smed_Widgets();
        $page = new smed_Page();

        $node = smed_Node::getFromId(1, $node);

        if (!isset($node)) {
            throw new Exception('Node not found');
        }
        
        
        $header = $W->FlowItems($this->nodeHeader($node))
            ->setSpacing(1, 'em')
            ->setVerticalAlign('top');
        
        $editor = $this->getEditor($node);
        $editor->addClass(Func_Icons::ICON_LEFT_16);
        
        if (isset($backurl)) {
            $editor->setHiddenValue('backurl', $backurl);
        }

        $nodeEditBox = $W->VBoxItems(
            $header,
            $editor
        )
        ->addClass('smed-content');
        
        $page->addItem($this->getEditFrame($header, $node, $nodeEditBox, $explorer));
        
        return $page;
    }


    /**
     * Creates a toolbar corresponding to the specified node.
     *
     * @param smed_Node	$node
     *
     * @return smed_Toolbar
     */
    protected function getNodeToolbar(smed_Node $node)
    {
        $toolbar = new smed_Toolbar();
        $W = smed_Widgets();

        if ($node->canUpdate()) {
            $toolbar->addButton(smed_translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT, $this->proxy()->edit($node->getId()));
        }

        if ($node->canDelete()) {
            $toolbar->addItem(
                $W->Link($W->Icon(smed_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->comfirmDelete($node->getId()))
                ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );

        }

        if ($node->canCreate()) {
            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/sitemap_editor/');

            $toolbar->addItem(
                $W->Link(
                    smed_translate('Add childnode'),
                    $this->proxy()->createChildNode($node->getId())
                    )
                ->setOpenMode(Widget_Link::OPEN_DIALOG)
                ->addClass('icon')
                ->addClass(Func_Icons::ACTIONS_FOLDER_NEW)
            );

        }
        
        
        if (bab_isUserAdministrator()) {
            
            $rightsAction = $this->proxy()->editRights($node->getId());
            
            if ($node->canEditRights()) {
                $toolbar->addItem(
                    $W->Link($W->Icon(smed_translate('Rights'), Func_Icons::ACTIONS_SET_ACCESS_RIGHTS),
                        $rightsAction
                    )
                    ->setTitle(smed_translate('Set rights on this node and on sub-publications'))
                );
            } else {
                
                $toolbar->addItem(
                    $W->Link($W->Icon(smed_translate('Set tree access'), Func_Icons::ACTIONS_VIEW_LIST_TREE),
                        $rightsAction
                    )
                    ->setTitle(smed_translate('Set rights on publication nodes in the tree'))
                );
            }
            
            $toolbar->addItem(
                $W->Link($W->Icon(smed_translate('Allowed content types'), Func_Icons::APPS_SECTIONS),
                    $this->proxy()->editTypes($node->getId()))
            );
            
            
        }


        return $toolbar;
    }


    /**
     * A frame to comfirm node removal
     *
     */
    public function comfirmDelete($node = null)
    {
        smed_requireCredential();
        bab_functionality::includeOriginal('Icons');

        $node = smed_Node::getFromId(1, $node);
        $W = smed_Widgets();

        $frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $frame->addClass(Func_Icons::ICON_LEFT_16);

        $frame->addItem($W->Title($node->getContentTypeLabel().' : '.$node->getName(), 3));

        $confirm = smed_translate('Are you sure you want to delete this node?');

        $siteMap = bab_siteMap::getFromSite();
        $adminTargets = array('babAdmin', 'smedUser');
        foreach($adminTargets as $adminTarget){
            $nodes = $siteMap->getNodesByIndex('target', $adminTarget);

            foreach($nodes as $currentNode){
                do{
                    if($currentNode->getId() == $node->getId()){
                        $confirm = smed_translate('This will remove an admin node, removing it could result to loose your administration access.') . ' ' . $confirm;
                    }
                }while($currentNode = $currentNode->parentNode());
            }
        }

        $frame->addItem($W->Label($confirm));

        $editor = new smed_ConfirmDeleteEditor($node);
        $frame->addItem($editor);

        die($frame->display($W->HtmlCanvas()));

    }




    public function createChildNode($node = null)
    {
        smed_requireCredential();
        $W = smed_Widgets();
        $page = $W->BabPage();

        $node = smed_Node::getFromId(1, $node);
        $page->setTitle(sprintf(smed_translate('Create a new child node under %s'), $node->getName()));

        bab_functionality::includeOriginal('Icons');

        $vbox = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        $vbox->addClass(Func_Icons::ICON_LEFT_24);
        $page->addItem($vbox);

        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */

        $allowed = $node->getContentTypes();
        $allowed = array_flip($allowed);
        $inserted = 0;

        foreach($defaultFunc->getAllContentTypes() as $content_type => $arr) {

            if (!isset($allowed[$content_type])) {
                continue;
            }
            
            $type = $arr['functionality'];
            /*@var $type Func_SitemapEditorNode */
            
            if (!$type->canEditRights() && !bab_isUserAdministrator()) {
                continue;
            }
            
            $vbox->addItem(
               $W->Link(
                   $arr['description'],
                   $this->proxy()->add($node->getId(), $content_type )
               )
                ->addClass(Func_Icons::ACTIONS_LIST_ADD)
                ->addClass('icon')
            );
            
            $inserted++;
        }

        if (0 === $inserted) {
            $page->addItem($W->Label(smed_translate('There are no nodes types compatibles with your access level')));
        }
        
        return $page;
    }


    /**
     *
     * @param string	$node			The node unique id_function
     *
     * @return Widget_Action
     */
    public function display($node = null)
    {
        smed_requireCredential();
        $W = smed_Widgets();

        $page = new smed_Page();

        $node = smed_Node::getFromId(1, $node);

        if (!isset($node)) {
            throw new Exception('Node not found');
        }
        
        if (!$node->canRead()) {
            throw new bab_AccessException(smed_translate('The node is not accessible'));
        }

        smed_BreadCrumbs::setCurrentPosition($this->proxy()->display($node->getId()), $node->getName($node->getLanguage()));

        $nodeView = $W->VBoxItems(
            $this->nodeHeader($node)
        );

        $nodeView->addClass('smed-content');

        $type = smed_nodeType($node);
        $nodeFunctionality = smed_getNodeFunctionality($type);

        if (false === $nodeFunctionality) {
            throw new bab_AccessException(sprintf(smed_translate('The node type %s is not supported'), $type));
        }
        
        $target = $node->getTarget();
        
        if (!isset($target) && $node->getTargetId()) {
            $page->addError(smed_translate('Target link to inexistant node'));
        }

        $frame = $nodeFunctionality->display($node);
        $nodeView->addItem($frame);
        
        $rightsSection = smed_getRightsSection($node);
        
        if (isset($rightsSection)) {
            $rightsSection->setFoldable(true, true);
            $nodeView->addItem($rightsSection);
        }
        

        $childNodesSection = smed_getChildNodesSection($node);

        if (isset($childNodesSection)) {
            $childNodesSection->setFoldable(true, false);
            $nodeView->addItem($childNodesSection);
        }

        $page->addItem(
            $this->nodeExplorerView(
                $node,
                $W->VBoxItems(
                    $this->getNodeToolbar($node),
                    $nodeView
                )
            )
        );

        return $page;
    }



    /**
     * Treeview from smed_node
     *
     * @param $rootNode		The root node id displayed in the treeview.
     * @param $selected		The id of the currently selected node.
     *
     * @return widget_SimpleTreeView
     */
    public function treeview($rootNode = null, $selected = null, $id = null)
    {
        smed_requireCredential();
        $W = smed_Widgets();
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();

        $treeView = $W->SimpleTreeView($id)
            //->hideToolbar()
            ;

        $treeView->setPersistent();


        if (!isset($rootNode)) {
            $rootNode = smed_Sitemap::ROOT;
        }


        $node = smed_Node::getFromId(1, $rootNode);


        $rootElement = $treeView->createRootNode($node->getName(), $rootNode);
        $rootElement->setLink($this->proxy()->display($node->getId())->url());

        $iterator = smed_Sitemap::getModifiedIterator();

        foreach ($iterator as $node)
        {
            /* @var $node smed_Node */

            $name = $node->getName();

            if (null === $name)
            {
                $name = sprintf(smed_translate('%s (Not accessible)'), $node->getId());
            }

            if (smed_Node::ISNEW === $node->getNodeType())
            {
                // the user can modifiy only the new nodes
                $url = $this->proxy()->display($node->getId())->url();
            } else {
                // other nodes will not be modifiables
                $url = null;
            }


            if (null !== $treeView->getRootNode()->getNodeById($node->getId()))
            {
                bab_debug('Allready inserted '.$node->getId());
                continue;
            }


            /* @var $element bab_TreeViewElement */
            $element = $treeView->createElement(
                $node->getId(),
                bab_toHtml($node->getIconClassnames()),
                bab_toHtml($name),
                bab_toHtml($node->getDescription()),
                $url
            );

            if ($node->getContentType()) {
                try {
                    $nodeFunc = smed_getNodeFunctionality($node->getContentType());
                } catch(Exception $e) {
                    continue;
                }
                $nodeFunc->updateTreeViewElement($node, $element);
            }

            if (smed_Node::ISNEW !== $node->getNodeType())
            {
                $element->setIcon($nodeImagePath . '/nodes/locked-item.png');
            }


            if (null !== $element)
            {
                try {
                    $treeView->appendElement($element, $node->getParentId());
                } catch(ErrorException $e)
                {
                    bab_debug($e->getMessage());
                }
            }
        }


        if (isset($selected)) {
            $treeView->highlightElement($selected);
        }

        return $treeView;
    }


    /**
     * @param smed_Node 	$node
     * @param Widget_Item	$item
     * @return Widget_HBoxLayout
     */
    protected function nodeExplorerView($node, $item)
    {
        $W = smed_Widgets();

        $nodeExplorerView = $W->HBoxItems(
                $this->treeview('Custom', $node->getId(), 'smed_treeview')
                    ->addClass('smed-resizable-panel')
                    ->setSizePolicy(Widget_SizePolicy::MINIMUM),
                $item
                    ->setSizePolicy(Widget_SizePolicy::MAXIMUM)
                    ->addClass('smed-right-panel')
            )
//			->setHorizontalSpacing(2, 'em')
            ->setId('sitemap_explorer');

        return $nodeExplorerView;
    }
    
    

    /**
     * @param string    $parentNode
     * @param string    $content_type
     * @param 1|0       $explorer       Display tree view
     * @param string    $backurl
     * 
     */
    public function add($parentNode = null, $content_type = null, $explorer = null, $backurl = null)
    {
        smed_requireCredential();
        $W = smed_Widgets();
        $page = new smed_Page();

        $nodeFunc = smed_getNodeFunctionality($content_type);
        if (!$nodeFunc) {
            throw new bab_AccessException(sprintf(smed_translate('The node type %s is not supported'), $content_type));
        }

        $editor = $nodeFunc->getEditor($content_type);
        $editor->setParentNode($parentNode);
        $editor->addIdFunction();
        $editor->addCustomSections($content_type);
        
        if (isset($backurl)) {
            $editor->setHiddenValue('backurl', $backurl);
        }
       

        $nodeEditBox = $W->VBoxItems(
            $W->Title($nodeFunc->getContentTypeDescription($content_type), 2),
            $editor->addClass('icon-left-16 icon-left icon-16x16')
        )
        ->setVerticalSpacing(2, 'em')
        ->addClass('smed-content');

        $node = smed_Node::getFromId(1, $parentNode);
        $header = $W->Layout();
        
        $page->addItem($this->getEditFrame($header, $node, $nodeEditBox, $explorer));
        
        
        return $page;
    }

    
    
    /**
     * @return int
     */
    private function countNodesFromLangId($languageCode, $langid, $nodeId)
    {
        global $babDB;
        
        $res = $babDB->db_query(' 
        SELECT * FROM 
            smed_nodes n, 
            smed_node_labels l 
        WHERE 
            n.id_sitemap='.$babDB->quote(1).' 
            AND l.id_sitemap=n.id_sitemap 
            AND l.id_function=n.id_function 
            AND l.lang='.$babDB->quote($languageCode).' 
            AND n.id_function<>'.$babDB->quote($nodeId).' 
            AND n.langid='.$babDB->quote($langid)
        );
        
        return $babDB->db_num_rows($res);
    }
    
    


    /**
     * Saves the node
     *
     * @param array	$note
     * @return Widget_Action
     */
    public function save($node = null, $redirect = true, $backurl = null)
    {
        smed_requireCredential();
        require_once dirname(__FILE__).'/articletopic.class.php';
        require_once $GLOBALS['babInstallPath'].'utilit/topincl.php';
        
        $this->requireSaveMethod();

        $nodeType = smed_Node::ISNEW;

        if (isset($node['id']) && 'Custom' === $node['id'])
        {
            $node['parent'] = 'DGAll';
        }

        $parentNode = smed_Node::getFromId(1, $node['parent']);
        if (!$parentNode)
        {
            throw new bab_SaveErrorException(smed_translate('missing parent node').' '.$node['parent']);
        }

        
        if (isset($node['id_function']))
        {
            $node['id_function'] = preg_replace('/[^A-Za-z0-9_]/', '' , $node['id_function']);
            $node['id_function'] = trim($node['id_function']);
        }
        


        /* @var $app Func_Applications */
        $app = bab_functionality::get('Applications');
        $nodeFunc = smed_getNodeFunctionality($node['type']);


        if (isset($node['id'])) {

            $smedNode = smed_Node::getFromId(1, $node['id']);

            if (!$smedNode->canUpdate()) {
                throw new bab_AccessException(smed_translate('Update not allowed'));
            }

            // do not overwrite the node type if modified by another way (admin)
            $nodeType = $smedNode->getNodeType();
            $preSave = $nodeFunc->preSave($node, true);
            
            $languageCode = $smedNode->getTreeSetLanguage();

        } else {

            if (!$parentNode->canCreate()) {
                throw new bab_AccessException(smed_translate('Node creation is not allowed here'));
            }


            $allowed = $parentNode->getContentTypes();
            $allowed = array_flip($allowed);

            if (!isset($allowed[$node['type']])) {
                throw new bab_AccessException(smed_translate('This content type is not allowed'));
            }


            $node['id'] = 'smed_' . uniqid();

            if (isset($node['id_function']) && '' !== $node['id_function']) {
                $node['id'] = $node['id_function'];
            }

            $preSave = $nodeFunc->preSave($node, false);
            
            $languageCode = $parentNode->getTreeSetLanguage();
        }
        

        if (isset($languageCode) && !empty($node['langid'])) {
            $count = $this->countNodesFromLangId($languageCode, $node['langid'], $node['id']);
            if ($count > 0) {
                throw new bab_SaveErrorException(sprintf(smed_translate('The lang id "%s" is allready used elsewere for the same language'), $node['langid']));
            }
        }



        if (isset($node['rewriting_root_node']) && $node['rewriting_root_node']) {
            $node['rewrite_name'] = 'rewriting-root-node';
        }
        
        

        $nameDescriptions = array();
        if (isset($languageCode)) {
            $nameDescriptions[$languageCode] = array('name' => $node['label'], 'description' => $node['description']);
        } else {
            $availableLanguages = bab_getAvailableLanguages();
            foreach ($availableLanguages as $language) {
                $nameDescriptions[$language] = array('name' => $node['label'], 'description' => $node['description']);
            }
        }
        
        


        if (!isset($node['popup'])) {
            $node['popup'] = 0;
        }

        if (!isset($node['target'])) {
            $node['target'] = '';
        }

        if (!isset($node['targetchildnodes'])) {
            $node['targetchildnodes'] = 0;
        }

        if (!isset($node['sort_type'])) {
            $node['sort_type'] = smed_Node::SORT_UNORDERED;
        }


        smed_Node::record(
            1,
            $node['parent'],
            $node['id'],
            $nodeType,
            isset($node['url']) ? $node['url'] : '',
            null,
            $nameDescriptions,
            $node['sort_type'],
            $node['classnames'],
            $node['target'],
            (int) $node['targetchildnodes'],
            $node['type'],
            $node['page_title'],
            $node['page_keywords'],
            $node['page_description'],
            $node['rewrite_name'],
            $node['popup'],
            (isset($node['disabled_rewrite']) ? (int) $node['disabled_rewrite'] : 0),
            (isset($node['menuignore']) ? $node['menuignore'] : '0'),
            (isset($node['breadcrumbignore']) ? $node['breadcrumbignore'] : '0'),
            (isset($node['rewriteignore']) ? $node['rewriteignore'] : '0'),
            (isset($node['setlanguage']) ? $node['setlanguage'] : null),
            (isset($node['langid']) ? $node['langid'] : null)
        );


        if(isset($node['treeArticle']) && $node['treeArticle']){
            $newNode = array();
            $newNode['label'] = '';
            $newNode['description'] = '';
            $newNode['parent'] = $node['id'];
            $newNode['popup'] = '0';
            $newNode['menuignore'] = '0';
            $newNode['breadcrumbignore'] = '0';
            $newNode['rewriteignore'] = '0';
            $newNode['page_title'] = '';
            $newNode['page_keywords'] = '';
            $newNode['page_description'] = '';
            $newNode['rewriting_root_node'] = '0';
            $newNode['classnames'] = '';
            $newNode['rewrite_name'] = '';
            $cats = bab_getChildrenArticleCategoriesInformation($node['category'], false, false);
            foreach($cats as $cat){
                $newNode['treeArticle'] = 1;
                $newNode['category'] = $cat['id'];
                $newNode['topic'] = $cat['id'];
                $newNode['type'] = 'category';
                $this->save($newNode, false);
            }
            $tops = bab_getChildrenArticleTopicsInformation($node['category'], false, false);
            foreach($tops as $top){
                $newNode['treeArticle'] = 0;
                $newNode['category'] = $top['id'];
                $newNode['topic'] = $top['id'];
                $newNode['type'] = 'topic';
                $this->save($newNode, false);
            }
        }


        // save custom sections
        if (isset($node['SitemapEditorNodeSection']))
        {
            $this->saveCustomSections($node['type'], $node['id'], $node['SitemapEditorNodeSection']);
        }


        $savedNode = smed_Node::get(1, $node['id']);
        $nodeFunc->postSave($savedNode, $node, $preSave);
        
        
        smed_Sitemap::removeCache();
        
        if ($savedNode->getId() !== $node['id_function'] && '' !== $node['id_function']) {
            $savedNode->updateIdFunction($node['id_function']);
            $node['id'] = $node['id_function'];
        }


        if($redirect){
            
            if (!isset($backurl)) {
                $backurl = $this->proxy()->display($node['id']);
            }
            
            smed_redirect($backurl, smed_translate('The node has been saved.'));
        }
        
        return $node['id'];
    }



    /**
     * Save custom form section from the SitemapEditorNodeSection functionality
     * @param string $content_type
     * @param string $nodeId
     * @param array $values
     */
    protected function saveCustomSections($content_type, $nodeId, $values)
    {
        foreach($values as $appName => $arr)
        {
            $ns = bab_functionality::get("SitemapEditorNodeSection/$appName");
            /* @var $ns Func_SitemapEditorNodeSection */

            $form = $ns->getFormSection($content_type);

            if (null === $form) {
                throw new Exception('Posted informations from non existent form');
            }

            $ns->saveFormSection($content_type, $nodeId, $arr);
        }
    }



    /**
     * Deletes the specified node
     *
     * @param int	$node
     * @return Widget_Action
     */
    public function delete($node = null)
    {
        smed_requireCredential();
        $this->requireDeleteMethod();
        $nodeObject = smed_Node::getFromId(1, $node['id']);

        if (!isset($nodeObject)) {
            throw new Exception('Node not found '.$node['id']);
        }

        if (!$nodeObject->canDelete()) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }

        if ('topic' === $nodeObject->getContentType() && $node['delete_topic']) {
            require_once $GLOBALS['babInstallPath'].'utilit/delincl.php';
            bab_confirmDeleteTopic(smed_getTopicId($nodeObject));
        }

        $parentNode = $nodeObject->getParentId();
        $nodeObject->deleteReference();
        smed_Sitemap::removeCache();

        smed_redirect($this->proxy()->display($parentNode)->url(), smed_translate('The node has been deleted.'));
    }


    public function sortChildNodes($childnodes = null, $node = null)
    {
        smed_requireCredential();
        $this->requireSaveMethod();
        $values = array();
        $i = 1;
        foreach($childnodes as $id_function => $dummy) {
            $values[$id_function] = $i;
            $i++;
        }

        $node = smed_Node::getFromId(1, $node);

        if (!$node->canUpdate()) {
            throw new bab_AccessException(smed_translate('Update must be allowed on parent node to sort node list'));
        }

        $node->setSortValues($values);

        smed_Sitemap::removeCache();

        smed_redirect(smed_BreadCrumbs::last(), smed_translate('The node has been saved.'));
    }


    public function getXmlSitemap($node = null)
    {
        if (strtolower(bab_browserAgent()) == 'msie') {
            header('Cache-Control: public');
        }

        header('Content-Type: application/xml' . "\n");
        echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84
                http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">' . "\n";

        if (!isset($node)) {
            $node = 'Custom';
        }

        $root = smed_Sitemap::get(1);
        $node = $root->getNodeById($node);

        $iterator = new bab_NodeIterator($node);
        while ($node = $iterator->nextNode()) {
            $sitemapItem = $node->getData();
            /*@var $sitemapItem bab_SitemapItem */
            $url = $sitemapItem->getRwUrl();

            if (!empty($url))
            {
                echo '  <url>' . "\n";
                echo '    <loc>' . bab_toHtml(smed_absolutizeUrl($url)) . '</loc>' . "\n";
                echo '  </url>' . "\n";
            }

        }
        echo '</urlset>' . "\n";
        die;
    }



    /**
     *
     * @param string $name
     */
    public function suggestCssClasses($name = null)
    {
        smed_requireCredential();
        require_once dirname(__FILE__)  . '/widgets/suggestclass.class.php';
        $suggest = new smed_SuggestClass('smed-suggest-classes');
        $suggest->setMetadata('suggestparam', 'name');
        $suggest->suggest();

        die;
    }

    /**
     * Does nothing and returns to the previous page.
     *
     * @return Widget_Action
     */
    public function cancel($backurl = null)
    {
        if (!isset($backurl)) {
            $backurl = smed_BreadCrumbs::last();
        }
        
        smed_redirect($backurl);
    }


    /**
     * Returns to the previous page and displays the specified error message.
     *
     * @param string	$errorMessage
     * @return Widget_Action
     */
    protected function error($errorMessage)
    {
        smed_redirect(smed_BreadCrumbs::last(), $errorMessage);
    }



    
    public function editTypes($node = null)
    {
        smed_requireCredential();
        
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
        
        require_once dirname(__FILE__).'/types.ui.php';
        
        $W = bab_Widgets();
        $page = $W->BabPage();
        
        $node = smed_Node::getFromId(1, $node);
        $editor = new smed_TypesEdit($node);
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveTypes(Array $node = null)
    {
        smed_requireCredential();
        $this->requireSaveMethod();
        
        $nodeObject = smed_Node::getFromId(1, $node['id_function']);
        
        if (!bab_isUserAdministrator() || !isset($nodeObject)) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
        
        $contentTypes = array();
        foreach($node['contenttypes'] as $name => $checked) {
            if ($checked) {
                $contentTypes[] = $name;
            }
        }
        
        
        if ($node['recursive']) {
        
            $nodeObject->saveContentTypesRecursive($contentTypes);
            
        
        } else {
        
            $nodeObject->setContentTypes($contentTypes);
            $nodeObject->update();
        }
        
        smed_Sitemap::removeCache();
        
        return true;
    }
    
    
    
    public function editRights($node = null)
    {
        smed_requireCredential();
    
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
        
        require_once dirname(__FILE__).'/right.ui.php';
    
        $W = bab_Widgets();
        $page = $W->BabPage();
        $babBody = bab_getBody();
    
        $node = smed_Node::getFromId(1, $node);
        
        if (!isset($node)) {
            throw new bab_AccessException('Node not found');
        }
        
        if (!$node->canEditRights()) {
            $babBody->addMessage(smed_translate('Rights modification is not permited here, the form can be used to set rights on sub nodes'));
        }
        
        $editor = new smed_RightEdit($node);
    
        $page->addItem($editor);
    
        return $page;
    }
    
    
    public function saveRights(Array $node = null)
    {
        smed_requireCredential();
        $this->requireSaveMethod();
        
        $nodeObject = smed_Node::getFromId(1, $node['id_function']);
    
        if (!bab_isUserAdministrator() || !isset($nodeObject)) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
        
        $rights = $node['rights'];
    
        if (!$nodeObject->canEditRights()) {
            $rights['recursive'] = 1;
        }
        
        
        if ($rights['recursive']) {
            $nodeObject->saveRightsRecursive($rights);
            
        } else {
            $nodeObject->setRights($rights);
        }

        smed_Sitemap::removeCache();
    
        return true;
    }
    
    
    
    public function editRightsDefault($node = null)
    {
        smed_requireCredential();
    
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
    
        require_once dirname(__FILE__).'/right.ui.php';
    
        $W = bab_Widgets();
        $page = $W->BabPage();
    
        $node = smed_Node::getFromId(1, $node);
    
        if (!isset($node)) {
            throw new bab_AccessException('Node not found');
        }
    
        $editor = new smed_RightDefaultEdit($node);
    
        $page->addItem($editor);
    
        return $page;
    }
    
    
    
    public function saveRightsDefault(Array $node = null)
    {
        smed_requireCredential();
        $this->requireSaveMethod();
        
        $nodeObject = smed_Node::getFromId(1, $node['id_function']);
        
        if (!bab_isUserAdministrator() || !isset($nodeObject)) {
            throw new bab_AccessException(smed_translate('Access denied'));
        }
        
        $nodeObject->setRightsDefault($node['default']);
        
        
        smed_Sitemap::removeCache();
        
        return true;
    }
}
