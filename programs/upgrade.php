<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


function smed_moveRightToCore($name)
{
    global $babDB;
    
    $smed = 'smed_'.$name;
    $bab = 'bab_sitemap_'.$name;
    
    if (!bab_isTable($smed)) {
        return;
    }
    
    if (!bab_isTable($bab)) {
        return;
    }
    
    $res = $babDB->db_query('SELECT COUNT(*) c FROM '.$babDB->backTick($smed));
    $arr = $babDB->db_fetch_assoc($res);
    
    $smed_count = (int) $arr['c'];
    
    if (0 === $smed_count) {
        return;
    }
    
    $babDB->db_query('TRUNCATE '.$babDB->backTick($bab));
    
    $res = $babDB->db_query('SELECT * FROM '.$babDB->backTick($smed));
    while ($arr = $babDB->db_fetch_assoc($res)) {
        $node = smed_Node::getFromId(1, $arr['id_object']);
        if (!isset($node)) {
            continue;
        }
        
        $targetNode = $node->getTarget();
        if (!isset($targetNode)) {
            continue;
        }
        
        $babDB->db_query('INSERT INTO '.$babDB->backTick($bab).' (id_object, id_group) 
                VALUES ('.$babDB->quote($targetNode->id_function).', '.$babDB->quote($arr['id_group']).')');
    }
    $babDB->db_query('DROP TABLE '.$babDB->backTick($smed));
    
    
    
    bab_installWindow::message(bab_toHtml(sprintf(smed_translate('Rights table %s moved to ovidentia sitemap'), $smed)));
}

function smed_moveCustomRights()
{
    smed_moveRightToCore('node_create_groups');
    smed_moveRightToCore('node_read_groups');
    smed_moveRightToCore('node_update_groups');
    smed_moveRightToCore('node_delete_groups');
}


/*
function smed_createDefaultRight()
{
    $node = smed_Node::getFromId(1, 'Custom');

    if (!$node->canRead()) {
        // set default rights if Custom is not readable

        $node->saveContentTypesRecursive(array('container', 'ref', 'topic', 'url'));

        $node->setCreate(BAB_ADMINISTRATOR_GROUP);
        $node->setRead(BAB_ALLUSERS_GROUP);
        $node->setUpdate(BAB_ADMINISTRATOR_GROUP);
        $node->setDelete(BAB_ADMINISTRATOR_GROUP);
        
        $node->saveRightsRecursive($node->getRights());
    }
}
*/