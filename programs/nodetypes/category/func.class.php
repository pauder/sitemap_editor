<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

bab_functionality::includeOriginal('SitemapEditorNode');


class Func_SitemapEditorNode_Category extends Func_SitemapEditorNode
{
    public function getDescription()
    {
        return smed_translate('Article category');
    }
    
    public function getClassName()
    {
        return Func_Icons::OBJECTS_PUBLICATION_CATEGORY;
    }
    
    /**
     * Return false to disable the node type in the proposed types for node creation
     * @return boolean
     */
    public function isActive()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/sitemap_editor/');
        return $registry->getValue('createCategoryNode', false);
    }
    
    
    protected function includeNodeEditor()
    {
        parent::includeNodeEditor();
        require_once dirname(__FILE__).'/category.ui.php';
    }
    
    /**
     * Get interface to display in node edit mode
     * @param smed_Node $node
     * @return smed_CategoryNodeEditor
     */
    public function getEditor($content_type, smed_Node $node = null)
    {
        $this->includeNodeEditor();

        $editor = new smed_CategoryNodeEditor(null, null, $node);
        
        if (isset($node)) {
            $category = substr($node->getTargetId(), strlen('babArticleCategory_'));
            $editor->setValue('node/category', $category);
            $url = $node->getUrl();
            $editor->setValue('node/custom_url', $url);
            $url = preg_replace('/&smed_id=[^&]+/', '', $url);
            $url = preg_replace('/&cat=[^&]+/', '', $url);
            $editor->setValue('node/url', $url);
            
            return $editor;
        }
        
        
        // New category
            
        
        $editor->addTreeArticle();
        return $editor;
    }
    
    
    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     * 
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    public function preSave(Array &$node, $nodeExists)
    {
        if(!isset($node['url']) || $node['url'] == '') {
            //nothing
        } elseif ('smed_custom_url' === $node['url']) {
            $node['url'] = trim($node['custom_url']);
        } else {
            $obj = new bab_url($node['url']);
            $obj->smed_id = $node['id'];
            $obj->cat = $node['category'];
            $node['url'] = $obj->toString();
        }
        $node['target'] = 'babArticleCategory_' . $node['category'];
    }
    
    
    
    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();
    
        $element->setIcon($nodeImagePath . '/nodes/publication-category.png');
    }
}