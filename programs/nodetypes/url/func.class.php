<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

bab_functionality::includeOriginal('SitemapEditorNode');


class Func_SitemapEditorNode_Url extends Func_SitemapEditorNode
{
    public function getDescription()
    {
        return smed_translate('Http URL');
    }
    
    
    public function getClassName()
    {
        return Func_Icons::MIMETYPES_TEXT_HTML;
    }
    
    
    protected function includeNodeEditor()
    {
        parent::includeNodeEditor();
        require_once dirname(__FILE__).'/url.ui.php';
    }
    
    
    private function getIdApplication($node)
    {
        return (int) substr($node->getTargetId(), strlen('applicationsApp_'));
    }
    
    
    /**
     * Get interface to display in node edit mode
     * @param smed_Node $node
     * @return smed_UrlNodeEditor
     */
    public function getEditor($content_type, smed_Node $node = null)
    {
        $this->includeNodeEditor();
        $editor = new smed_UrlNodeEditor(null, null, $node);
        
        if (isset($node)) {
            $editor->setValue('node/url', $node->getUrl());
            $target_id = $node->getTargetId();
            if ($target_id)
            {
                $application = $this->getIdApplication($node);
                $editor->setValue('node/application_url', true);
                $editor->setValue('node/application', $application);
            
                $app = bab_functionality::get('Applications');
                /*@var $app Func_Applications */
                $editor->setValue('node/application_rights', $app->getRightsString($application));
            }
            
        }
       
        
        return $editor;
        
    }
    
    
    
    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     * 
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    public function preSave(Array &$node, $nodeExists)
    {
        /* @var $app Func_Applications */
        $app = bab_functionality::get('Applications');
    
        if (!$app) {
            return;    
        }
        
        $id_app = (int) $node['application'];
        
        if ($node['application_url']) {
            
            if ($id_app > 0) {
                
                $app->updateApplication($id_app, $node['url'], $node['label'], '', $node['description']);
                
            } else {
            
                // add the url to application list
                try {
                    $id_app = $app->createApplication($node['url'], $node['label'], '', $node['description'], Func_Applications::LINK, Func_Applications::SITEMAP);
                } catch(application_Exception $e) {
                    throw new bab_SaveErrorException($e->getMessage());
                }
            }
            
            $node['target'] = sprintf('applicationsApp_%d', $id_app);
            $app->setRightsString($id_app, $node['application_rights']);
            return;
        }
        
        
        if ($id_app > 0) {
            $app->deleteApplication($id_app);
        }
    }
    
    
    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();
    
        $element->setIcon($nodeImagePath . '/nodes/text-html.png');
    }
}