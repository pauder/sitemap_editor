<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

bab_functionality::includeOriginal('SitemapEditorNode');


class Func_SitemapEditorNode_Ref extends Func_SitemapEditorNode
{
    public function getDescription()
    {
        return smed_translate('Reference to a sitemap node from Ovidentia');
    }
    
    
    public function getClassName()
    {
        return Func_Icons::MIMETYPES_TEXT_HTML;
    }
    
    protected function includeNodeEditor()
    {
        parent::includeNodeEditor();
        require_once dirname(__FILE__).'/ref.ui.php';
    }
    
    
    /**
     * Get interface to display in node edit mode
     * @param smed_Node $node
     * @return smed_RefNodeEditor
     */
    public function getEditor($content_type, smed_Node $node = null)
    {
        $this->includeNodeEditor();
        $editor = new smed_RefNodeEditor(null, null, $node);
        
        if (isset($node)) {
            $editor->setValue('node/target', $node->getTargetId());
            $editor->setValue('node/targetchildnodes', $node->isTargetChildNodes());
        }

        return $editor;
    }
    
    
    
    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('sitemap_editor');
        $nodeImagePath = $addonInfos->getImagesPath();
    
        if ($node->isTargetChildNodes())
		{
			$element->setIcon($nodeImagePath . '/nodes/refchildnodes.png');
		} else {
			$element->setIcon($nodeImagePath . '/nodes/ref.png');
		}
    }
}