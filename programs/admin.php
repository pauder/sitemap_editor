<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/widgets.class.php';
require_once dirname(__FILE__).'/sitemap.class.php';

bab_functionality::get('Widgets')->includePhpClass('Widget_Icon');
bab_functionality::includefile('Icons');

/**
 * Main tree view
 *
 */
function display_tree_view() {
    $W = bab_functionality::get('Widgets');
    $page = smed_Widgets::getPage();

    $id_sitemap = (int) bab_rp('id_sitemap');
    $sitemap = new smed_Sitemap($id_sitemap);
    $arr = $sitemap->getValues();

    $page->setTitle($arr['name']);

    $page->AddItem(
        $W->Frame(null, $W->HBoxLayout())->addClass('smed_buttons')->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
            ->AddItem(
                $W->Link($W->Icon(smed_translate('List'), Func_Icons::ACTIONS_GO_PREVIOUS), $GLOBALS['babAddonUrl'].'admin')
            )
    );
    $page->AddItem(smed_Widgets::getMergedTree($id_sitemap));

    $page->displayHtml();
}








/**
 * List of sitemaps
 */
function display_maps() {

    require_once dirname(__FILE__).'/sitemaplist.class.php';

    $page = smed_Widgets::getPage();
    $page->setTitle(smed_translate('Site maps'));
    $page->addItemMenu('list', smed_translate('List'), $GLOBALS['babAddonUrl'].'admin');
    $page->addItemMenu('edit', smed_translate('Add'), $GLOBALS['babAddonUrl'].'admin&idx=edit');
    $page->addItemMenu('import', smed_translate('Import topics'), $GLOBALS['babAddonUrl'].'admin&idx=import');
    $page->addItemMenu('move', smed_translate('move linked items'), $GLOBALS['babAddonUrl'].'admin&idx=move');
    $page->setCurrentItemMenu('list');

    $page->addClass('smed_buttons')->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left');

    $list = new smed_SiteMapList;
    $page->addItem($list);

    $page->displayHtml();

}





/**
 *
 *
 */
function smed_sitemap_record($page) {

    $id_sitemap = (int) bab_pp('id_sitemap');

    if (bab_pp('delete')) {

        smed_Sitemap::delete($id_sitemap);
        smed_Sitemap::removeCache();

        $url = bab_url::request('tg');
        $url = bab_url::mod($url, 'idx', 'list');

        smed_goto($url);
    }


    $name = trim(bab_pp('name'));
    $description = bab_pp('description');

    if (empty($name)) {
        $page->addError(smed_translate('Error, the name must not be empty'));
        return false;
    }

    if ($id_sitemap) {
        smed_Sitemap::update($id_sitemap, $name, $description);
    } else {
        smed_Sitemap::create($name, $description);
    }

    smed_Sitemap::removeCache();

    $url = bab_url::request('tg');
    $url = bab_url::mod($url, 'idx', 'list');

    smed_goto($url);
}











/**
 * Edit sitemap
 *
 */
function display_edit() {

    $page = smed_Widgets::getPage();


    if (!empty($_POST)) {
        smed_sitemap_record($page);
    }


    $W = bab_functionality::get('Widgets');




    $page->setTitle(smed_translate('Edit site map'));
    $page->addItemMenu('list', smed_translate('List'), $GLOBALS['babAddonUrl'].'admin');



    if ($id_sitemap = (int) bab_rp('id_sitemap')) {
        $sitemap = new smed_Sitemap($id_sitemap);
        $values = $sitemap->getValues();
        $page->addItemMenu('edit', smed_translate('Edit'), $GLOBALS['babAddonUrl'].'admin&idx=edit&id_sitemap='.$id_sitemap);
    } else {
        $values = array();
        $page->addItemMenu('edit', smed_translate('New sitemap'), $GLOBALS['babAddonUrl'].'admin&idx=edit');
    }


    $page->setCurrentItemMenu('edit');


    $form = $W->Form(null, $W->VBoxLayout())->addClass('smed_edit')

        ->addItem(smed_Widgets::getLabelEdit('name', smed_translate('Name'), 62))
        ->addItem(smed_Widgets::getLabelEditMulti('description', smed_translate('Description')));

    $frame = $W->Frame()->addClass('smed_submit')
        ->additem($W->SubmitButton()->setLabel(smed_translate('Save')));

    if ($id_sitemap && 1 !== $id_sitemap) {
        $frame->additem($W->SubmitButton()->setName('delete')->setLabel(smed_translate('Delete')));
    }

    $form->additem($frame);

    $form->setSelfPageHiddenFields();
    $form->setValues($_POST + $values);

    $page->addItem($form);

    $page->displayHtml();
}






/**
 *
 *
 */
function smed_edit_node_record($page) {

    $id_sitemap			= (int) bab_pp('id_sitemap');
    $id_parent			= bab_pp('id_parent');
    $id_function 		= bab_pp('id_function');
    $node_type			= (int) bab_pp('node_type');
    $url				= bab_pp('url');
    $childnodes			= bab_pp('nodes');
    $labels				= bab_pp('languages');
    $sort_type			= bab_pp('sort_type');
    $classnames			= bab_pp('classnames');
    $target 			= bab_pp('target');
    $targetchildnodes 	= bab_pp('targetchildnodes', 0); // TODO missing checkbox ?
    $content_type		= bab_pp('content_type');

    if ($id_function) {
        if ($node = smed_Node::get($id_sitemap, $id_function)) {
            $id_parent = $node->getParentId();
        }
    }

    switch($node_type) {
        case smed_Node::ISCORE:

            if (!$id_function) {
                // creation
                $id_function 	= bab_pp('core_id_function');
            }

            break;

        case smed_Node::ISMODIFIED:


            if (!$id_function) {
                // creation
                $id_function 	= bab_pp('core_id_function');
            }

            if (empty($labels[$GLOBALS['babLanguage']])) {
                $page->addError(sprintf(smed_translate('The name of the node for the language %s must not be empty'), $GLOBALS['babLanguage']));
                return false;
            }


            break;

        case smed_Node::ISNEW:

            break;


        default:
            $page->addError(smed_translate('Unexpected error 1'));
            return false;
            break;
    }


    if (empty($id_parent)) {
        $page->addError(smed_translate('Unexpected error 2'));
        return false;
    }

    if (empty($id_sitemap)) {
        $page->addError(smed_translate('Unexpected error 3'));
        return false;
    }



    $id_function = trim($id_function);

    if (empty($id_function)) {
        $page->addError(smed_translate('The node ID must not be empty'));
        return false;
    }

    // check id_function validity

    if (!preg_match('/[a-zA-Z0-9_]+/', $id_function)) {
        $page->addError(smed_translate('a node ID must contain only ASCII letters and numbers and underscores'));
        return false;
    }

    smed_node::record($id_sitemap, $id_parent, $id_function, $node_type, $url, $childnodes, $labels, $sort_type, $classnames, $target, $targetchildnodes, $content_type);
    smed_Sitemap::removeCache();
}








/**
 * Edit a sitemap Node
 *
 *
 */
function display_edit_node()
{
    $W = bab_functionality::get('Widgets');
    $page = smed_Widgets::getPage();

    if (!empty($_POST)) {
        smed_edit_node_record($page);
    }


    $form = $W->Form(null, $W->VBoxLayout())->addClass('smed_edit_node');
    $edit_ID = smed_Widgets::getLabelEdit('id_function', smed_translate('ID'));
    $edit_ID->setId('smed_id_function');

    $suggest_ID = smed_Widgets::suggestNode('core_id_function', smed_translate('ID (from core)'))->setId('smed_id_function_core');


    $id_sitemap		= bab_rp('id_sitemap');
    $id_function 	= bab_rp('id_function');	// edit node
    $id_parent	 	= bab_rp('id_parent');		// add node

    if ($node = smed_Node::get($id_sitemap, $id_function)) {
        $id_parent	= $node->getParentId();
    } else {
        $id_function = '';
    }


    $page->addItemMenu('tree', smed_translate('Sitemap tree'), $GLOBALS['babAddonUrl'].'admin&idx=tree&id_sitemap='.$id_sitemap);


    if ($id_function) {

        // modification




        $page->setTitle(smed_translate('Edit node'));
        $edit_ID->disable();
        $suggest_ID->disable();


        $form->setHiddenValue('id_function', $id_function);

        $values = $node->getFormValues();
        $nodes = $values['nodes'];

        $core_name 			= $node->getName();
        $core_description 	= $node->getDescription();

        switch($node->getNodeType()) {



            case smed_Node::ISNEW:
                $nodes_types		= array(smed_Node::ISCORE, smed_Node::ISMODIFIED, smed_Node::ISNEW);
                break;



            default:
                bab_debug('node type error '.$node->getNodeType());

            case smed_Node::ISCORE:
            case smed_Node::ISMODIFIED:
                $nodes_types		= array(smed_Node::ISCORE, smed_Node::ISMODIFIED);
                break;
        }


        $page->addItemMenu('edit_node', smed_translate('Edit node'), $GLOBALS['babAddonUrl'].'admin&idx=edit_node&id_sitemap='.$id_sitemap.'&id_function='.$id_function);

        if ($node->isSortManually()) {
            $page->addItemMenu('sort', smed_translate('Sort childnodes'), $GLOBALS['babAddonUrl'].'admin&idx=sort&id_function='.$id_function.'&id_sitemap='.$id_sitemap);
        }

    } else {

        // node creation

        $page->setTitle(smed_translate('Create new node'));
        $values = array();
        $nodes = array();

        $core_name 			= '';
        $core_description 	= '';
        $nodes_types		= array(smed_Node::ISCORE, smed_Node::ISMODIFIED, smed_Node::ISNEW);

        $page->addItemMenu('edit_node', smed_translate('New node'), $GLOBALS['babAddonUrl'].'admin&idx=edit_node&id_sitemap='.$id_sitemap);
    }

    $page->setCurrentItemMenu('edit_node');


    if (!$id_parent) {
        throw new Exception('missing parameters');
    }




    $parent_node	= smed_Node::get($id_sitemap, $id_parent);


    if (isset($_POST['nodes'])) {
        $nodes = $_POST['nodes'] + $nodes;
    }




    $HBox = $W->HBoxLayout();
    $form->addItem($HBox);

    $frame = $W->Frame();



    $left = $W->VBoxLayout();
    $left->addItem($frame);

    if ($node && $node->isMovedReference()) {
        $left->addItem(smed_Widgets::movedReference($id_sitemap, $node));
    }

    $frame
        ->addItem(smed_Widgets::getNodeTypeSelect($nodes_types))
        ->addItem($edit_ID)
        ->addItem($suggest_ID)
        ->addItem(smed_Widgets::getContentTypeAndTarget())
        ->addItem(smed_Widgets::getNodeCoreLabels($core_name, $core_description))
        ->addItem(smed_Widgets::getNodeLanguagesTabSet())
        ->addItem(smed_Widgets::getLabelEdit('url', smed_translate('Url'), 50)->setId('smed_url'))
        ->addItem(smed_Widgets::getSortTypeSelect())
        ->addItem(smed_Widgets::getIconSelect())
        ->addItem($W->Pair($W->Label(smed_translate('Parent Node')), $W->Label($parent_node->getName())))
        ->addClass('smed_edit_node_col');

    $HBox->AddItem($left);


    $HBox->AddItem(
        $W->VBoxItems(
            $W->Title(smed_translate('Child nodes')),
            smed_Widgets::getCoreTree($nodes)
        )->addClass('smed_edit_node_col')
    );


    $form->additem($W->Frame()->addClass('smed_submit')->additem($W->SubmitButton()->setLabel(smed_translate('Save'))));

    $form->setSelfPageHiddenFields();

    if (isset($values['id_function'])) {
        $values['core_id_function'] = $values['id_function'];
    }

    $form->setValues($_POST + $values);

    $page->addItem($form);
    $page->displayHtml();


}


/**
 * Delete a node
 * set disabled if the node is a recorded core reference
 * Add a disabled if the node is a core node
 * Or delete is the node is a new node
 *
 *
 */
function delete_node()
{
    $id_sitemap = (int) bab_rp('id_sitemap');
    $id_function = bab_rp('id_function');

    $sitemap = new smed_Sitemap($id_sitemap);
    $sitemap->deleteNodeFromMerged($id_function);

    smed_Sitemap::removeCache();

    $url = bab_url::request('tg', 'id_sitemap');
    $url = bab_url::mod($url, 'idx', 'tree');

    smed_goto($url);
}

/**
 * Restore original node
 *
 *
 */
function deleteReference()
{
    $id_sitemap = (int) bab_rp('id_sitemap');
    $id_function = bab_rp('id_function');

    $node = smed_Node::getFromId($id_sitemap, $id_function);
    $node->deleteReference();

    smed_Sitemap::removeCache();

    $url = bab_url::request('tg', 'id_sitemap');
    $url = bab_url::mod($url, 'idx', 'tree');

    smed_goto($url);

}






function smed_sort_node_record($page, $node)
{
    $values = array();
    $i = 1;
    foreach($_POST['childnodes'] as $id_function => $dummy) {
        $values[$id_function] = $i;
        $i++;
    }


    $node->setSortValues($values);
}





/**
 * Display sort childnodes interface
 *
 */
function display_sort_node()
{
    $id_sitemap = (int) bab_rp('id_sitemap');
    $id_function = bab_rp('id_function');

    $W = bab_functionality::get('Widgets');
    $page = smed_Widgets::getPage();

    if (empty($id_sitemap) || empty($id_function)) {
        throw new Exception('missing parameter');
    }

    $node = smed_Node::getFromId($id_sitemap, $id_function);

    if (!empty($_POST)) {
        smed_sort_node_record($page, $node);
    }


    $form = $W->Form(null, $W->VBoxLayout());


    $page->addItemMenu('tree', smed_translate('Sitemap tree'), $GLOBALS['babAddonUrl'].'admin&idx=tree&id_sitemap='.$id_sitemap);
    $page->addItemMenu('edit_node', smed_translate('Edit node'), $GLOBALS['babAddonUrl'].'admin&idx=edit_node&id_sitemap='.$id_sitemap.'&id_function='.$id_function);
    $page->addItemMenu('sort', smed_translate('Sort childnodes'), $GLOBALS['babAddonUrl'].'admin&idx=sort&id_function='.$id_function.'&id_sitemap='.$id_sitemap);

    $page->setTitle(sprintf(smed_translate('Sort childnodes of %s'), $node->getName()));

    $page->setCurrentItemMenu('sort');

    $frame = $W->Frame(null, $W->VBoxLayout()->sortable())->setName('childnodes')->addClass('smed_sort');


    foreach($node->getChildNodes() as $cnode) {
        $frame->addItem(
            $W->HBoxItems(
                $W->Hidden()->setName($cnode->getId()),
                $W->Label($cnode->getName())
            )
        );
    }



    $form->addItem($frame);

    $form->additem($W->Frame()->addClass('smed_submit')->additem($W->SubmitButton()->setLabel(smed_translate('Save'))));
    $form->setSelfPageHiddenFields();

    $page->addItem($form);
    $page->displayHtml();
}






class smed_artapi
{
    public function __construct()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
    }


    /**
     *
     * @return int
     */
    protected function getCustomCategory()
    {
        $id_category = smed_getCategoryByName(0, 'Racine');

        if (null === $id_category)
        {
            // the category does not exists, create it

            $id_category = bab_addTopicsCategory('Racine', '', 'N', '', '', 0);
        }

        return $id_category;
    }


    public function redirect()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
        bab_url::get_request('tg')->location();
    }
}






class smed_import_topics extends smed_artapi
{


    /**
     * Move all root categories into custom node
     * @return int		id "Racine" category
     */
    public function moveToCustom()
    {

        $categories = bab_getChildrenArticleCategoriesInformation(0, false, false);
        $custom = $this->getCustomCategory();

        foreach($categories as $id_category => $cat)
        {
            if ('Racine' === $cat['title'])
            {
                continue;
            }

            bab_updateTopicsCategory($id_category, $cat['title'], $cat['description'], null, null, null, $custom);
        }

        return $custom;
    }



    /**
     * get categories and topic tree
     * recursive
     * @return unknown_type
     */
    public function processCategory($category, $nodeId)
    {
        $node = smed_Node::get(1, $nodeId);

        $childnodes = array();
        foreach($node->getChildNodes() as $childnode)
        {
            $childnodes[$childnode->getName()] = 1;
        }


        $topics = bab_getChildrenArticleTopicsInformation($category, false, false);

        foreach($topics as $id_topic => $topic)
        {
            if (isset($childnodes[$topic['title']]))
            {
                continue;
            }

            smed_Node::record(
                1,
                $nodeId,
                'smed_' . uniqid(),
                smed_node::ISNEW,
                '',
                array(),
                array(
                    'fr' => array('name' => $topic['title'], 'description' => $topic['description']),
                    'en' => array('name' => $topic['title'], 'description' => $topic['description'])
                ),
                null,
                '',
                'babArticleTopic_'.$id_topic,
                0,
                'topic'
            );
        }


        $categories = bab_getChildrenArticleCategoriesInformation($category, false, false);

        foreach($categories as $id_category => $cat)
        {
            if (isset($childnodes[$cat['title']]))
            {
                continue;
            }

            $uid = 'smed_' . uniqid();

            smed_Node::record(
                1,
                $nodeId,
                $uid,
                smed_node::ISNEW,
                '',
                array(),
                array(
                    'fr' => array('name' => $cat['title'], 'description' => $cat['description']),
                    'en' => array('name' => $cat['title'], 'description' => $cat['description'])
                ),
                null,
                '',
                'babArticleCategory_'.$id_category,
                0,
                'category'
            );


            $this->processCategory($id_category, $uid);
        }
    }



}




/**
 *
 * @return unknown_type
 */
function display_import()
{
    if (isset($_POST['import']))
    {
        $import = new smed_import_topics();
        $id = $import->moveToCustom();
        $import->processCategory($id, 'Custom');
        $import->redirect();
    }


    $W = bab_functionality::get('Widgets');
    /*@var $W Func_Widgets */
    $page = smed_Widgets::getPage();

    $form = $W->Form(null, $W->VBoxLayout());

    $page->addItemMenu('list', smed_translate('List'), $GLOBALS['babAddonUrl'].'admin');
    $page->addItemMenu('import', smed_translate('Import topics'), $GLOBALS['babAddonUrl'].'admin&idx=import');
    $page->addItemMenu('move', smed_translate('move linked items'), $GLOBALS['babAddonUrl'].'admin&idx=move');

    $page->setTitle(smed_translate('Import ovidentia topics and categories into sitemap'));

    $page->setCurrentItemMenu('import');


    $form->addItem($W->Label(smed_translate('Warning! this action is not recoverable, this should be used only with an empty sitemap tree. The topics will be moved also in the articles administration to match the sitemap structure')));
    $form->additem($W->Frame()->addClass('smed_submit')->additem($W->SubmitButton()->setName('import')->setLabel(smed_translate('Import'))));
    $form->setSelfPageHiddenFields();


    $page->addItem($form);
    $page->displayHtml();
}



class smed_move_topics extends smed_artapi
{

    /**
     * @return bool
     */
    protected function containTopics($category)
    {
        $topics = bab_getChildrenArticleTopicsInformation($category, false, false);
        if (count($topics) > 0) {
            return true;
        }

        $subcats = bab_getChildrenArticleCategoriesInformation($category, false, false);
        foreach($subcats as $id => $subcat) {
            if ($this->containTopics($id)) {
                return true;
            }
        }

        return false;
    }


    public function deleteEmptyCategories()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/delincl.php';

        $arr = bab_getArticleCategories();
        foreach($arr as $id => $category) {
            if ($this->containTopics($id)) {
                continue;
            }

            bab_deleteTopicCategory($id, true);
        }
    }



    /**
     * browse topics in ovidentia
     * if not in sitemap, remove from "Racine" to "Hors plan du site"
     */
    public function moveOrphans()
    {
        global $babDB;

        $res = $babDB->db_query('SELECT id FROM bab_topics');
        while ($arr = $babDB->db_fetch_assoc($res)) {
            $node = smed_Node::getFromTargetId(1, 'babArticleTopic_'.$arr['id']);

            if (isset($node) && smed_Node::ISNEW === $node->getNodeType()) {
                // a reference exists in sitemap
                continue;
            }





            $ancestors = $this->getAncestors($arr['id']);
            $root = reset($ancestors);

            if ("Hors plan du site" === $root) {
                // allready moved out
                continue;
            }

            $id_category = $this->copyCategories($ancestors);
            $this->moveOrphanTopic($id_category, $arr['id']);
        }


    }



    /**
     * copy categories into "Hors plan du site", do not include "Racine" if exists
     * @return int  the last category id
     */
    protected function copyCategories(Array $arr)
    {
        $root = reset($arr);
        if ("Racine" === $root) {
            $rootKey = key($arr);
            unset($arr[$rootKey]);
        }

        $id_parent = $this->getNamedCategory(0, "Hors plan du site");

        foreach($arr as $id_source => $categoryName) {
            $source = bab_getTopicCategoryArray($id_source);
            $id_parent = $this->getNamedCategory($id_parent, $categoryName, $source['description'], $source['template'], $source['display_tmpl']);
        }

        return $id_parent;
    }




    /**
     * Get list of ancestors categories
     * from top to the topic category
     * @return array
     */
    protected function getAncestors($topic)
    {
        $topcats = bab_getArticleCategories();
        $topic = bab_getTopicArray($topic);


        $categoryid = (int) $topic['id_cat'];
        $idcategories = array();
        $idcategories[$categoryid] = $topcats[$categoryid]['title'];

        if (isset($topcats[$categoryid])) {
            while ($topcats[$categoryid]['parent'] != 0) {
                $idcategories[$categoryid] = $topcats[$categoryid]['title'];
                $categoryid = $topcats[$categoryid]['parent'];
            }

            $idcategories[$categoryid] = $topcats[$categoryid]['title'];
        }

        return array_reverse($idcategories, true);
    }



    /**
     * Process sitemap tree level
     * @param	string	$nodeId
     * @param	string	$id_parent		default id parent used to create categories if a parent category has not been found by it's name
     * @return unknown_type
     */
    public function process($nodeId)
    {
        $node = smed_Node::get(1, $nodeId);

        if (!isset($node)) {
            return;
        }


        foreach($node->getChildNodes() as $childnode)
        {
            /*@var $childnode smed_Node */

            switch($childnode->getContentType())
            {
                case 'topic':
                    $id = $this->getCategory($node);
                    $this->moveTopic($id, $childnode);
                    break;

                case 'category':
                    $id = $this->getCategory($node);
                    $this->moveCategory($id, $childnode);
                    break;

                default:
                    // do nothing, other items are not linked
                    break;
            }

            $this->process($childnode->getId());
        }
    }


    /**
     * Get category ID from one level, create if not exists
     * @param int $id_category
     * @param string $name
     * @return int
     */
    private function getNamedCategory($id_category, $name, $description = '', $template = '', $disptmpl = '')
    {

       $new_id_category = smed_getCategoryByName($id_category, $name);
        if (null !== $new_id_category)
        {
          return $new_id_category;
        }


        $new_id_category = bab_addTopicsCategory($name, $description, 'N', $template, $disptmpl, $id_category);

        if (!$new_id_category)
        {
            throw new Exception(sprintf('Error : cannot create category %s (last level)', $name));
        }

        return $new_id_category;
    }



    /**
     * Get category ID
     * @param smed_Node $node
     * @return int
     */
    private function getCategory(smed_Node $node)
    {
        // build tree from top to bottom
        $ancestors = $node->getModifiedAncestors();
        $id_parent = 0;


        foreach($ancestors as $browsed)
        {
            $id_parent = $this->getNamedCategory($id_parent, $browsed->getName(), $browsed->getDescription());
        }

        $id_parent = $this->getNamedCategory($id_parent, $node->getName(), $node->getDescription());
        return $id_parent;
    }


    private function moveOrphanTopic($id_parent, $id_topic)
    {
        $top = bab_getTopicArray($id_topic);
        $error = array();
        bab_updateTopic($id_topic, $top['category'], $top['description'], $id_parent, $error);
    }


    /**
     *
     * @param int			$id_parent		id topic category
     * @param smed_Node		$node
     * @return unknown_type
     */
    private function moveTopic($id_parent, smed_Node $node)
    {
        $topic = (int) substr($node->getTargetId(), strlen('babArticleTopic_'));

        //bab_debug("move topic id $topic into category $id_parent ". $node->getName());
        $error = array();
        bab_updateTopic($topic, $node->getName(), $node->getDescription(), $id_parent, $error);
    }


    private function moveCategory($id_parent, smed_Node $node)
    {
        $category = (int) substr($node->getTargetId(), strlen('babArticleCategory_'));

        //bab_debug("move category id $category into category $id_parent ". $node->getName());
        bab_updateTopicsCategory($category, $node->getName(), $node->getDescription(), null, null, null, (int) $id_parent);
    }
}




/**
 *
 * @return unknown_type
 */
function display_move()
{
    if (isset($_POST['move']))
    {
        $import = new smed_move_topics();
        $import->process('Custom');
        $import->moveOrphans();
        $import->deleteEmptyCategories();
        $import->redirect();
    }


    $W = bab_functionality::get('Widgets');
    /*@var $W Func_Widgets */
    $page = smed_Widgets::getPage();

    $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

    $form->addClass('widget-bordered');
    $form->addClass('BabLoginMenuBackground');

    $page->addItemMenu('list', smed_translate('List'), $GLOBALS['babAddonUrl'].'admin');
    $page->addItemMenu('import', smed_translate('Import topics'), $GLOBALS['babAddonUrl'].'admin&idx=import');
    $page->addItemMenu('move', smed_translate('move linked items'), $GLOBALS['babAddonUrl'].'admin&idx=move');

    $page->setTitle(smed_translate('Organize linked topics and categories'));

    $page->setCurrentItemMenu('move');


    $form->addItem($W->Label(smed_translate('Warning! this action is not recoverable, this should be used only with items in your sitemap tree linked to categories or topics, the categories and topics will be moved in administration tree according to the sitemap structure. others topics will be moved under the "Hors plan du site" category')));
    $form->addItem($W->Label(smed_translate('The empty categories will be deleted by the script in administration > articles')));
    $form->additem($W->Frame()->addClass('smed_submit')->additem($W->SubmitButton()->setName('move')->setLabel(smed_translate('Move'))));
    $form->setSelfPageHiddenFields();


    $page->addItem($form);
    $page->displayHtml();
}








if (!bab_isUserAdministrator()) {
    return;
}




$idx = bab_rp('idx');
if (!$idx) {
    $idx = 'list';
}

switch($idx) {
    case 'edit':
        display_edit();
        break;

    case 'tree':
        display_tree_view();
        break;

    case 'list':
        display_maps();
        break;

    case 'edit_node':
        display_edit_node();
        break;


    case 'delete':
        delete_node();
        break;

    case 'deleteReference':
        deleteReference();
        break;

    case 'add':
        display_edit_node();
        break;

    case 'sort':
        display_sort_node();
        break;

    case 'import':
        display_import();
        break;

    case 'move':
        display_move();
        break;

    case 'test':
        $sitemap = smed_Sitemap::get(bab_rp('id_sitemap'), '', '');
        bab_debug(bab_toHtml($sitemap->__toString()));

        break;
}
