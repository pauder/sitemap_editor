<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/base.ui.php';

class smed_NodeEditor extends smed_Editor
{
    /**
     * @var \Widget_Section
     */
    protected $generalSection;
    
    /**
     * @var \Widget_Section
     */
    protected $referencingSection;
    
    /**
     * @var \Widget_Section
     */
    protected $advancedSection = null;

    /**
     * @var smed_Node
     */
    protected $node = null;
    
    /**
     * set if setlanguage is set in ancestors
     * 
     * @var string
     */
    protected $language = null;

    public function __construct($id = null, Widget_Layout $layout = null, smed_Node $node = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        
        parent::__construct($id, $layout);

        $this->node = $node;
        $this->colon();
        
        
        if (isset($node)) {
            $this->language = $node->getTreeSetLanguage();
        }
        
        $this->setName('node');
        $this->setHiddenValue('tg', bab_rp('tg'));

        $generalLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        $advancedLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

        if (isset($this->language)) {
            $advancedLayout->addItem($this->langid());
        }
        
        $advancedLayout->addItem($this->SelectParentNode());
        
        if (null === $node || 'Custom' !== $node->getId())
        {
            $advancedLayout->addItem($this->Popup());
            $advancedLayout->addItem($this->menuignore());
        
        }
        
        $advancedLayout->addItem($this->breadcrumbignore());
        $advancedLayout->addItem($this->rewriteignore());
        $advancedLayout->addItem($this->Classes());
        

        $this->generalSection = $W->Section(
            smed_translate('General'),
            $generalLayout
        );
        $this->generalSection->setFoldable(true);

        $this->advancedSection = $W->Section(
            smed_translate('Advanced'),
            $advancedLayout
        );
        $this->advancedSection->setFoldable(true, true);


        $this->referencingSection = $W->section(
            smed_translate('Referencing'),
            $W->VBoxItems(
                $this->PageTitle(),
                $this->PageKeyWords(),
                $this->PageDescription(),
                $this->RewritingRootNode(),
                $this->RewriteName()
            )->setVerticalSpacing(1, 'em')
        );
        $this->referencingSection->setFoldable(true);

        $this->addSections();

        $this->setNodeValues();

        $this->setSaveAction(smed_controller()->Node()->save());
        $this->setCancelAction(smed_controller()->Node()->cancel());
    }
    
    
    
    protected function appendButtons()
    {
        parent::appendButtons();
        
        if (isset($this->node)) {
        
            $W = bab_Widgets();
            $this->buttonsLayout->addItem(
                $W->SubmitButton()->setLabel(smed_translate('Delete'))
                ->setAction(smed_controller()->Node()->delete())
                ->setConfirmationMessage(
                    sprintf(
                        smed_translate('Do you really want to delete the node (%s) ?'), 
                        $this->node->getName()
                    )
                )
            );
        }
    }
    
    
    
    /**
     * Insert sections into form
     */
    protected function addSections()
    {
        $this->addItem($this->generalSection);
        $this->addItem($this->referencingSection);
        $this->addItem($this->advancedSection);
    }


    /**
     * Add custom section from API
     * @param string $content_type
     */
    public function addCustomSections($content_type)
    {
        $F = new bab_functionalities();
        $arr = $F->getChildren('SitemapEditorNodeSection');

        if (empty($arr))
        {
            return;
        }


        $W = $this->widgets;

        foreach($arr as $subfolder)
        {
            $ns = bab_functionality::get('SitemapEditorNodeSection/'.$subfolder);
            /* @var $ns Func_SitemapEditorNodeSection */


            $formSection = $ns->getFormSection($content_type);

            if (null === $formSection)
            {
                continue;
            }


            $section = $W->Section(
                $ns->getDescription(),
                $formSection
            )->setFoldable(true, true);

            $section->setName(array('SitemapEditorNodeSection', $subfolder));

            $this->addItem($section);

            if (null !== $this->node)
            {
                if (null !== $values = $ns->getValues($this->node->getId()))
                {
                    $this->setValues($values, array('node', 'SitemapEditorNodeSection', $subfolder));
                }
            }
        }
    }



    /**
     * The item classes.
     *
     * @return array
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'smed-node-editor';
        return $classes;
    }

    /**
     * @param string	$nodeId	The parent node id.
     *
     * @return smed_NodeEditor
     */
    public function setParentNode($nodeId)
    {
        $this->setValue('node/parent', $nodeId);
        return $this;
    }


    /**
     * @return smed_NodeEditor
     */
    protected function setNodeValues()
    {

        if (!isset($this->node)) {
            return;
        }

        $node = $this->node;


        $this->setValue('node/id_function', $node->getId());
        $this->setValue('node/parent', $node->getParentId());
        $this->setHiddenValue('node[id]', $node->getId());
        $this->setValue('node/langid', $node->getLangId());
        $this->setValue('node/label', $node->getName($this->language));
        $this->setValue('node/description', $node->getDescription($this->language));
        $this->setValue('node/sort_type', $node->getSortType());
        $this->setValue('node/page_title', $node->getPageTitle());
        $this->setValue('node/page_keywords', $node->getPageKeywords());
        $this->setValue('node/page_description', $node->getPageDescription());
        $rewriteName = $node->getRewriteName(false);
        if ($rewriteName === 'rewriting-root-node') {
            $this->setValue('node/rewriting_root_node', true);
            $this->setValue('node/rewrite_name', '');
        } else {
            $this->setValue('node/rewriting_root_node', false);
            $this->setValue('node/rewrite_name', $rewriteName);
        }
        $this->setValue('node/disabled_rewrite', $node->isRewriteDisabled());
        $this->setValue('node/popup', $node->isPopup());
        $this->setValue('node/menuignore', $node->isMenuIgnore());
        $this->setValue('node/breadcrumbignore', $node->isBreadCrumbIgnore());
        $this->setValue('node/rewriteignore', $node->isRewriteIgnore());
        $this->setValue('node/classnames', $node->getIconClassnames());
        $this->setValue('node/setlanguage', $node->getSetLanguage());

        if ($node->getIconClassnames() !== '') {
            $this->advancedSection->setFoldable(true, false);
        } else {
            $this->advancedSection->setFoldable(true, true);
        }

        $this->advancedSection->addItem(
            $this->AttachedFiles($node)
        );

        return $this;
    }



    public function addIdFunction()
    {
        $this->advancedSection->addItem($this->IdFunction(), 0);
    }


    protected function IdFunction()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Id'),
            $W->LineEdit()->setSize(50),
            'id_function'
        );
    }
    
    

    protected function langid()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Lang. node Id'),
            $W->LineEdit()->setSize(50),
            __FUNCTION__,
            smed_translate('The same id can be used for translations, the id must be unique for one language')
        );
    }
    
    
    

    
    


    protected function Label()
    {
        $W = $this->widgets;
        $labelled = $this->labelledField(
            smed_translate('Label'),
            $W->LineEdit()->setSize(50),
            'label'
        );
        
        if (!isset($this->language)) {
            return $labelled;
        }
        
        
        return $W->FlowItems($labelled, $W->CountryFlag(smed_getCountryFromLanguage($this->language)))->setHorizontalSpacing(.5, 'em');
    }



    protected function Description()
    {
        $W = $this->widgets;
        $labelled = $this->labelledField(
            smed_translate('Description'),
            $W->TextEdit()->setColumns(50)->setLines(2),
            'description'
        );
        
        if (!isset($this->language)) {
            return $labelled;
        }
        
        return $W->FlowItems($labelled, $W->CountryFlag(smed_getCountryFromLanguage($this->language)))->setHorizontalSpacing(.5, 'em');
    }


    protected function Url()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Url'),
            $W->LineEdit()->setSize(50),
            'url'
        );
    }



    /**
     *  Display a select input with the tree of nodes
     */
    protected function SelectParentNode()
    {
        require_once dirname(__FILE__)  . '/widgets/nodepicker.class.php';

        return $this->labelledField(
            smed_translate('Parent node'),
            smed_NodePicker()->basenode(bab_rp('node', null)),
            'parent' /* Set the default value (parent node) */
        );

    }



    protected function Popup()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Open in new window'),
            $W->CheckBox(),
            'popup'
        );
    }

    protected function menuignore()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
            smed_translate('Ignore this node and his children while displaying a menu'),
            $W->CheckBox(),
            'menuignore'
        );
    }


    protected function breadcrumbignore()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
            smed_translate('Ignore this node in the breadcrumb path'),
            $W->CheckBox(),
            'breadcrumbignore'
        );
    }
    
    protected function rewriteignore()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
                smed_translate('Ignore this node in rewrite path'),
                $W->CheckBox(),
                'rewriteignore'
        );
    }

    protected function PageTitle()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Page title'),
            $W->LineEdit()->setSize(50),
            'page_title'
        );
    }

    protected function PageKeyWords()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Page keywords'),
            $W->TextEdit()->setColumns(48)->setLines(2),
            'page_keywords'
        );
    }

    protected function PageDescription()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Page description'),
            $W->TextEdit()->setColumns(48)->setLines(2),
            'page_description'
        );
    }

    protected function RewriteName()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
            smed_translate('Rewrite name'),
            $W->LineEdit()->setSize(50),
            'rewrite_name'
        );
    }


    protected function RewritingRootNode()
    {
        $W = $this->widgets;
        return
        $this->labelledField(
            smed_translate('Rewrite name root node'),
            $W->CheckBox(),
            'rewriting_root_node'
        )->setTitle(smed_translate('This node and nodes above will not be taken into account for Url rewriting.'));
    }


    protected function Classes()
    {
        require_once dirname(__FILE__)  . '/widgets/suggestclass.class.php';
        $suggestClass = new smed_SuggestClass('smed-suggest-classes');

        $suggestClass->setSuggestAction(smed_controller()->Node()->suggestCssClasses(), 'name');

        return $this->labelledField(
            smed_translate('CSS classes'),
            $suggestClass,
            'classnames'
        );
    }


    protected function AttachedFiles(smed_Node $node)
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Attached files'),
            $W->FilePicker()
            ->setFolder($node->uploadPath()),
            'files'
        );
    }


    public function getAdvencedSection()
    {
        return $this->advancedSection;
    }


    public function getGeneralSection()
    {
        return $this->generalSection;
    }
}
