<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';



class smed_Widgets {


    public static function getPage() {

        $W = bab_functionality::get('Widgets');
        $page = $W->BabPage();
        $addon = bab_getAddonInfosInstance('sitemap_editor');
        $page->addStyleSheet($addon->getStylePath().'main.css');
        $page->addJavascriptFile(smed_getAddon()->getTemplatePath().'scripts.js');

        bab_sitemap::setPosition('smed', 'Admin');

        return $page;
    }










    /**
     * Get core sitemap merged with modifications
     * @param	int		$id_sitemap
     * @return	Widget_SimpleTreeView
     */
    public static function getMergedTree($id_sitemap) {

        $sitemap = new smed_Sitemap($id_sitemap);


        $W = bab_functionality::get('Widgets');
        $treeView = $W->SimpleTreeView();
        $treeView->createRootNode(smed_translate('Home'), smed_Sitemap::ROOT);
        $treeView = $sitemap->getMerged($treeView);


        $iterator = new bab_NodeIterator($treeView->getRootNode());

        $node = $iterator->nextNode();
        $element = $node->getData();

        $baseurl = bab_url::request('tg', 'id_sitemap');
        $images = smed_getAddon()->getImagesPath();

        while ($node = $iterator->nextNode()) {

            $element = $node->getData();

            $functionurl = bab_url::mod($baseurl, 'id_function', $node->getId());
            $parenturl = bab_url::mod($baseurl, 'id_parent', $node->getId());






            $element->addAction(
                'edit',
                smed_translate('Edit'),
                $images . 'actions/edit.png',
                bab_url::mod($functionurl, 'idx', 'edit_node'),
                ''
            );



            $element->addAction(
                'add',
                smed_translate('Add'),
                $images . 'actions/add.png',
                bab_url::mod($parenturl, 'idx', 'add'),
                ''
            );


            if (smed_Sitemap::ROOT !== $node->getId()) {


                $deleteMessage = sprintf(smed_translate("Do you really want to delete the node (%s) ?"), $element->_title);
                $deleteMessage = bab_toHtml($deleteMessage, BAB_HTML_JS);

                $element->addAction(
                    'delete',
                    smed_translate('Delete'),
                    $images . 'actions/delete.png',
                    bab_url::mod($functionurl, 'idx', 'delete'),
                    "return confirm('" . $deleteMessage . "')"
                );


            }

        }

        return $treeView;
    }




    /**
     * Get core sitemap tree
     *
     * @param	array		$checked_nodes		array of nodes id as in _POST[nodes], nodes names are stored in keys
     *
     * @return	Widget_SimpleTreeView
     */
    public static function getCoreTree($checked_nodes = array()) {

        $W = bab_functionality::get('Widgets');
        $treeView = $W->SimpleTreeView();
        $treeView->createRootNode(smed_translate('Home'), smed_Sitemap::ROOT);

        $root = bab_siteMap::get();
        $DGAll = $root->getNodeById(smed_Sitemap::ROOT);

        $iterator = new bab_NodeIterator($DGAll);
        $node = $iterator->nextNode();




        // $element =& $node->getData();

        while ($node =& $iterator->nextNode()) {

            $sitemapItem = $node->getData();


            $element = $treeView->createElement(
                $sitemapItem->id_function,
                $sitemapItem->iconClassnames,
                $sitemapItem->name,
                $sitemapItem->description,
                $sitemapItem->url
            );

            $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

            $id_parent = $node->parentNode()->getId();
            $element->addCheckBox('nodes['.$node->getId().']',isset($checked_nodes[$node->getId()]));
            $treeView->appendElement($element, $id_parent);

        }

        return $treeView;
    }











    /**
     * Creates a label and line edit in an pair widget.
     * @param string	$name
      * @param string	$labelText
      * @param int		$size
      * @param array		$classes
      * @param string	$suffix
     * @return Widget_Pair
     */
    public static function getLabelEdit($name, $labelText, $size = 20, $classes = array(), $suffix = null, $maxsize = 255) {
        $W = bab_functionality::get('Widgets');

        $pair = $W->create('Widget_Pair');

        $lineEdit = $W->LineEdit();
        $lineEdit->setName($name);
        $lineEdit->setSize($size);
        $lineEdit->setMaxSize($maxsize);

        foreach($classes as $classname) {
            $lineEdit->addClass($classname);
            $pair->addClass('pair_'.$classname);
        }

        if (null !== $suffix) {
            $lineEditHtml = $W->Frame()->addItem($lineEdit)->addItem($W->Label($suffix));
        } else {
            $lineEditHtml = $lineEdit;
        }


        $pair
            ->setFirst($W->Label($labelText)->setAssociatedWidget($lineEdit))
            ->setSecond($lineEditHtml);

        return $pair;
    }



    /**
     * Creates a label and text edit in an pair widget.
     * @param string	$name
      * @param string	$labelText
     * @return Widget_Pair
     */
    public static function getLabelEditMulti($name, $labelText, $nbLines = 5, $nbColumns = 70, $classes = array()) {

        $W = bab_functionality::get('Widgets');

        $textEdit = $W->textEdit()
            ->setName($name)
            ->setLines($nbLines)
            ->setColumns($nbColumns);

        $pair = $W->create('Widget_Pair');

        foreach($classes as $classname) {
            $textEdit->addClass($classname);
            $pair->addClass('pair_'.$classname);
        }

        $pair
            ->setFirst($W->Label($labelText)->setAssociatedWidget($textEdit))
            ->setSecond($textEdit);



        return $pair;
    }









    /**
     * Node Type select
     *
     * @return Widget_Pair
     */
    public static function getNodeTypeSelect($nodes_types) {

        $W = bab_functionality::get('Widgets');
        $pair = $W->create('Widget_Pair');
        $select = $W->Select('smed_node_type')->setName('node_type');

        $labels = array(

            smed_Node::ISCORE 		=> smed_translate('Reference to a core node'),
            smed_Node::ISMODIFIED 	=> smed_translate('Reference to a core node with custom labels'),
            smed_Node::ISNEW 		=> smed_translate('Custom node')
        );

        $options = array();
        foreach($nodes_types as $nt) {
            $options[$nt] = $labels[$nt];
        }


        $select->setOptions($options);

        $pair
            ->setFirst($W->Label(smed_translate('Node type'))->setAssociatedWidget($select))
            ->setSecond($select);



        return $pair;
    }






    /**
     * Sort Type select
     *
     * @return Widget_Pair
     */
    public static function getSortTypeSelect() {

        $W = bab_functionality::get('Widgets');
        $pair = $W->create('Widget_Pair');
        $select = $W->Select('smed_sort_type')->setName('sort_type');

        $options = array(

            smed_Node::SORT_UNORDERED 		=> smed_translate('No sort'),
            smed_Node::SORT_ALPHA 			=> smed_translate('Sort alphanumeric by the node name'),
            smed_Node::SORT_FIXED_TOP 		=> smed_translate('Sort manually, remaining nodes are on top'),
            smed_Node::SORT_FIXED_BOTTOM 	=> smed_translate('Sort manually, remaining nodes are at the bottom')
        );


        $select->setOptions($options);

        $pair
            ->setFirst($W->Label(smed_translate('Method used to sort childnodes'))->setAssociatedWidget($select))
            ->setSecond($select);



        return $pair;
    }








    /**
     * Get a forms elements of a node for one language
     * @param	string	$language
     * @return 	Widget_Tab
     */
    public static function getNodeLanguageTab($lang) {
        $W = bab_functionality::get('Widgets');

        $tab = $W->Tab(null, $lang)->setName($lang)

        ->addItem(smed_Widgets::getLabelEdit('name', smed_translate('Name'), 50))
        ->addItem(smed_Widgets::getLabelEditMulti('description', smed_translate('Description')));

        return $tab;
    }



    /**
     * Get forms elements dependant of languages
     * @return Widget_Tabs
     */
    public static function getNodeLanguagesTabSet() {
        $W = bab_functionality::get('Widgets');
        $tabs = $W->Tabs('smed_labels_modify')->setName('languages');

        $languages = bab_getAvailableLanguages();
        bab_sort::natcasesort($languages);

        foreach($languages as $lang) {
            $tab = self::getNodeLanguageTab($lang);
            $tabs->addItem($tab);

            if ($GLOBALS['babLanguage'] === $lang) {
                $tabs->setSelectedTab($tab);
            }
        }


        $tabs->displayAllTabs(true);

        return $tabs;
    }



    public static function getNodeCoreLabels($name, $description) {
        $W = bab_functionality::get('Widgets');
        $frame = $W->Frame('smed_labels_core');


        $frame->addItem(
            $W->Pair($W->Label(smed_translate('Name')), $W->Label($name))
        );

        if ($description) {

            $frame->addItem(
                $W->Pair($W->Label(smed_translate('Description')), $W->Label($description))
            );
        }

        return $frame;
    }


    /**
     * Get form elments for custom node type
     *  - content type selector 			(container|topic|category|url)
     *  - optional taget node
     * @return unknown_type
     */
    public static function getContentTypeAndTarget()
    {
        $W = bab_functionality::get('Widgets');
        $hbox = $W->FlowLayout('smed_custom_options')->setHorizontalSpacing(3,'em');

        $select = $W->Select()->setName('content_type');

        $hbox->addItem(self::getContentTypeSelect());
        $hbox->addItem(self::suggestNode('target', smed_translate('Node id of the target')));

        return $hbox;
    }




    /**
     * Content type select
     *
     * @return Widget_Pair
     */
    public static function getContentTypeSelect() {

        $W = bab_functionality::get('Widgets');
        $select = $W->Select()->setName('content_type');

        $options = array(
            ''			=> '',
            'container' => smed_translate('Container'),
            'topic' 	=> smed_translate('Topic'),
            'category' 	=> smed_translate('Category'),
            'url' 		=> smed_translate('Url')
        );


        $select->setOptions($options);

        return $W->Pair($W->Label(smed_translate('Content type'))->setAssociatedWidget($select), $select);
    }



    /**
     * search in core sitemap by keyword
     * @array
     */
    private static function searchSiteMap($keyword) {

        $keyword = mb_strtolower($keyword);

        $return = array();

        $root = bab_siteMap::get();
        $DGAll = $root->getNodeById(smed_Sitemap::ROOT);

        $iterator = new bab_NodeIterator($DGAll);
        $node = $iterator->nextNode();

        while ($node = $iterator->nextNode()) {

            $sitemapItem = $node->getData();
            if (false !== mb_strpos(mb_strtolower($sitemapItem->name), $keyword)
             || false !== mb_strpos(mb_strtolower($sitemapItem->id_function), $keyword)) {

                $return[] = $sitemapItem;
            }
        }

        return $return;
    }



    /**
     * auto suggest field for node ID
     *
     * @param	string	$name
     * @param	string	$label
     *
     * @return Widget_Pair
     */
    public static function suggestNode($name, $label) {

        $W = bab_functionality::get('Widgets');
        $suggest = $W->SuggestLineEdit()
            ->setName($name)
            ->setSize(30);

        if ($keyword = $suggest->getSearchKeyword()) {

            foreach(self::searchSiteMap($keyword) as $sitemapItem) {
                $suggest->addSuggestion($sitemapItem->id_function, $sitemapItem->id_function, $sitemapItem->name);
            }
            $suggest->sendSuggestions();
        }


        return $W->Pair(
            $W->Label($label),
            $suggest
        );
    }










    /**
     *
     * @return Widget_Widget
     */
    public static function movedReference($id_sitemap, $node) {

        $W = bab_functionality::get('Widgets');

        $originalPath = $node->getOriginalPath();
        $restoreUrl = bab_url::request('tg');
        $restoreUrl = bab_url::mod($restoreUrl, 'idx', 'deleteReference');
        $restoreUrl = bab_url::mod($restoreUrl, 'id_sitemap', $id_sitemap);
        $restoreUrl = bab_url::mod($restoreUrl, 'id_function', $node->getId());

        return $W->VBoxLayout()
            ->addItem($W->Label(sprintf(smed_translate("This node has been moved, the original place was : %s"), $originalPath)))
            ->addItem($W->Frame()->addClass('smed_infoButton')
                ->addItem($W->Link(smed_translate('Restore original node'), $restoreUrl))
            )

            ->addClass('smed_movedReference')
            ;

    }









    public static function getIconSelect() {


        $W = bab_functionality::get('Widgets');
        $pair = $W->create('Widget_Pair')->setId('smed_classnames');
        $select = $W->Select()->setName('classnames');

        bab_functionality::includefile('Icons');
        $iconReflector = new ReflectionClass('Func_Icons');
        $constants = array('' => '');
        $constants += array_flip($iconReflector->getConstants());

        bab_Sort::natcasesort($constants);

        $select->setOptions($constants);

        $pair
            ->setFirst($W->Label(smed_translate('Icon'))->setAssociatedWidget($select))
            ->setSecond($select);



        return $pair;

    }
}
