<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';

/**
 * Initialize a default sitemap on install
 *
 */
class smed_defaultSitemap
{
	const SITEMAP_ID = 1;
	
	public function create()
	{
	    bab_functionality::includeOriginal('Icons');
	    
		smed_Node::createContainer(self::SITEMAP_ID, 'DGAll', 'Custom', smed_translate('Root'), smed_translate('Sitemap root'));
		
		$this->url('Custom', 'smed_Home', '?', smed_translate('Home'), null, Func_Icons::PLACES_USER_HOME);
		$this->ref('Custom', 'smed_UserMenu', 'babUser', 1, smed_Node::SORT_ALPHA, Func_Icons::PLACES_USER_APPLICATIONS);
		$this->ref('Custom', 'smed_AdminMenu', 'babAdmin', 1, smed_Node::SORT_ALPHA, Func_Icons::PLACES_ADMINISTRATOR_APPLICATIONS);
		
		$settings = bab_getInstance('bab_Settings');
		/* @var $settings bab_Settings */

	    $settings->setForAllSites('sitemap', 'My sitemap');
	}
	
	

	
	private function ref($parent, $nodeId, $targetId, $targetChildnodes, $sort_type = smed_Node::SORT_FIXED_BOTTOM, $icon = '')
	{
		
		smed_Node::record(
				self::SITEMAP_ID,
				$parent,
				$nodeId,
				smed_Node::ISNEW, 	// smed_node::ISCORE | smed_node::ISMODIFIED | smed_node::ISNEW
				'', 				// url
				null,				// list of core node ID
				array(),			// labels
				$sort_type, 		// childnodes sort method
				$icon,				//	icon class
				$targetId, 			//	Target function id
				$targetChildnodes,	//  target childnodes
				'ref'				//content type of node : container|topic|category|url|ref,
		);
		
	}
	
	
	private function url($parent, $nodeId, $url, $name, $description = null, $icon = '')
	{
		$lang = bab_getLanguage();
		
		smed_Node::record(
				self::SITEMAP_ID,
				$parent,
				$nodeId,
				smed_Node::ISNEW, 	// smed_node::ISCORE | smed_node::ISMODIFIED | smed_node::ISNEW
				$url, 				// url
				null,				// list of core node ID
				array(
					$lang => array('name' => $name, 'description' => $description)
				),					// labels
				smed_Node::SORT_FIXED_BOTTOM, // childnodes sort method
				$icon, 				//	icon class
				'', 				//	Target function id
				0,					//  target childnodes
				'url'				//content type of node : container|topic|category|url|ref,
		);
	}
	
}