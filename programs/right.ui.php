<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/node.ui.php';

bab_Widgets()->includePhpClass('Widget_Form');

/* @var $I Func_Icons */
$I = \bab_Functionality::get('Icons');
if ($I) {
    $I->includeCss();
}




class smed_RightEdit extends Widget_Form
{
    /**
     * @var smed_Node
     */
    protected $node;


    public function __construct(smed_Node $node) {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(3, 'em');
        parent::__construct(null, $layout);

        $this->node = $node;
        
        $this->setName('node');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        


        $this->addFields();
        $this->addButtons();

        $this->loadValues();
    }

    

    

    protected function addButtons()
    {
        $W = bab_Widgets();

        $controller = smed_controller()->Node();
        $nodeId = $this->node->getId();
        

        $buttons = $W->FlowItems(
            $W->SubmitButton()->validate()
            ->setAction($controller->saveRights())
            ->setSuccessAction($controller->display($nodeId))
            ->setFailedAction($controller->editRights($nodeId))
            ->setLabel(smed_translate('Save'))
        )->addClass(Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(1, 'em');

        
        $buttons->addItem(
            $W->Link(smed_translate('Cancel'), $controller->display($nodeId))
            ->addClass(Func_Icons::ACTIONS_DIALOG_CANCEL)
            ->addClass('icon')
        );
        

        $this->addItem($buttons);
    }



    protected function loadValues()
    {
    
        $this->setHiddenValue('node[id_function]', $this->node->getId());
        
        $this->setValues(array(
            'node' => 
                array(
                    'rights' => $this->node->getRights(),
                    'default' => $this->node->getRightsDefault()
                )
            )
        );
    
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        
        $setRights = $W->Section(
            smed_translate('Set the rights'),
            $W->VBoxItems()->setVerticalSpacing(1, 'em')
        );
       
        $setRights->setName('rights');
        $this->addItem($setRights);
        
        $setRights->addItem($this->recursive());

        $setRights->addItem($this->create());
        $setRights->addItem($this->read());
        $setRights->addItem($this->update());
        $setRights->addItem($this->delete());
        
        
    }
    
    

    
    
    protected function recursive()
    {
        $W = bab_Widgets();
        
        $label = smed_translate('Set right recursively on childnodes');
        $box = $W->CheckBox();
        
        if (!$this->node->canEditRights()) {
            $box->setValue(1);
            $box->addAttribute('disabled', 'disabled');
        }
    
        return $W->LabelledWidget(
            $label,
            $box,
            __FUNCTION__
        );
    }
    
    
    
    protected function create()
    {
        $W = bab_Widgets();

        return $W->Acl()->setName(__FUNCTION__)->setTitle(smed_translate('Who can create sub-nodes?'));
    }
    
    protected function read()
    {
        $W = bab_Widgets();
    
        return $W->Acl()->setName(__FUNCTION__)->setTitle(smed_translate('Who can view?'));
    }
    
    
    protected function update()
    {
        $W = bab_Widgets();
    
        return $W->Acl()->setName(__FUNCTION__)->setTitle(smed_translate('Who can modify node?'));
    }
    
    protected function delete()
    {
        $W = bab_Widgets();
    
        return $W->Acl()->setName(__FUNCTION__)->setTitle(smed_translate('Who can delete the node?'));
    }
}









class smed_RightDefaultEdit extends smed_RightEdit
{
    protected function addFields()
    {
        $W = bab_Widgets();
    
        $this->addItem($W->Label(smed_translate('Only publication items will be created with the default rights')));

        $setDefault = $W->Section(
            smed_translate('Default rights on created sub-nodes'),
            $W->VBoxItems()->setVerticalSpacing(1, 'em')
            );
        $setDefault->setName('default');
        $this->addItem($setDefault);
    
        $setDefault->addItem($this->create());
        $setDefault->addItem($this->read());
        $setDefault->addItem($this->update());
        $setDefault->addItem($this->delete());
    }
    
    
    
    protected function addButtons()
    {
        $W = bab_Widgets();
        
    
        $controller = smed_controller()->Node();
        $nodeId = $this->node->getId();
    
    
        $buttons = $W->FlowItems(
            $W->SubmitButton()->validate()
            ->setAction($controller->saveRightsDefault())
            ->setSuccessAction($controller->editTypes($nodeId))
            ->setFailedAction($controller->editTypes($nodeId))
            ->setLabel(smed_translate('Save'))
            )->addClass(Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(1, 'em');
    
    
            $buttons->addItem(
                $W->Link(smed_translate('Cancel'), $controller->editTypes($nodeId))
                ->addClass(Func_Icons::ACTIONS_DIALOG_CANCEL)
                ->addClass('icon')
            );
    
    
            $this->addItem($buttons);
    }

}