<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/node.ui.php';

bab_Widgets()->includePhpClass('Widget_Form');






class smed_TypesEdit extends Widget_Form
{
    /**
     * @var smed_Node
     */
    private $node;


    public function __construct(smed_Node $node) {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        parent::__construct(null, $layout);

        $this->node = $node;
        
        $this->setName('node');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        


        $this->addFields();
        $this->addButtons();

        $this->loadValues();
    }

    


    

    protected function addButtons()
    {
        $W = bab_Widgets();
        /* @var $I Func_Icons */
        $I = \bab_Functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        $controller = smed_controller()->Node();
        $nodeId = $this->node->getId();
        

        $buttons = $W->FlowItems(
            $W->SubmitButton()->validate()
            ->setAction($controller->saveTypes())
            ->setSuccessAction($controller->display($nodeId))
            ->setFailedAction($controller->editRights($nodeId))
            ->setLabel(smed_translate('Save'))
        )->addClass(Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(1, 'em');

        
        $buttons->addItem(
            $W->Link(smed_translate('Cancel'), $controller->display($nodeId))
            ->addClass(Func_Icons::ACTIONS_DIALOG_CANCEL)
            ->addClass('icon')
        );
        

        $this->addItem($buttons);
    }



    protected function loadValues()
    {
    
        $this->setHiddenValue('node[id_function]', $this->node->getId());
        $values = array();
        $contenttypes = $this->node->getContentTypes();
        $values['contenttypes'] = array();
        foreach($contenttypes as $checked) {
            $values['contenttypes'][$checked] = 1;
        }
        $this->setValues(array('node' => $values));
    
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        

        $this->addItem($this->recursive());
        $this->addItem($this->contenttypes());
        
    }
    
    

    
    
    protected function recursive()
    {
        $W = bab_Widgets();
        
    
        return $W->LabelledWidget(
            smed_translate('Set allowed content types recursively on childnodes'),
            $W->CheckBox(),
            __FUNCTION__
        );
    }
    
    
    protected function contenttypes()
    {
        $W = bab_Widgets();
        
        $vbox = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');
        $frame = $W->Frame(null, $vbox);
        $frame->setName(__FUNCTION__);
        $vbox->addItem($W->Title(smed_translate('Allowed content types')));
        
        $withRights = $W->Section(
            smed_translate('Content types with ability to set rights'),
            $W->VBoxLayout()->setVerticalSpacing(0.5, 'em')
        );
        
        $withRights->setFoldable(true, false);
        
        $defaultFunc = bab_functionality::get('SitemapEditorNode');
        /*@var $defaultFunc Func_SitemapEditorNode */
         
        foreach($defaultFunc->getAllContentTypes() as $content_type => $arr) {
            
            $type = $arr['functionality'];
            /*@var $type Func_SitemapEditorNode */
            
            if ($type && $type->canEditRights()) {
                $withRights->addItem(
                    $W->LabelledWidget($arr['description'], $W->CheckBox(), $content_type)
                );
                continue;
            }
            
            $vbox->addItem(
                $W->LabelledWidget($arr['description'], $W->CheckBox(), $content_type)
            );
        }
        
        
        
        
        
        if (count($withRights->getLayout()->getItems()) > 0) {
            $frame->addItem($withRights);
            
            // default rights are modifiables from 8.5.97
            
            if (method_exists('bab_siteMapItem', 'getDefaultCreate')) {
            
                $menu = $withRights->addContextMenu('popup');
                $menu->addItem(
                    $W->Link($W->Icon(smed_translate('Default rights'), Func_Icons::ACTIONS_SET_ACCESS_RIGHTS),
                        smed_controller()->Node()->editRightsDefault($this->node->getId())
                    )
                    ->setOpenMode(Widget_Link::OPEN_DIALOG)
                );
            
            }
        }
        
        return $frame;
    }
    
    
}