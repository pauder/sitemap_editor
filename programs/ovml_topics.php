<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_functionality::includefile('Ovml/Function');

/**
 * <OFSitemapEditorTopics node="">
 */
class Func_Ovml_Function_SitemapEditorTopics extends Func_Ovml_Function
{
    public function toString()
    {
        $node = '';
        $saveas = '';
        if(count($this->args))
        {
            foreach( $this->args as $p => $v)
            {
                switch(mb_strtolower(trim($p)))
                {
                    case 'node':
                        $node = $v;
                    break;

                    case 'saveas':
                        $saveas = $v;
                    break;
                }
            }

            if (!$node)
            {
                return '';
            }


            global $babBody;
            $sitemap = bab_siteMap::getByUid($babBody->babsite['sitemap']);
            if (!isset($sitemap)) {
                $sitemap = bab_siteMap::get();
            }

            $bab_node = $sitemap->getNodeById($node);

            if (!$bab_node)
            {
                return 'Node not found for : '.$node;
            }

            $topics = implode(',',$this->getTopics($bab_node));

            if ($saveas)
            {
                $this->gctx->push($saveas, $topics);
                return '';
            }

            return $topics;
        }
    }


    /**
     * Returns the topic associated to the node.
     *
     * @param bab_Node $node
     * @return int|boolean		The topic id associated to the node or false if not a topic node.
     */
    private static function getNodeTopic(bab_Node $node)
    {
        /* @var $item bab_SitemapItem */
        $item = $node->getData();
        $target = $item->target;

        // if target is a topic
        if ($target instanceof bab_SitemapItem) {
            $target = $target->id_function;
        }
        if ($target && is_string($target) && 'babArticleTopic_' === substr($target, 0, 16)) {
            return (int) substr($target, 16);
        }

        return false;
    }


    /**
     * @param	bab_Node	$node
     * @return array
     */
    private function getTopics(bab_Node $node)
    {
        $topics = array();
        if ($node->hasChildNodes()) {
            $I = new bab_NodeIterator($node);
            while ($node = $I->nextNode()) {
                if ($topic = self::getNodeTopic($node)) {
                    $topics[] = $topic;
                }
            }
        } else {
            if ($topic = self::getNodeTopic($node)) {
                $topics[] = $topic;
            }
        }

        return $topics;
    }
}





bab_Functionality::get('Ovml/Container/SitemapEntries');

/**
 * <OCSitemapEditorAttachedFiles basenode="" node="" keeplastknown="0|1">
 *
 * keep last known disabled by default
 *
 * <OVSitemapEditorAttachedFileFullpath>
 * <OVSitemapEditorAttachedFileName>
 * <OVSitemapEditorAttachedFileUrl>
 */
class Func_Ovml_Container_SitemapEditorAttachedFiles extends Ovml_Container_Sitemap
{


    /**
     * @var bab_Path
     */
    protected $attachedFilesPath = null;

    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $baseNodeId = $ctx->get_value('basenode');

        $nodeId = $ctx->get_value('node');

        if (isset($this->sitemap)) {

            if ($nodeId === false || empty($nodeId)) {
                $nodeId = bab_Sitemap::getPosition();

                if ($baseNodeId) {
                    // if base node (parameter 'basenode') has been specified,
                    // we try to find if a descendant of this node has
                    // a target to the current position.
                    $baseNode = $this->sitemap->getNodeById($baseNodeId);

                    if (null === $baseNode)
                    {
                        trigger_error(sprintf('the basenode="%s" attribute has been specified in ovml file %s but the node has not been found in the sitemap',$baseNodeId, (string) $ctx->debug_location));
                        bab_debug((string) $this->sitemap);
                    } else {

                        $nodes = $this->sitemap->createNodeIterator($baseNode);

                        while ($node = $nodes->nextNode()) {
                            $sitemapItem = $node->getData();
                            $target = $sitemapItem->target;
                            if ($target === $nodeId) {
                                $nodeId = $sitemapItem->id_function;
                                break;
                            }
                        }
                    }
                }
            }


            $keepLastKnown = $ctx->get_value('keeplastknown');
            if ($keepLastKnown === false) {
                // If keeplastknown is not specified, active by default
                $keepLastKnown = 0;
            }


            if ($keepLastKnown)
            {
                if (empty($nodeId)) {

                    if (isset($_SESSION['bab_sitemap_lastknownnode'])) {
                        $nodeId = $_SESSION['bab_sitemap_lastknownnode'];
                    }
                } else {
                    $_SESSION['bab_sitemap_lastknownnode'] = $nodeId;
                }
            }

            require_once dirname(__FILE__) . '/node.class.php';
            $node = smed_Node::getFromId(1, $nodeId);

            /*if (empty($baseNodeId))
            {
                $baseNodeId = bab_Sitemap::getVisibleRootNodeByUid($this->sitemap_name);
            }*/

            if (isset($node)) {
                $this->attachedFilesPath = $node->uploadPath();

                $this->attachedFiles = array();
                foreach ($this->attachedFilesPath as $attachedFile) {
                    $this->attachedFiles[] = $attachedFile;
                }

                $this->count = count($this->attachedFiles);
            }

        }

    }

    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }
        $this->ctx->curctx->push('CIndex', $this->idx);

        $fullpath = $this->attachedFiles[$this->idx]->toString();

        $imageUrl = '';
        if ($T = bab_functionality::get('Thumbnailer')) {
            $T->setSourceFile($fullpath);
//			$T->setBorder(1, '#bbbbbb', 1, '#ffffff');
            $imageUrl = $T->getThumbnail(2000, 2000);
        }

        $this->ctx->curctx->push('SitemapEditorAttachedFileFullpath', $fullpath);
        $this->ctx->curctx->push('SitemapEditorAttachedFileName', bab_Path::decode(basename($fullpath)));
        $this->ctx->curctx->push('SitemapEditorAttachedFileUrl', $imageUrl);
        $this->idx++;
        $this->index = $this->idx;
        return true;
    }

}



/**
 * <OFSitemapEditorThumbnail path="" maxwidth="" maxheight="" innerborderwidth="" innerbordercolor="" innerborder="" outerborderwidth="" outerbordercolor="" outerborder="">
 *
 * path : the full path to the file to thumbnail. For example as return in <OVSitemapEditorAttachedFileFullpath> by <OCSitemapEditorAttachedFiles>
 * maxwidth : maximum width of thumbnail image in pixels.
 * maxheight : maximum height of thumbnail image in pixels.
 * innerborderwidth : inner border width in pixels.
 * innerbordercolor : inner border color in hex, eg. innerbordercolor="#ffffff".
 * innerborder : same as innerborderwidth,innerbordercolor. Eg. innerborder="2,#ffffff".
 * outerborderwidth : outer border width in pixels.
 * outerbordercolor : inner border color in hex, eg. outerbordercolor="#cccccc".
 * outerborder : same as outerborderwidth,outerbordercolor. Eg. outerborder="1,#cccccc".
 *
 * Returns the url to the thumbnail image.
 *
 */
class Func_Ovml_Function_SitemapEditorThumbnail extends Func_Ovml_Function
{

    public function toString()
    {
        $path = '';
        $maxwidth = 3000;
        $maxheight = 3000;
        $innerbordercolor = '#ffffff';
        $outerbordercolor = '#cccccc';
        $innerborderwidth = 0;
        $outerborderwidth = 0;
        $saveas = false;

        if (count($this->args)) {
            foreach ($this->args as $p => $v) {
                switch (mb_strtolower(trim($p))) {

                    case 'path':
                        $path = $v;
                    break;

                    case 'maxwidth':
                        $maxwidth = $v;
                        break;

                    case 'maxheight':
                        $maxheight = $v;
                        break;

                    case 'innerborderwidth':
                        $innerborderwidth = $v;
                        break;
                    case 'innerbordercolor':
                        $innerbordercolor = $v;
                        break;
                    case 'innerborder':
                        list($innerborderwidth, $innerbordercolor) = explode(',', $v);
                        break;

                    case 'outerborderwidth':
                        $outerborderwidth = $v;
                        break;
                    case 'outerbordercolor':
                        $outerbordercolor = $v;
                        break;
                    case 'outerborder':
                        list($outerborderwidth, $outerbordercolor) = explode(',', $v);
                        break;

                    case 'saveas':
                        $saveas = $v;
                        break;
                }
            }

            if (!$path) {
                return '';
            }
        }

        $imageUrl = '';
        /* @var $T Func_Thumbnailer */
        if ($T = bab_functionality::get('Thumbnailer')) {
            $T->setSourceFile($path);
            if ($innerborderwidth != 0 || $outerborderwidth != 0) {
                $T->setBorder($outerborderwidth, $outerbordercolor, $innerborderwidth, $innerbordercolor);
            }
            $imageUrl = $T->getThumbnail($maxwidth, $maxheight);
        }

        if ($saveas) {
            $this->gctx->push($saveas, $imageUrl);
            return '';
        }
        return $imageUrl;
    }

}
