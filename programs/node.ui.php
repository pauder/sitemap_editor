<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/base.ui.php';

$W = smed_Widgets();

$W->includePhpClass('Widget_Frame');

/* !!! This class is just used by the method SelectParentNode() in smed_NodeEditor class */
class smed_recursive {

	var $nodes; /* array : contains all nodes */
	var $select; /* object Widget_Select */

	var $nodesSummit; /* array : contains all nodes already coursed */
	var $level; /* string : contains dots who indicates the level of the current node */

	private $nodeId;

	/**
	 *
	 * @param Iterator 		$nodes
	 * @param Widget_Select $select
	 * @param string 		$nodeId			Current modified node or null for a new node
	 */
	public function __construct(Iterator $nodes, Widget_Select $select, $nodeId) {
		$this->select = $select;
		$this->nodesSummit = array();

		$this->nodeId = $nodeId;

		// index

		foreach($nodes as $node)
		{
			$this->nodes[$node->getId()] = $node;
		}
	}

	/* Recursive course
	 *
	 * @param	string	$currentNode
	 */
	public function makeTree($currentNode, $level='') {
		/* Save the node summit not to go it through several times */
		$this->saveNodeSummit($currentNode);
		$node = $this->nodes[$currentNode];

		if ($node->getId() == $this->nodeId)
		{
			return;
		}

		if ($node->getName())
		{
			$this->select->addOption($currentNode, $level.$node->getName());
		}

		/* List of sub-nodes */
		foreach ($this->nodes as $nodeTmp) {
			if ($nodeTmp->getParentId() == $currentNode && !isset($this->nodesSummit[$nodeTmp->getId()])) {
				$this->makeTree($nodeTmp->getId(), $level.bab_nbsp().bab_nbsp().bab_nbsp());
			}
		}

	}

	/* Save the node summit not to go it through several times */
	public function saveNodeSummit($noeud) {

		$this->nodesSummit[$noeud] = 1;

	}

}







class smed_ConfirmDeleteEditor extends smed_Editor
{

    public function __construct(smed_Node $node)
    {
        $W = bab_Widgets();
        
        parent::__construct(null, $W->VBoxLayout(), $node);
        
        $this->setName('node');
        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('node[id]', $node->getId());

        $this->setSaveAction(smed_controller()->Node()->delete(), smed_translate('Ok'));
        $this->setCancelAction(smed_controller()->Node()->cancel());
        
        if ('topic' === $node->getContentType()) {
            $this->addItem($this->delete_topic());
        }
    }


    protected function delete_topic()
    {
        $W = $this->widgets;
        return $this->labelledField(
            smed_translate('Delete the associated topic'),
            $W->Checkbox()->setValue(false),
            __FUNCTION__
        );
    }

}











function smed_getRightsSection(smed_Node $node)
{
    $W = smed_Widgets();
    
    $vboxRights = $W->VBoxItems(
        $W->Acl()->setValue($node->getCreate())->setDisplayMode()->setTitle(smed_translate('Create')),
        $W->Acl()->setValue($node->getRead())->setDisplayMode()->setTitle(smed_translate('View')),
        $W->Acl()->setValue($node->getUpdate())->setDisplayMode()->setTitle(smed_translate('Update')),
        $W->Acl()->setValue($node->getDelete())->setDisplayMode()->setTitle(smed_translate('Delete'))
    )->setVerticalSpacing(1, 'em');
    
    $vboxContentTypes = $W->VBoxItems()->setVerticalSpacing(.5, 'em');
    $vboxContentTypes->addItem($W->Title(smed_translate('Allowed content types for subnodes creation'), 4));
    foreach ($node->getContentTypesLabels() as $label) {
        $vboxContentTypes->addItem($W->Label($label));
    }
    
    return $W->Section(
        smed_translate('Rights configuration'),
        $W->HBoxItems(
            $vboxRights,
            $vboxContentTypes
        )->setHorizontalSpacing(14, 'em')
    );
}


function smed_getChildNodesSection(smed_Node $node)
{
    $W = smed_Widgets();
    $childNodes = smed_Sitemap::getModifiedIterator($node->getId());
    
    if (count($childNodes) === 0) {
    
        $childNodesSection = null;
    
    } else if (count($childNodes) === 1) {
    
        $childNodesSection = $W->Section(
            smed_translate('Child nodes'),
            $vbox = $W->VBoxItems(
                )->addClass('icon-left-16 icon-left icon-16x16')
            );
        foreach($childNodes as $childNode) {
            $vbox->addItem(
                $W->Icon($childNode->getName(), Func_Icons::PLACES_FOLDER)
                );
        }
    
    } else {
    
        $childNodesForm = $W->Form(null, $W->VBoxLayout());
        $childNodesFrame = $W->Frame(null, $W->VBoxLayout()->sortable())
        ->setName('childnodes')
        ->addClass('icon-left-16 icon-left icon-16x16')
        ->addClass('smed_sort');
    
        foreach($childNodes as $childNode) {
            $childNodesFrame->addItem(
                $W->FlowItems(
                    $W->Hidden()->setName($childNode->getId()),
                    $W->Icon($childNode->getName(), Func_Icons::PLACES_FOLDER)
                    )
                );
        }
    
        $childNodesForm->addItem($childNodesFrame);
    
        $childNodesForm->additem(
            $W->Frame()
            ->addClass('smed_submit')
            ->additem(
                $W->SubmitButton()
                ->setAction(smed_controller()->node()->sortChildNodes())
                ->setLabel(smed_translate('Save order')
                    )
                )
            );
        $childNodesForm->setSelfPageHiddenFields();
    
        if (count($childNodes) > 1) {
            $vbox = $W->VBoxItems(
                $W->Label(smed_translate('Drag and drop the nodes to reorder them')),
                $childNodesForm
                )->setVerticalSpacing(1, 'em');
        } else {
            $vbox = $W->VBoxItems($childNodesForm);
        }
        
        $childNodesSection = $W->Section(
            smed_translate('Child nodes'),
            $vbox
        );
    
    }
    
    return $childNodesSection;
}



