; <?php /*
[general]
name							="sitemap_editor"
version							="0.9.0"
encoding						="ISO-8859-15"
description						="Editor for Ovidentia core site map"
description.fr					="Module de gestion du plan du site et des menus"
long_description.fr             ="README.md"
mysql_character_set_database	="latin1,utf8"
db_prefix						="smed_"
delete							=1
ov_version						="8.6.0"
php_version						="5.1.0"
mysql_version					="4.1.2"
addon_access_control			=0
addon_type						="EXTENSION"
author							="Paul de Rosanbo / CANTICO"
icon							="alacarte.png"
configuration_page				="main&idx=config.edit"
tags							="extension,default"

; mysql_max_allowed_packet		=15M 	; le cache du plan du site necessite le traitement de grosse requette si on utilise la classe smed_Cache_Db
magic_quotes_gpc				="Off" 	; l'extraction de la variable babrw des url ne fonctionne pas correctement avec les magic_quote (fonction parse_str)

[recommendations]
mod_rewrite						="Available"


[addons]
jquery							=">=1.3.2.3"
widgets							=">=1.0.110"
LibTranslate					=">=1.12.0rc3.01"
applications					=">=4.6.3"

[functionalities]
Widgets							="Available"
jquery							="Available"


;*/?>
